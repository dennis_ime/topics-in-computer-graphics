import argparse
import re
import numpy as np

class MetaVertexIndex:
	def __init__(self, vidx, tidx, nidx):
		self.vidx = vidx
		self.nidx = nidx
		self.tidx = tidx

class VertexData:
	def __init__(self, vertex, normal, coordTex):
		self.vertex = vertex 
		self.normal = normal
		self.coordTex = coordTex

class Face:
	def __init__(self, obj_file_line):		
		tface = re.findall("\d+/\d*/\d+|\d+\s", obj_file_line)
		self._indices = []
		self._invalid_indices = []
		for tidx in tface:
			idxs = tidx.split("/")			
			self._indices.append(MetaVertexIndex(int(idxs[0]), int(idxs[1]), int(idxs[2])))
			
	@property
	def indices(self):
		return self._indices

	def is_triangle(self):
		return len(self._indices) == 3

	def to_triangles(self):
		idx = self._indices
		if self.is_triangle():
			return self._indices

		return [idx[0], idx[1], idx[2], idx[0], idx[2], idx[3]]

	

class OBJParser:
	def parse(self, obj_path):
		vertices, texCoord, normals, faces = [], [], [], []
		with open(obj_path, "r") as f:
			vertices, texCoord, normals, faces = self._read_data(f)

		v, t, n, i = self._process_faces(faces, texCoord, vertices, normals)

		return OBJEntity(v, t, n, i)

	def _process_faces(self, faces, texCoord, vertices, normals):
		N = np.max([len(vertices), len(normals), len(texCoord)])
		vd = []
		vt = []
		nd = []
		idxmap = {}
		fidx = []
		curidx = 0

		for f in faces:
			for idx in f.to_triangles():
				if (idx.vidx, idx.nidx) in idxmap:
					fidx.append(idxmap[(idx.vidx-1, idx.tidx-1, idx.nidx-1)])
				else: 
					fidx.append(curidx)
					vd.append(vertices[idx.vidx-1])
					vt.append(texCoord[idx.tidx-1])
					nd.append(normals[idx.nidx-1])
					idxmap[(idx.vidx-1, idx.tidx-1, idx.nidx-1)] = curidx
					curidx += 1 

		return (np.array(vd, dtype=np.float32), np.array(vt, dtype=np.float32), 
					np.array(nd, dtype=np.float32), np.array(fidx, dtype=np.uint32))


	def _read_data(self, objfile):
		vertices = []		
		normals = []
		texCoord = []
		faces = []
		for line in objfile:
			values = re.split("\s+", line)
			if values[0] == "v":
				vertices.append(np.array([values[1], values[2], values[3]], np.float))				
			elif values[0] == "vn":
				normals.append(np.array([values[1], values[2], values[3]], np.float))
			elif values[0] == "vt":
				texCoord.append(np.array([values[1], values[2]], np.float))
			elif values[0] == "f":
				faces.append(Face(line[1:]))

		return vertices, texCoord, normals, faces


class OBJEntity:
	def __init__(self, vertices, texCoords, normals, indices):
		self._vertices = vertices
		self._normals = normals
		self._indices = indices
		self._texCoords = texCoords

	def _vertices_str(self):
		return np.array2string(self._vertices, separator=",")

	def _texCoords_str(self):
		return np.array2string(self._texCoords, separator=",")

	def _normals_str(self):
		return np.array2string(self._normals, separator=",")

	def _indices_str(self):
		return np.array2string(self._indices, separator=",")

	def to_str(self):
		#TODO: CODE
		rstr = """\t\tself._vertices = np.array({}, dtype=np.float32)
			\n\n\t\tself._texCoord = np.array({}, dtype=np.float32)
			\n\n\t\tself._normals = np.array({},dtype=np.float32)			
			\n\n\t\tself._indices = np.array({}, dtype=np.uint32)"""
		return rstr.format(self._vertices_str(), self._texCoords_str(), self._normals_str(), self._indices_str())

def main():
	parser = argparse.ArgumentParser(description="Convert wavefront obj file into python arrays as source code.")
	parser.add_argument("obj_path", type=str, help="obj wavefront file to be converted into python arrays")
	parser.add_argument("-out_path", type=str, default="./out.py", help="output filename")
	args = parser.parse_args()

	objparser = OBJParser()
	objentity = objparser.parse(args.obj_path)
	
	np.set_printoptions(threshold=np.nan)
	with open(args.out_path, "w") as f:
		f.write(objentity.to_str())

	print("DONE SUCCESSFULLY")

if __name__ == "__main__":
	main()