from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from MySource.Graphics.MyRenderer import MyRenderer
from MySource.Graphics.MyLight import MyLightType
from Source.Graphics.Material import Material
from MySource.Graphics.EpicMaterial import EpicMaterial
from MySource.Graphics.MyLight import MyLightType
from MySource.MyGUI.ApplicationStatus import ApplicationStatus

import os


class MyRenderWidget(QWidget):
	def __init__(self, **kwargs):
		super(MyRenderWidget, self).__init__()
	
		self._status = App		
		self._renderer = MyRenderer(self, antialiasing=True, **kwargs)

		self._viewFunc = [
			self._renderer.viewLeft,
			self._renderer.viewRight,
			self._renderer.viewTop,
			self._renderer.viewBottom,
			self._renderer.viewFront,
			self._renderer.viewBack
		]

	@property
	def layout(self):
		return self._mainLayout