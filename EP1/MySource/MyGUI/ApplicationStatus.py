from PyQt5.QtCore import QObject
from PyQt5.QtGui import QVector3D

class ApplicationStatus(QObject):
	def __init__(self):
		super(ApplicationStatus, self).__init__()
		self._status = {}
		self.backToDefaults()

	_instance = None

	@classmethod
	def instance(cls):
		if ApplicationStatus._instance is None:
			ApplicationStatus._instance = ApplicationStatus()
		return ApplicationStatus._instance

	def set(self, key, value):
		self._status[key] = value

	def get(self, key):
		return self._status[key]

	def backToDefaults(self):
		self._status["panelModel.modelCombo"] = 5
		self._status["panelModel.Torus.latResolution"] = 10
		self._status["panelModel.Torus.lngResolution"] = 10
		self._status["panelModel.Torus.outerRadius"] = 1.0
		self._status["panelModel.Torus.innerRadius"] = 0.5
		
		self._status["panelEffect.Spiky.spikychk"] = False
		self._status["panelEffect.Spiky.width"] = 0.1
		self._status["panelEffect.Spiky.height"] = 0.3
		
		self._status["panelLight.lightCombo"] = 0
		self._status["panelLight.pointLight.position"] = QVector3D(1.0, 1.0, 1.0)
		self._status["panelLight.pointLight.diffuse"] = QVector3D(0.5, 0.5, 0.5)
		self._status["panelLight.pointLight.specular"] = QVector3D(0.5, 0.5, 0.5)
		self._status["panelLight.pointLight.ambient"] = QVector3D(0.5, 0.5, 0.5)
		self._status["panelLight.pointLight.radius"] = 20.0
		self._status["panelLight.pointLight.constantAttenuation"] = 0.2
		self._status["panelLight.pointLight.linearAttenuation"] = 0.2
		self._status["panelLight.pointLight.quadraticAttenuation"] = 0.2
		self._status["panelLight.pointLight.headlight"] = False
		self._status["panelLight.directionalLight.direction"] = QVector3D(1.0, 1.0, 1.0)
		self._status["panelLight.directionalLight.diffuse"] = QVector3D(0.5, 0.5, 0.5)
		self._status["panelLight.directionalLight.specular"] = QVector3D(0.5, 0.5, 0.5)
		self._status["panelLight.directionalLight.ambient"] = QVector3D(0.5, 0.5, 0.5)
		self._status["panelLight.directionalLight.headlight"] = False
		self._status["panelLight.hemisphericalLight.groundDiffuse"] = QVector3D(1.0, 1.0, 1.0)
		self._status["panelLight.hemisphericalLight.groundSpecular"] = QVector3D(1.0, 1.0, 1.0)
		self._status["panelLight.hemisphericalLight.groundAmbient"] = QVector3D(1.0, 1.0, 1.0)
		self._status["panelLight.hemisphericalLight.skyDiffuse"] = QVector3D(0.2, 0.2, 0.2)
		self._status["panelLight.hemisphericalLight.skySpecular"] = QVector3D(0.2, 0.2, 0.2)
		self._status["panelLight.hemisphericalLight.skyAmbient"] = QVector3D(0.2, 0.2, 0.2)

		self._status["panelMaterial.emission"] = QVector3D(0.0, 0.0, 0.0)
		self._status["panelMaterial.ambient"] = QVector3D(0.2, 0.2, 0.2)
		self._status["panelMaterial.diffuse"] = QVector3D(0.8, 0.8, 0.8)
		self._status["panelMaterial.specular"] = QVector3D(0.0, 0.0, 0.0)
		self._status["panelMaterial.shininess"] = 12.0
		
		