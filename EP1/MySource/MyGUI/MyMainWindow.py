import platform

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from MySource.MyGUI.MyMainWidget import MyMainWidget

class MyMainWindow(QMainWindow):
	def __init__(self, parent=None, **kwargs):
		super(MyMainWindow, self).__init__(parent)
		
		self._format = kwargs.get("format", None)
		self.initialize()

		self._timer = QTimer(self)
		self._timer.timeout.connect(self.handleTimer)
		self._timer.start(1000)

		self.setWindowTitle("Viewer")
		self.setUnifiedTitleAndToolBarOnMac(True)

	def createFileActions(self):
		self._fileNewAction = QAction("&New", self, shortcut=QKeySequence.New, statusTip="Create a new protocol",
			triggered=self.new)

		self._fileOpenAction = QAction("&Open", self, shortcut=QKeySequence.Open, 
			statusTip="Open an existing synaptic protocol file", triggered=self.open)

		self._fileSaveAction = QAction("&Save", self, shortcut=QKeySequence.Save, 
			statusTip="Saves synaptic protocol file", triggered=self.open)

		self._fileSaveAsAction = QAction("&Save As...", self, shortcut=QKeySequence.SaveAs,
			statusTip="Saves synaptic protocol under a different name", triggered=self.saveAs)

		self._fileExitAction = QAction("&Exit", self, shortcut="Ctrl+Q", statusTip="Exit the application",
			triggered=self.close)

	def createMenus(self):
		self.fileMenu = self.menuBar().addMenu("&File")
		self.fileMenu.addAction(self._fileNewAction)
		self.fileMenu.addAction(self._fileOpenAction)
		self.fileMenu.addSeparator()
		self.fileMenu.addAction(self._fileSaveAction)
		self.fileMenu.addAction(self._fileSaveAsAction)
		self.fileMenu.addSeparator()
		self.fileMenu.addAction(self._fileExitAction)

	def initialize(self):
		self.createFileActions()
		self.createMenus()

		fontSize10 = QFont()
		fontSize10.setPointSize(10)

		self.statusBar().setFont(fontSize10)
		self.statusBar().showMessage("Ready")

		self.statistics = QLabel(" ")
		self.statistics.setFont(fontSize10)
		self.statusBar().addPermanentWidget(self.statistics)

		self._mainWidget = MyMainWidget(parent=self, font=fontSize10)
		self.setCentralWidget(self._mainWidget)
		self.resize(720, 480)

	def new(self):
		pass

	def open(self):
		pass

	def save(self):
		pass

	def saveAs(self):
		pass

	def stopTimer(self):
		self._timer.stop()

	def restartTimer(self):
		self._timer.start()

	def handleTimer(self):
		times = self._mainWidget._renderer.renderTimeEstimates()
		self.statistics.setText("Render time: " + str(round(times[0], 2)) + "ms, GPU time: " + str(round(times[1], 2)) + "ms")

	def clearStatistics(self):
		self.statistics.setText(" ")

	def closeEvent(self, closeEvent):
		super(MyMainWindow, self).closeEvent(closeEvent)