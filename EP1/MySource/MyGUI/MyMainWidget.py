from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from MySource.MyGUI.MyRenderWidget import MyRenderWidget
from MySource.Graphics.MyRenderer import MyRenderer
from MySource.MyGUI.MyStatusBarWidget import MyStatusBarWidget
from MySource.MyGUI.MySideMenuWidget import MySideMenuWidget
from MySource.MyGUI.ApplicationStatus import ApplicationStatus

from MySource.Graphics.MyLight import MyLightType
from MySource.Graphics.MyRenderer import MyRenderer
from Source.Graphics.Material import Material

class MyMainWidget(QWidget):
	def __init__(self, parent=None, **kwargs):
		super(MyMainWidget, self).__init__(parent)
		self._status = ApplicationStatus.instance()
		rendererProps = self.setMaterialInitialValue()
		rendererProps.update(kwargs)

		self._renderer = MyRenderer(self, antialiasing=True, **rendererProps)
		self._font = kwargs.get("font", QFont())
		self.setFont(self._font)

		self._parent = parent
		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(0, 0, 0, 0)
		self._mainLayout.setSpacing(3)

		self._secondRowLayout = QHBoxLayout()
		self._secondRowLayout.setContentsMargins(0, 0, 0, 0)
		self._secondRowLayout.setSpacing(3)

		self._sideMenuLayout = QHBoxLayout()
		self._sideMenuLayout.setContentsMargins(0, 0, 0, 0)
		self._sideMenuLayout.setSpacing(3)
		self._sideMenuWidget = MySideMenuWidget(parent=self, renderer=self._renderer)
		self._secondRowLayout.addWidget(self._sideMenuWidget)

		self._renderLayout = QHBoxLayout()
		self._renderLayout.setContentsMargins(0,0,0,0)
		self._renderLayout.addWidget(self._renderer)
		self._secondRowLayout.addLayout(self._renderLayout)

		self._mainLayout.addLayout(self._secondRowLayout)

		self._statusBar = MyStatusBarWidget(parent=self, renderer=self._renderer, font=self._font)
		self._statusBarLayout = QHBoxLayout()
		self._statusBarLayout.setContentsMargins(3, 0, 3, 0)
		self._statusBarLayout.addWidget(self._statusBar)
		self._statusBar.setFixedHeight(30)
		self._mainLayout.addLayout(self._statusBarLayout)
		
		self.setLayout(self._mainLayout)
		self.setLightInitialValue()
		self.setMaterialInitialValue()

	def setLightInitialValue(self):
		lightType = self._status.get("panelLight.lightCombo")
		self._renderer.changeBPLight(lightType)
		if lightType == MyLightType.POINT:
			self._renderer.setPointLightPositionX(self._status.get("panelLight.pointLight.position").x())
			self._renderer.setPointLightPositionY(self._status.get("panelLight.pointLight.position").y())
			self._renderer.setPointLightPositionZ(self._status.get("panelLight.pointLight.position").z())
			self._renderer.setPointLightDiffuseColor(self._status.get("panelLight.pointLight.diffuse"))
			self._renderer.setPointLightSpecularColor(self._status.get("panelLight.pointLight.specular"))
			self._renderer.setPointLightAmbientColor(self._status.get("panelLight.pointLight.ambient"))
			self._renderer.setPointLightRadius(self._status.get("panelLight.pointLight.radius"))
			self._renderer.setPointLightConstantAttenuation(self._status.get("panelLight.pointLight.constantAttenuation"))
			self._renderer.setPointLightLinearAttenuation(self._status.get("panelLight.pointLight.linearAttenuation"))
			self._renderer.setPointLightQuadAttenuation(self._status.get("panelLight.pointLight.quadraticAttenuation"))
			self._renderer.setPointLightHeadlight(self._status.get("panelLight.pointLight.headlight"))
		elif lightType == MyLightType.DIRECTIONAL:
			self._renderer.setDirectionalLightDirectionX(self._status.get("panelLight.directionalLight.direction").x())
			self._renderer.setDirectionalLightDirectionY(self._status.get("panelLight.directionalLight.direction").y())
			self._renderer.setDirectionalLightDirectionZ(self._status.get("panelLight.directionalLight.direction").z())
			self._renderer.setDirectionalLightAmbientColor(self._status.get("panelLight.directionalLight.ambient"))
			self._renderer.setDirectionalLightDiffuseColor(self._status.get("panelLight.directionalLight.diffuse"))
			self._renderer.setDirectionalLightSpecularColor(self._status.get("panelLight.directionalLight.specular"))
			self._renderer.setDirectionalLightHeadlight(self._status.get("panelLight.directionalLight.headlight"))
		elif lightType == MyLightType.HEMISPHERICAL:
			self._renderer.setHemisphericalLightSkyDiffuseColor(self._status.get("panelLight.hemisphericalLight.skyDiffuse"))
			self._renderer.setHemisphericalLightSkySpecularColor(self._status.get("panelLight.hemisphericalLight.skySpecular"))
			self._renderer.setHemisphericalLightSkyAmbientColor(self._status.get("panelLight.hemisphericalLight.skyAmbient"))
			self._renderer.setHemisphericalLightGroundDiffuseColor(
				self._status.get("panelLight.hemisphericalLight.groundDiffuse"))
			self._renderer.setHemisphericalLightGroundSpecularColor(
				self._status.get("panelLight.hemisphericalLight.groundSpecular"))
			self._renderer.setHemisphericalLightGroundAmbientColor(
				self._status.get("panelLight.hemisphericalLight.groundAmbient"))

	def setMaterialInitialValue(self):
		args = {}
		args["material"] = Material(emission=self._status.get("panelMaterial.emission"),
			ambient=self._status.get("panelMaterial.ambient"),
			diffuse=self._status.get("panelMaterial.diffuse"),
			specular=self._status.get("panelMaterial.specular"),
			shininess=self._status.get("panelMaterial.shininess"))
		args["actorType"] = self._status.get("panelModel.modelCombo")

		return args
		
	

			