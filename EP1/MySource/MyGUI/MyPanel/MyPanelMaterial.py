from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from MySource.MyGUI.ApplicationStatus import ApplicationStatus
from MySource.MyGUI.ColorInputWidget import ColorInputWidget
from Source.Graphics.Material import Material

class MyPanelMaterial(QWidget):
	def __init__(self, parent=None, renderer=None, **kwargs):
		super(MyPanelMaterial, self).__init__(parent)
		self._parent = parent
		self._renderer = renderer
		self._status = ApplicationStatus.instance()
		self._font = kwargs.get("font", QFont())

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(0, 0, 0, 0)
		self._mainLayout.setSpacing(0)

		self._titleLayout = QHBoxLayout()
		self._titleLayout.setContentsMargins(15, 15, 15, 15)
		label = QLabel("Material", self)
		label.setAlignment(Qt.AlignCenter)
		self._titleLayout.addWidget(label)
		self._mainLayout.addLayout(self._titleLayout)

		self._emissionColorWidget = ColorInputWidget(parent=self, text="Emission", font=self._font,
			callbackSpinChanged = self.emissionColor_onValueChanged, 
			initial_value=self._status.get("panelMaterial.emission"))
		self._mainLayout.addWidget(self._emissionColorWidget)

		self._ambientColorWidget = ColorInputWidget(parent=self, text="Ambient", font=self._font,
			callbackSpinChanged = self.ambientColor_onValueChanged, 
			initial_value=self._status.get("panelMaterial.ambient"))
		self._mainLayout.addWidget(self._ambientColorWidget)

		self._diffuseColorWidget = ColorInputWidget(parent=self, text="Diffuse", font=self._font,
			callbackSpinChanged = self.diffuseColor_onValueChanged,
			initial_value=self._status.get("panelMaterial.diffuse"))
		self._mainLayout.addWidget(self._diffuseColorWidget)

		self._specularColorWidget = ColorInputWidget(parent=self, text="Specular", font=self._font,
			callbackSpinChanged = self.specularColor_onValueChanged,
			initial_value=self._status.get("panelMaterial.specular"))
		self._mainLayout.addWidget(self._specularColorWidget)

		self._shininessLayout = QHBoxLayout()
		self._shininessLayout.setContentsMargins(0, 0, 0, 0)
		self._shininessLayout.setSpacing(0)
		label = QLabel("Shininess: ", self)
		label.setFont(self._font)
		self._shininessSpin = QDoubleSpinBox(self)
		self._shininessSpin.setRange(0.0, 30.0)
		self._shininessSpin.setSingleStep(1.0)
		self._shininessSpin.setValue(self._status.get("panelMaterial.shininess"))
		self._shininessSpin.valueChanged.connect(self.shininessSpin_onValueChanged)
		self._shininessLayout.addWidget(label)
		self._shininessLayout.addWidget(self._shininessSpin)
		self._mainLayout.addLayout(self._shininessLayout)

		self._mainLayout.setAlignment(Qt.AlignTop)
		self.setLayout(self._mainLayout)

	def emissionColor_onValueChanged(self, value):
		self._renderer.setBPMaterialEmission(value)
		self._status.set("panelMaterial.emission", value)

	def ambientColor_onValueChanged(self, value):
		self._renderer.setBPMaterialAmbientColor(value)
		self._status.set("panelMaterial.ambient", value)

	def diffuseColor_onValueChanged(self, value):
		self._renderer.setBPMaterialDiffuseColor(value)
		self._status.set("panelMaterial.diffuse", value)

	def specularColor_onValueChanged(self, value):
		self._renderer.setBPMaterialSpecularColor(value)
		self._status.set("panelMaterial.specular", value)

	def shininessSpin_onValueChanged(self, value):
		self._renderer.setBPMaterialShininess(value)
		self._status.set("panelMaterial.shininess", value)
