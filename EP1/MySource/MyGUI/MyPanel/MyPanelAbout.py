from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class MyPanelAbout(QWidget):
	def __init__(self, parent=None, renderer=None, **kwargs):
		super(MyPanelAbout, self).__init__(parent)

		self._font = kwargs.get("font", QFont())		
		self._renderer = renderer

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(2,0,0,0)
		self._mainLayout.setSpacing(2)
		self._mainLayout.setAlignment(Qt.AlignTop)
		
		self._titleLayout = QHBoxLayout()
		self._titleLayout.setContentsMargins(15,15,15,15)
		label = QLabel("About", self)
		label.setAlignment(Qt.AlignCenter)
		self._titleLayout.addWidget(label)
		self._mainLayout.addLayout(self._titleLayout)


		text = """
		<p>Application developed by Dennis José da Silva as the first assigment of the course
		MAC6913 - topics in computer graphics <p>

		<p>The icons used in this application are downloaded from www.flaticon.com as credited below:</p>

		<ul>
			<li> original name: Geometry. Used on menu: 3D Model. Designed by Smashicons from www.flaticon.com </li>
			<li> original name: Cube. Used on menu: BRDF: Designed by Smashicons from www.flaticon.com </li>
			<li> original name: light. Used on menu light: Designed by Freepik from www.flaticon.com </li>
			<li> original name: big-data. Used on menu: material: Designed by Dinosoft from www.flaticon.com </li>
			<li> original name: enhance-effect. Used on menu effect: Designed by Freepik from www.flaticon.com </li>
			<li> original name: information. Used on menu about: Designed by Good Ware from www.flaticon.com </li>
		</ul>
		"""


		self._textLayout = QHBoxLayout()
		self._textLayout.setContentsMargins(15,15,15,15)
		label = QTextBrowser(self)			
		label.setText(text)
		#label.setWordWrap(True)	
		self._textLayout.addWidget(label)
		self._mainLayout.addLayout(self._textLayout)

#		self._scroll = QScrollArea(label)
#		self._scroll.setWidget(label)
#		self._textLayout.addWidget(self._scroll)

		self.setLayout(self._mainLayout)