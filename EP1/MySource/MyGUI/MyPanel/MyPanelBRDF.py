from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class MyPanelBRDF(QWidget):
	def __init__(self, parent=None, renderer=None, **kwargs):
		super(MyPanelBRDF, self).__init__(parent)

		self._font = kwargs.get("font", QFont())
		self._renderer = renderer

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(2,0,0,0)
		self._mainLayout.setSpacing(2)
		self._titleLayout = QHBoxLayout()
		self._titleLayout.setContentsMargins(15,15,15,15)

		label = QLabel("BRDF", self)
		label.setAlignment(Qt.AlignCenter)
		self._titleLayout.addWidget(label)
		self._mainLayout.addLayout(self._titleLayout)

		self._comboLayout = QHBoxLayout()
		self._comboLayout.setContentsMargins(15,0,0,0)
		self._comboLayout.setSpacing(0)

		label = QLabel("Select BRDF: ")
		self._comboLayout.addWidget(label)
		self._brdfCombo = QComboBox(self)
		self._brdfCombo.addItem("Blinn-Phong")
		self._brdfCombo.addItem("Epic")
		self._brdfCombo.setEnabled(False)
		self._comboLayout.addWidget(self._brdfCombo)
		self._mainLayout.addLayout(self._comboLayout)
		

		text = """
		<div style='color: rgb(255,0,0);'> 
			<p>
				This panel should change the BRDFs coded in the shaders (coded for the assigments of the course: 
				MAC5744 Introduction to Computer Graphics):
			</p>

			<ul>
				<li>Blinn-Phong: Classical Blinn-Phong model as coded by the course instructor. </li>
				<li>
					Epic: Model described by a paper for Epic games as presented in the assignment #1 from
					MAC5744 Introduction to Computer Graphics.
				</li>
			</ul>

			<p>
			Since the Epic BRDF is not being evaluated in this assignment and due to the refactoring 
			I have made which has taken much of time spent in this assignment, I disabled this feature.
			Even though the application does not support Epic BRDF, the framework does, thus, the code
			for the BRDF is in the source code of the application.
			</p>
		</div>
		"""
		self._textLayout = QHBoxLayout()
		self._textLayout.setContentsMargins(10, 10, 10, 10)
		textBrowser = QTextBrowser(self)
		textBrowser.setText(text)
		self._textLayout.addWidget(textBrowser)
		self._mainLayout.addLayout(self._textLayout)

		self._mainLayout.setAlignment(Qt.AlignTop)
		self.setLayout(self._mainLayout)