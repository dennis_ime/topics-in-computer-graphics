from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from MySource.MyGUI.ApplicationStatus import ApplicationStatus
from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect


class SpikyEffectWidget(QWidget):	
	def __init__(self, parent=None, renderer=None, **kwargs):
		super(SpikyEffectWidget, self).__init__(parent)
		
		self._status = ApplicationStatus.instance()
		self._font = kwargs.get("font", QFont())
		self._renderer = renderer

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(0, 0, 0, 0)
		self._mainLayout.setSpacing(0)

		self._titleLayout = QHBoxLayout()
		self._titleLayout.setContentsMargins(15, 15, 15, 15)
		label = QLabel("Spiky Effect", self)
		label.setFont(self._font)
		label.setAlignment(Qt.AlignCenter)
		self._titleLayout.addWidget(label)
		self._mainLayout.addLayout(self._titleLayout)

		self._spikyLayout = QHBoxLayout()
		self._spikyLayout.setContentsMargins(2,0,0,0)
		self._spikyLayout.setSpacing(0)
		self._spikychk = QCheckBox("spiky", self)		
		self._spikychk.setChecked(self._status.get("panelEffect.Spiky.spikychk"))
		self._spikychk.stateChanged.connect(self.spikychk_onStateChanged)
		self._spikyLayout.addWidget(self._spikychk)
		self._mainLayout.addLayout(self._spikyLayout)

		self._widthLayout = QHBoxLayout()
		self._widthLayout.setContentsMargins(0,0,0,0)
		self._widthLayout.setSpacing(0)
		label = QLabel("Spiky Width: ", self)
		label.setFont(self._font)
		self._widthLayout.addWidget(label)
		self._widthSpin = QDoubleSpinBox(self)
		self._widthSpin.setRange(0.01, 5.0)
		self._widthSpin.setSingleStep(0.05)
		self._widthSpin.setValue(self._status.get("panelEffect.Spiky.width"))
		self._widthSpin.setEnabled(self._spikychk.checkState())
		self._widthSpin.valueChanged.connect(self.widthSpin_onValueChanged)
		self._widthLayout.addWidget(self._widthSpin)
		self._mainLayout.addLayout(self._widthLayout)

		self._heightLayout = QHBoxLayout()
		self._heightLayout.setContentsMargins(0,0,0,0)
		self._heightLayout.setSpacing(0)
		label = QLabel("Spiky Height: ", self)
		label.setFont(self._font)
		self._heightLayout.addWidget(label)
		self._heightSpin = QDoubleSpinBox(self)
		self._heightSpin.setRange(0.01, 5.0)
		self._heightSpin.setSingleStep(0.05)
		self._heightSpin.setValue(self._status.get("panelEffect.Spiky.height"))
		self._heightSpin.setEnabled(self._spikychk.checkState())
		self._heightSpin.valueChanged.connect(self.heightSpin_onValueChanged)
		self._heightLayout.addWidget(self._heightSpin)
		self._mainLayout.addLayout(self._heightLayout)

		self.setLayout(self._mainLayout)
		
	def spikychk_onStateChanged(self, state):
		if state == Qt.Unchecked:
			self._widthSpin.setEnabled(False)
			self._heightSpin.setEnabled(False)
			self._renderer.setGeometryShaderEffect(GeometryShaderEffect.DEFAULT)
			self._status.set("panelEffect.Spiky.spikychk", False)
		elif state == Qt.Checked:
			self._widthSpin.setEnabled(True)
			self._heightSpin.setEnabled(True)
			self._renderer.setGeometryShaderEffect(GeometryShaderEffect.SPIKY)
			self._renderer.setSpikyHeight(self._heightSpin.value())
			self._renderer.setSpikyWidth(self._widthSpin.value())			
			self._status.set("panelEffect.Spiky.spikychk", True)

	def heightSpin_onValueChanged(self):		
		self._renderer.setSpikyHeight(self._heightSpin.value())
		self._status.set("panelEffect.Spiky.height", self._heightSpin.value())

	def widthSpin_onValueChanged(self):		
		self._renderer.setSpikyWidth(self._widthSpin.value())
		self._status.set("panelEffect.Spiky.width", self._widthSpin.value())



class MyPanelEffect(QWidget):
	def __init__(self, parent=None, renderer=None, **kwargs):
		super(MyPanelEffect, self).__init__(parent)

		self._status = ApplicationStatus.instance()
		self._font = kwargs.get("font", QFont())
		self._renderer = renderer

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(2,0,0,0)
		self._mainLayout.setSpacing(0)

		self._titleLayout = QHBoxLayout()
		self._titleLayout.setContentsMargins(15, 15, 15, 15)
		label = QLabel("Effects", self)
		label.setFont(self._font)
		label.setAlignment(Qt.AlignCenter)
		self._titleLayout.addWidget(label)
		self._mainLayout.addLayout(self._titleLayout)

		self._spikyEffectWidget = SpikyEffectWidget(parent=self, renderer=self._renderer)
		self._mainLayout.addWidget(self._spikyEffectWidget)

		self._mainLayout.setAlignment(Qt.AlignTop)
		self.setLayout(self._mainLayout)


