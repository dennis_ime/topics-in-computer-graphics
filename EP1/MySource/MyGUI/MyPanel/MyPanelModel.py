from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from MySource.MyGUI.ApplicationStatus import ApplicationStatus
from Source.Graphics.Material import Material

class TorusWidget(QWidget):
	
	def __init__(self, parent, renderer=None, **kwargs):
		super(TorusWidget, self).__init__(parent)
		self._status = ApplicationStatus.instance()
		self._font = kwargs.get("font", QFont())
		self._renderer = renderer

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(0, 0, 0, 0)
		self._mainLayout.setSpacing(0)

		self._latResolutionLayout = QHBoxLayout()
		self._latResolutionLayout.setContentsMargins(0, 0, 0, 0)
		self._latResolutionLayout.setSpacing(0)
		label = QLabel("Latitude Resolution: ", self)
		label.setFont(self._font)
		self._latResolutionLayout.addWidget(label)
		self._latResolutionSpin = QSpinBox(self)
		self._latResolutionSpin.setRange(5, 95)
		self._latResolutionSpin.setSingleStep(5)
		self._latResolutionSpin.setValue(self._status.get("panelModel.Torus.latResolution"))
		self._latResolutionLayout.addWidget(self._latResolutionSpin)
		self._mainLayout.addLayout(self._latResolutionLayout)

		self._lngResolutionLayout = QHBoxLayout()
		self._lngResolutionLayout.setContentsMargins(0, 0, 0, 0)
		self._lngResolutionLayout.setSpacing(0)
		label = QLabel("Longitude Resolution: ", self)
		label.setFont(self._font)
		self._lngResolutionSpin = QSpinBox(self)
		self._lngResolutionSpin.setRange(5, 95)
		self._lngResolutionSpin.setSingleStep(5)
		self._lngResolutionSpin.setValue(self._status.get("panelModel.Torus.lngResolution"))
		self._lngResolutionLayout.addWidget(label)
		self._lngResolutionLayout.addWidget(self._lngResolutionSpin)
		self._mainLayout.addLayout(self._lngResolutionLayout)

		self._outerRadiusLayout = QHBoxLayout()
		self._outerRadiusLayout.setContentsMargins(0, 0, 0, 0)
		self._outerRadiusLayout.setSpacing(0)
		label = QLabel("Outer Radius: ", self)
		label.setFont(self._font)
		self._outerRadiusSpin = QDoubleSpinBox(self)
		self._outerRadiusSpin.setRange(0.1, 5.0)
		self._outerRadiusSpin.setSingleStep(0.2)
		self._outerRadiusSpin.setValue(self._status.get("panelModel.Torus.outerRadius"))
		self._outerRadiusLayout.addWidget(label)
		self._outerRadiusLayout.addWidget(self._outerRadiusSpin)
		self._mainLayout.addLayout(self._outerRadiusLayout)

		self._innerRadiusLayout = QHBoxLayout()
		self._innerRadiusLayout.setContentsMargins(0, 0, 0, 0)
		self._innerRadiusLayout.setSpacing(0)
		label = QLabel("Inner Radius", self)
		label.setFont(self._font)
		self._innerRadiusInnerSpin = QDoubleSpinBox(self)
		self._innerRadiusInnerSpin.setRange(0.1, 5.0)
		self._innerRadiusInnerSpin.setSingleStep(0.2)
		self._innerRadiusInnerSpin.setValue(self._status.get("panelModel.Torus.innerRadius"))
		self._innerRadiusLayout.addWidget(label)
		self._innerRadiusLayout.addWidget(self._innerRadiusInnerSpin)
		self._mainLayout.addLayout(self._innerRadiusLayout)

		self._latResolutionSpin.valueChanged.connect(self.latResolutionSpin_onValueChanged)
		self._lngResolutionSpin.valueChanged.connect(self.lgnResolutionSpin_onValueChanged)
		self._outerRadiusSpin.valueChanged.connect(self.outerRadiusSpin_onValueChanged)
		self._innerRadiusInnerSpin.valueChanged.connect(self.innerRadiusSpin_onValueChanged)
		self.updateTorusProperties()
		self.setLayout(self._mainLayout)

	def latResolutionSpin_onValueChanged(self):
		self._status.set("panelModel.Torus.latResolution", self._latResolutionSpin.value())
		self.updateTorusProperties()

	def lgnResolutionSpin_onValueChanged(self):
		self._status.set("panelModel.Torus.lngResolution", self._lngResolutionSpin.value())
		self.updateTorusProperties()

	def innerRadiusSpin_onValueChanged(self):
		self._status.set("panelModel.Torus.innerRadius", self._innerRadiusInnerSpin.value())
		self.updateTorusProperties()

	def outerRadiusSpin_onValueChanged(self):
		self._status.set("panelModel.Torus.outerRadius", self._outerRadiusSpin.value())
		self.updateTorusProperties()

	def updateTorusProperties(self):
		props = {
			"outerRadius": self._outerRadiusSpin.value(),
			"innerRadius": self._innerRadiusInnerSpin.value(),
			"nlat": self._latResolutionSpin.value(),
			"nlng": self._lngResolutionSpin.value()
		}
		material = Material(emission=self._status.get("panelMaterial.emission"),
				ambient=self._status.get("panelMaterial.ambient"),
				diffuse=self._status.get("panelMaterial.diffuse"),
				specular=self._status.get("panelMaterial.specular"),
				shininess=self._status.get("panelMaterial.shininess"))

		self._renderer.changeActor(5, material=material, pargs=props)

	def resetApplicationStatus(self):
		self._status.set("panelModel.Torus.latResolution", 10)
		self._status.set("panelModel.Torus.lngResolution", 10)
		self._status.set("panelModel.Torus.outerRadius", 1.0)
		self._status.set("panelModel.Torus.innerRadius", 0.5)


class MyPanelModel(QWidget):
	def __init__(self, parent=None, renderer=None, **kwargs):
		super(MyPanelModel, self).__init__(parent)
	
		self._status = ApplicationStatus.instance()		
		self._font = kwargs.get("font", QFont())
		self._renderer = renderer

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(2,0,0,0)
		self._mainLayout.setSpacing(2)
		
		self._titleLayout = QHBoxLayout()
		self._titleLayout.setContentsMargins(15,15,15,15)

		label = QLabel("Model 3D", self)
		label.setAlignment(Qt.AlignCenter)
		self._titleLayout.addWidget(label)
		self._mainLayout.addLayout(self._titleLayout)

		self._comboLayout = QHBoxLayout()
		self._comboLayout.setContentsMargins(15,0,0,0)
		self._comboLayout.setSpacing(0)

		label = QLabel("Select Model: ")
		self._comboLayout.addWidget(label)
		self._modelCombo = QComboBox(self)
		self._modelCombo.addItem("Cube")
		self._modelCombo.addItem("Cone")
		self._modelCombo.addItem("Cylinder")
		self._modelCombo.addItem("Sphere")
		self._modelCombo.addItem("Teapot")
		self._modelCombo.addItem("Torus")
		self._modelCombo.activated.connect(self.modelCombo_onClick)
		self._modelCombo.setCurrentIndex(self._status.get("panelModel.modelCombo"))
		self._comboLayout.addWidget(self._modelCombo)

		self._modelWidgetLayout = QHBoxLayout()
		self._modelWidgetLayout.setContentsMargins(15,15,15,15)
		self._modelWidgetLayout.setSpacing(0)
		self.initModelPanel(self._modelCombo.currentIndex())

		self._mainLayout.setAlignment(Qt.AlignTop)
		self._mainLayout.addLayout(self._comboLayout)
		self._mainLayout.addLayout(self._modelWidgetLayout)		
		self.setLayout(self._mainLayout)

	def modelCombo_onClick(self, index):
		self.initModelPanel(index)	
		self._status.set("panelModel.modelCombo", index)
	
	def initModelPanel(self, index):
		if index == 5:
			modelWidget = TorusWidget(parent=self, renderer=self._renderer, font=self._font)
			self._modelWidgetLayout.addWidget(modelWidget)
		else:
			item = self._modelWidgetLayout.takeAt(0)
			if item is not None:
				w = item.widget()			
				w.setParent(None)
				w.deleteLater()

			material = Material(emission=self._status.get("panelMaterial.emission"),
				ambient=self._status.get("panelMaterial.ambient"),
				diffuse=self._status.get("panelMaterial.diffuse"),
				specular=self._status.get("panelMaterial.specular"),
				shininess=self._status.get("panelMaterial.shininess"))

			self._renderer.changeActor(index, material=material)
