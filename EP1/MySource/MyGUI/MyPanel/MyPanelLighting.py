from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from MySource.MyGUI.ApplicationStatus import ApplicationStatus
from MySource.MyGUI.ColorInputWidget import ColorInputWidget
from MySource.Graphics.MyLight import MyLightType

class PositionWidget(QWidget):

	def createSpinWithLabel(self, min, max, step, value):
		spin = QDoubleSpinBox(self)
		spin.setRange(min, max)
		spin.setSingleStep(step)
		spin.setValue(value)
		return spin

	def __init__(self, parent=None, renderer=None, text="", **kwargs):
		super(PositionWidget, self).__init__(parent)
		self._parent = parent
		self._renderer = renderer
		self._font = kwargs.get("font", QFont())
		self._min = kwargs.get("min", -1.0)
		self._max = kwargs.get("max", 1.0)
		self._step = kwargs.get("step", 0.05)
		self._xvalue = kwargs.get("xvalue", 0.0)
		self._yvalue = kwargs.get("yvalue", 0.0)
		self._zvalue = kwargs.get("zvalue", 0.0)
		self._callbackValueChanged = kwargs.get("callbackValueChaged", None)

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(0, 0, 0, 0)
		self._mainLayout.setSpacing(0)

		self._positionLayout = QHBoxLayout()
		self._positionLayout.setContentsMargins(0, 0, 0, 0)
		self._positionLayout.setSpacing(0)

		label = QLabel(text, self)
		label.setFont(self._font)
		self._positionLayout.addWidget(label)
		label = QLabel(" X: ")
		label.setFont(self._font)
		self._positionLayout.addWidget(label)
		self._positionXSpin = self.createSpinWithLabel(max=self._max, min=self._min, step=self._step, value=self._xvalue)
		self._positionXSpin.valueChanged.connect(self.anySpin_onValueChanged)
		self._positionLayout.addWidget(self._positionXSpin)

		label = QLabel(" Y: ", self)
		label.setFont(self._font)
		self._positionLayout.addWidget(label)
		self._positionYSpin = self.createSpinWithLabel(max=self._max, min=self._min, step=self._step, value=self._yvalue)
		self._positionYSpin.valueChanged.connect(self.anySpin_onValueChanged)
		self._positionLayout.addWidget(self._positionYSpin)

		label = QLabel(" Z: ", self)
		label.setFont(self._font)
		self._positionLayout.addWidget(label)
		self._positionZSpin = self.createSpinWithLabel(max=self._max, min=self._min, step=self._step, value=self._zvalue)
		self._positionZSpin.valueChanged.connect(self.anySpin_onValueChanged)
		self._positionLayout.addWidget(self._positionZSpin)
		self._mainLayout.addLayout(self._positionLayout)

		self.setLayout(self._mainLayout)	

	def anySpin_onValueChanged(self):
		if self._callbackValueChanged is not None:
			value = QVector3D(self._positionXSpin.value(), self._positionYSpin.value(), self._positionZSpin.value())
			self._callbackValueChanged(value)

class PointLightPanel(QWidget):
	def __init__(self, parent=None, renderer=None, **kwargs):
		super(PointLightPanel, self).__init__(parent)
		self._parent = parent
		self._font = kwargs.get("font", QFont())
		self._renderer = renderer
		self._status = ApplicationStatus.instance()

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(0, 0, 0, 0)
		self._mainLayout.setSpacing(0)

		self._positionLayout = QHBoxLayout()
		self._positionLayout.setContentsMargins(0, 0, 0, 0)
		self._positionLayout.setSpacing(0)
		self._positionWidget = PositionWidget(parent=self, renderer=renderer, text="Position: ", min=-20.0, 
			max=20.0, step=1.0, 
			xvalue=self._status.get("panelLight.pointLight.position").x(), 
			yvalue=self._status.get("panelLight.pointLight.position").y(), 
			zvalue=self._status.get("panelLight.pointLight.position").z(), 
			callbackValueChaged=self.positionSpin_onValueChanged)
		self._positionLayout.addWidget(self._positionWidget)
		self._mainLayout.addLayout(self._positionLayout)

		self._diffuseColorWidget = ColorInputWidget(parent=self, text="Diffuse", font=self._font, 
			callbackSpinChanged=self.diffuseColor_onValueChanged, 
			initial_value=self._status.get("panelLight.pointLight.diffuse"))
		self._mainLayout.addWidget(self._diffuseColorWidget)

		self._specularColorWidget = ColorInputWidget(parent=self, text="Specular", font=self._font,
			callbackSpinChanged=self.specularColor_onValueChanged,
			initial_value=self._status.get("panelLight.pointLight.specular"))
		self._mainLayout.addWidget(self._specularColorWidget)

		self._ambientColorWidget = ColorInputWidget(parent=self, text="Ambient", font=self._font, 
			callbackSpinChanged=self.ambientColor_onValueChanged,
			initial_value=self._status.get("panelLight.pointLight.ambient"))
		self._mainLayout.addWidget(self._ambientColorWidget)

		self._radiusLayout = QHBoxLayout()
		self._radiusLayout.setContentsMargins(0, 0, 0, 0)
		self._radiusLayout.setSpacing(0)
		label = QLabel("Radius: ", self)
		label.setFont(self._font)
		self._radiusSpin = QDoubleSpinBox(self)
		self._radiusSpin.setRange(0.0, 100.0)
		self._radiusSpin.setSingleStep(1.0)
		self._radiusSpin.setValue(self._status.get("panelLight.pointLight.radius"))
		self._radiusSpin.valueChanged.connect(self.radiusSpin_onValueChanged)
		self._radiusLayout.addWidget(label)
		self._radiusLayout.addWidget(self._radiusSpin)
		self._mainLayout.addLayout(self._radiusLayout)
		
		self._constantAttenuationLayout = QHBoxLayout()
		self._constantAttenuationLayout.setContentsMargins(0, 0, 0, 0)
		self._constantAttenuationLayout.setSpacing(0)
		label = QLabel("Constant Attenuation: ", self)
		label.setFont(self._font)
		self._constantAttenuationSpin = QDoubleSpinBox(self)
		self._constantAttenuationSpin.setRange(0.0, 1.0)
		self._constantAttenuationSpin.setSingleStep(0.1)
		self._constantAttenuationSpin.setValue(self._status.get("panelLight.pointLight.constantAttenuation"))
		self._constantAttenuationSpin.valueChanged.connect(self.constantAttenuationSpin_onValueChanged)
		self._constantAttenuationLayout.addWidget(label)
		self._constantAttenuationLayout.addWidget(self._constantAttenuationSpin)
		self._mainLayout.addLayout(self._constantAttenuationLayout)

		self._linearAttenuationLayout = QHBoxLayout()
		self._linearAttenuationLayout.setContentsMargins(0, 0, 0, 0)
		self._linearAttenuationLayout.setSpacing(0)
		label = QLabel("Linear Attenuation: ", self)
		label.setFont(self._font)
		self._linearAttenuationSpin = QDoubleSpinBox(self)
		self._linearAttenuationSpin.setRange(0.0, 1.0)
		self._linearAttenuationSpin.setSingleStep(0.1)
		self._linearAttenuationSpin.setValue(self._status.get("panelLight.pointLight.linearAttenuation"))
		self._linearAttenuationSpin.valueChanged.connect(self.linearAttenuationSpin_onValueChanged)
		self._linearAttenuationLayout.addWidget(label)
		self._linearAttenuationLayout.addWidget(self._linearAttenuationSpin)
		self._mainLayout.addLayout(self._linearAttenuationLayout)

		self._quadraticAttenuationLayout = QHBoxLayout()
		self._quadraticAttenuationLayout.setContentsMargins(0, 0, 0, 0)
		self._quadraticAttenuationLayout.setSpacing(0)
		label = QLabel("Quadratic Attenuation: ", self)
		label.setFont(self._font)
		self._quadraticAttenuationSpin = QDoubleSpinBox(self)
		self._quadraticAttenuationSpin.setRange(0.0, 1.0)
		self._quadraticAttenuationSpin.setSingleStep(0.1)
		self._quadraticAttenuationSpin.setValue(self._status.get("panelLight.pointLight.quadraticAttenuation"))
		self._quadraticAttenuationSpin.valueChanged.connect(self.quadraticAttenuationSpin_onValeChanged)
		self._quadraticAttenuationLayout.addWidget(label)
		self._quadraticAttenuationLayout.addWidget(self._quadraticAttenuationSpin)
		self._mainLayout.addLayout(self._quadraticAttenuationLayout)

		self._headLightChk = QCheckBox("HeadLight", self)
		self._headLightChk.setChecked(self._status.get("panelLight.pointLight.headlight"))
		self._headLightChk.stateChanged.connect(self.headLight_onStateChanged)

		self.setupInitialRendererLight()
		self._mainLayout.addWidget(self._headLightChk)
		self.setLayout(self._mainLayout)

	def positionSpin_onValueChanged(self, value):
		self._status.set("panelLight.pointLight.position", value)
		self._renderer.setPointLightPositionX(value.x())
		self._renderer.setPointLightPositionY(value.y())
		self._renderer.setPointLightPositionZ(value.z())

	def diffuseColor_onValueChanged(self, value):
		self._renderer.setPointLightDiffuseColor(value)
		self._status.set("panelLight.pointLight.diffuse", value)

	def specularColor_onValueChanged(self, value):
		self._renderer.setPointLightSpecularColor(value)
		self._status.set("panelLight.pointLight.specular", value)

	def ambientColor_onValueChanged(self, value):
		self._renderer.setPointLightAmbientColor(value)
		self._status.set("panelLight.pointLight.ambient", value)

	def radiusSpin_onValueChanged(self, value):
		self._renderer.setPointLightRadius(value)
		self._status.set("panelLight.pointLight.radius", value)

	def constantAttenuationSpin_onValueChanged(self, value):
		self._renderer.setPointLightConstantAttenuation(value)
		self._status.set("panelLight.pointLight.constantAttenuation", value)

	def linearAttenuationSpin_onValueChanged(self, value):
		self._renderer.setPointLightLinearAttenuation(value)
		self._status.set("panelLight.pointLight.linearAttenuation", value)

	def quadraticAttenuationSpin_onValeChanged(self, value):
		self._renderer.setPointLightQuadAttenuation(value)
		self._status.set("panelLight.pointLight.quadraticAttenuation", value)

	def headLight_onStateChanged(self, state):
		r = None
		if state == Qt.Unchecked:
			r = False
		elif state == Qt.Checked:
			r = True
		self._renderer.setPointLightHeadlight(r)
		self._status.set("panelLight.pointLight.headlight", r)

	def setupInitialRendererLight(self):
		self._renderer.setPointLightPositionX(self._status.get("panelLight.pointLight.position").x())
		self._renderer.setPointLightPositionY(self._status.get("panelLight.pointLight.position").y())
		self._renderer.setPointLightPositionZ(self._status.get("panelLight.pointLight.position").z())
		self._renderer.setPointLightDiffuseColor(self._status.get("panelLight.pointLight.diffuse"))
		self._renderer.setPointLightSpecularColor(self._status.get("panelLight.pointLight.specular"))
		self._renderer.setPointLightAmbientColor(self._status.get("panelLight.pointLight.ambient"))
		self._renderer.setPointLightRadius(self._status.get("panelLight.pointLight.radius"))
		self._renderer.setPointLightConstantAttenuation(self._status.get("panelLight.pointLight.constantAttenuation"))
		self._renderer.setPointLightLinearAttenuation(self._status.get("panelLight.pointLight.linearAttenuation"))
		self._renderer.setPointLightQuadAttenuation(self._status.get("panelLight.pointLight.quadraticAttenuation"))
		self._renderer.setPointLightHeadlight(self._status.get("panelLight.pointLight.headlight"))

class DirectionalLightPanel(QWidget):	
	def __init__(self, parent=None, renderer=None, **kwargs):
		super(DirectionalLightPanel, self).__init__(parent)
		self._paent = parent
		self._renderer = renderer
		self._font = kwargs.get("font", QFont())
		self._status = ApplicationStatus.instance()

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(0, 0, 0, 0)
		self._mainLayout.setSpacing(0)

		self._direcionalLayout = QHBoxLayout()
		self._direcionalLayout.setContentsMargins(0, 0, 0, 0)
		self._direcionalLayout.setSpacing(0)

		self._directionalWidget = PositionWidget(parent=self, renderer=renderer, text="Dir: ", 
			min=-1.0, max=1.0, step=0.05, 
			callbackValueChaged=self.direction_onValueChanged,
			xvalue=self._status.get("panelLight.directionalLight.direction").x(), 
			yvalue=self._status.get("panelLight.directionalLight.direction").y(), 
			zvalue=self._status.get("panelLight.directionalLight.direction").z())
		self._direcionalLayout.addWidget(self._directionalWidget)
		self._mainLayout.addLayout(self._direcionalLayout)

		self._diffuseColorWidget = ColorInputWidget(parent=self, text="Diffuse", font=self._font,
			callbackSpinChanged=self.diffuseColor_onValueChanged, 
			initial_value=self._status.get("panelLight.directionalLight.diffuse"))
		self._mainLayout.addWidget(self._diffuseColorWidget)

		self._specularColorWidget = ColorInputWidget(parent=self, text="Specular", font=self._font,
			callbackSpinChanged=self.specularColor_onValueChanged, 
			initial_value=self._status.get("panelLight.directionalLight.specular"))
		self._mainLayout.addWidget(self._specularColorWidget)
		
		self._ambientColorWidget = ColorInputWidget(parent=self, text="Ambient", font=self._font,
			callbackSpinChanged=self.ambientColor_onValueChanged, 
			initial_value=self._status.get("panelLight.directionalLight.ambient"))
		self._mainLayout.addWidget(self._ambientColorWidget)

		self._headLightChk = QCheckBox("HeadLight", self)
		self._headLightChk.setChecked(self._status.get("panelLight.directionalLight.headlight"))
		self._headLightChk.stateChanged.connect(self.headLight_onStateChanged)

		self.setupInitialRendererLight()
		self._mainLayout.addWidget(self._headLightChk)
		self.setLayout(self._mainLayout)

	def direction_onValueChanged(self, value):
		self._status.set("panelLight.directionalLight.direction", value)
		self._renderer.setDirectionalLightDirectionX(value.x())
		self._renderer.setDirectionalLightDirectionY(value.y())
		self._renderer.setDirectionalLightDirectionZ(value.z())

	def diffuseColor_onValueChanged(self, value):
		self._status.set("panelLight.directionalLight.diffuse", value)
		self._renderer.setDirectionalLightDiffuseColor(value)

	def specularColor_onValueChanged(self, value):
		self._status.set("panelLight.directionalLight.specular", value)
		self._renderer.setDirectionalLightSpecularColor(value)

	def ambientColor_onValueChanged(self, value):
		self._status.set("panelLight.directionalLight.ambient", value)
		self._renderer.setDirectionalLightAmbientColor(value)

	def headLight_onStateChanged(self, state):
		r = None
		if state == Qt.Unchecked:
			r = False
		elif state == Qt.Checked:
			r = True
		self._renderer.setPointLightHeadlight(r)
		self._status.set("panelLight.directionalLight.headlight", r)

	def setupInitialRendererLight(self):
		self._renderer.setDirectionalLightDirectionX(self._status.get("panelLight.directionalLight.direction").x())
		self._renderer.setDirectionalLightDirectionY(self._status.get("panelLight.directionalLight.direction").y())
		self._renderer.setDirectionalLightDirectionZ(self._status.get("panelLight.directionalLight.direction").z())
		self._renderer.setDirectionalLightAmbientColor(self._status.get("panelLight.directionalLight.ambient"))
		self._renderer.setDirectionalLightDiffuseColor(self._status.get("panelLight.directionalLight.diffuse"))
		self._renderer.setDirectionalLightSpecularColor(self._status.get("panelLight.directionalLight.specular"))
		self._renderer.setDirectionalLightHeadlight(self._status.get("panelLight.directionalLight.headlight"))


class HemisphericalLightPanel(QWidget):
	def __init__(self, parent=None, renderer=None, **kwargs):
		super(HemisphericalLightPanel, self).__init__(parent)
		self._parent = parent
		self._renderer = renderer
		self._font = kwargs.get("font", QFont())
		self._status = ApplicationStatus.instance()

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(0,0,0,0)
		self._mainLayout.setSpacing(0)

		self._groundTitleLayout = QHBoxLayout()
		self._groundTitleLayout.setContentsMargins(15,15,15,15)
		self._groundTitleLayout.setSpacing(0)
		self._groundTitleLayout.setAlignment(Qt.AlignCenter)
		label = QLabel("Ground", self)
		label.setFont(self._font)
		self._groundTitleLayout.addWidget(label)
		self._mainLayout.addLayout(self._groundTitleLayout)

		self._groundDiffuseColorWidget = ColorInputWidget(parent=self, text="Diffuse", font=self._font,
			callbackSpinChanged=self.groundDiffuseColor_onValueChanged,
			initial_value=self._status.get("panelLight.hemisphericalLight.groundDiffuse"))
		self._mainLayout.addWidget(self._groundDiffuseColorWidget)
		
		self._groundSpecularColorWidget = ColorInputWidget(parent=self, text="Specular", font=self._font,
			callbackSpinChanged=self.groundSpecularColor_onValueChanged,
			initial_value=self._status.get("panelLight.hemisphericalLight.groundSpecular"))
		self._mainLayout.addWidget(self._groundSpecularColorWidget)

		self._groundAmbientColorWidget = ColorInputWidget(parent=self, text="Ambient", font=self._font,
			callbackSpinChanged=self.groundAmbientColor_onValueChanged,
			initial_value=self._status.get("panelLight.hemisphericalLight.groundAmbient"))
		self._mainLayout.addWidget(self._groundAmbientColorWidget)

		self._skyTitleLayout = QHBoxLayout()
		self._skyTitleLayout.setContentsMargins(15,15,15,15)
		self._skyTitleLayout.setSpacing(0)
		self._skyTitleLayout.setAlignment(Qt.AlignCenter)
		label = QLabel("Sky", self)
		label.setFont(self._font)
		self._skyTitleLayout.addWidget(label)
		self._mainLayout.addLayout(self._skyTitleLayout)

		self._skyDiffuseColorWidget = ColorInputWidget(parent=self, text="Diffuse", font=self._font,
			callbackSpinChanged=self.skyDiffuseColor_onValueChanged,
			initial_value=self._status.get("panelLight.hemisphericalLight.skyDiffuse"))
		self._mainLayout.addWidget(self._skyDiffuseColorWidget)

		self._skySpecularColorWidget = ColorInputWidget(parent=self, text="Specular", font=self._font,
			callbackSpinChanged=self.skySpecularColor_onValueChanged,
			initial_value=self._status.get("panelLight.hemisphericalLight.skySpecular"))
		self._mainLayout.addWidget(self._skySpecularColorWidget)

		self._skyAmbientColorWidget = ColorInputWidget(parent=self, text="Ambient", font=self._font,
			callbackSpinChanged=self.skyAmbientColor_onValueChanged,
			initial_value=self._status.get("panelLight.hemisphericalLight.skyAmbient"))
		self._mainLayout.addWidget(self._skyAmbientColorWidget)

		self.setupInitialRendererLight()
		self.setLayout(self._mainLayout)

	def groundDiffuseColor_onValueChanged(self, value):
		self._renderer.setHemisphericalLightGroundDiffuseColor(value)
		self._status.set("panelLight.hemisphericalLight.groundDiffuse", value)

	def groundSpecularColor_onValueChanged(self, value):
		self._renderer.setHemisphericalLightGroundSpecularColor(value)
		self._status.set("panelLight.hemisphericalLight.groundSpecular", value)

	def groundAmbientColor_onValueChanged(self, value):
		self._renderer.setHemisphericalLightGroundAmbientColor(value)
		self._status.set("panelLight.hemisphericalLight.groundSpecular", value)

	def skyDiffuseColor_onValueChanged(self, value):
		self._renderer.setHemisphericalLightSkyDiffuseColor(value)
		self._status.set("panelLight.hemisphericalLight.skyDiffuse", value)

	def skySpecularColor_onValueChanged(self, value):
		self._renderer.setHemisphericalLightSkySpecularColor(value)
		self._status.set("panelLight.hemisphericalLight.skySpecular", value)

	def skyAmbientColor_onValueChanged(self, value):
		self._renderer.setHemisphericalLightSkyAmbientColor(value)
		self._status.set("panelLight.hemisphericalLight.skyAmbient", value)

	def setupInitialRendererLight(self):
		self._renderer.setHemisphericalLightSkyDiffuseColor(self._status.get("panelLight.hemisphericalLight.skyDiffuse"))
		self._renderer.setHemisphericalLightSkySpecularColor(self._status.get("panelLight.hemisphericalLight.skySpecular"))
		self._renderer.setHemisphericalLightSkyAmbientColor(self._status.get("panelLight.hemisphericalLight.skyAmbient"))
		self._renderer.setHemisphericalLightGroundDiffuseColor(
			self._status.get("panelLight.hemisphericalLight.groundDiffuse"))
		self._renderer.setHemisphericalLightGroundSpecularColor(
			self._status.get("panelLight.hemisphericalLight.groundSpecular"))
		self._renderer.setHemisphericalLightGroundAmbientColor(
			self._status.get("panelLight.hemisphericalLight.groundAmbient"))
	
class MyPanelLighting(QWidget):
	def __init__(self, parent, renderer=None, **kwargs):
		super(MyPanelLighting, self).__init__(parent)
		self._status = ApplicationStatus.instance()
		self._font = kwargs.get("font", QFont())
		self._renderer = renderer

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(0, 0, 0, 0)
		self._mainLayout.setSpacing(0)

		self._titleLayout = QHBoxLayout()
		self._titleLayout.setContentsMargins(15,15,15, 15)
		label = QLabel("Lighting", self)
		label.setAlignment(Qt.AlignCenter)
		self._titleLayout.addWidget(label)
		self._mainLayout.addLayout(self._titleLayout)

		self._comboLayout = QHBoxLayout()
		self._comboLayout.setContentsMargins(15, 0, 0, 0)
		self._comboLayout.setSpacing(0)
		label = QLabel("Select Light: ", self)
		self._comboLayout.addWidget(label)
		self._lightCombo = QComboBox(self)
		self._lightCombo.addItem("Point Light")
		self._lightCombo.addItem("Directional Light")
		self._lightCombo.addItem("Hemispherical Light")
		self._lightCombo.setCurrentIndex(self._status.get("panelLight.lightCombo"))
		self._comboLayout.addWidget(self._lightCombo)
		self._lightCombo.activated.connect(self.lightCombo_onClick)
		self._mainLayout.addLayout(self._comboLayout)

		self._lightLayout = QHBoxLayout()
		self._lightLayout.setContentsMargins(0,0,0,0)
		self._lightLayout.setSpacing(0)

		self._lightLayout.addWidget(self.getLightPanel(self._lightCombo.currentIndex()))

		self._mainLayout.addLayout(self._lightLayout)
		self._mainLayout.setAlignment(Qt.AlignTop)
		self.setLayout(self._mainLayout)

	def getLightPanel(self, index):		
		self._renderer.changeBPLight(index)
		if index == 0:
			return PointLightPanel(parent=self, renderer=self._renderer)
		elif index == 1:
			return DirectionalLightPanel(parent=self, renderer=self._renderer)
		elif index == 2:
			return HemisphericalLightPanel(parent=self, renderer=self._renderer)

	def lightCombo_onClick(self, index):
		lightwidget = self._lightLayout.takeAt(0).widget()
		lightwidget.setParent(None) 
		lightwidget.deleteLater()
		lightwidget = self.getLightPanel(index)
		self._status.set("panelLight.lightCombo", index)
		self._lightLayout.addWidget(lightwidget)