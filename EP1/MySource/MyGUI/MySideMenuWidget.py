from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from MySource.MyGUI.MyPanel.MyPanelBRDF import MyPanelBRDF
from MySource.MyGUI.MyPanel.MyPanelModel import MyPanelModel
from MySource.MyGUI.MyPanel.MyPanelAbout import MyPanelAbout
from MySource.MyGUI.MyPanel.MyPanelEffect import MyPanelEffect
from MySource.MyGUI.MyPanel.MyPanelLighting import MyPanelLighting
from MySource.MyGUI.MyPanel.MyPanelMaterial import MyPanelMaterial


class Panel:
	COLLAPSED = 0
	SHADING = 1
	MODEL = 2
	LIGHT = 3
	MATERIAL = 4
	EFFECT = 5
	ABOUT = 6
	Types = [COLLAPSED, SHADING, MODEL, LIGHT, MATERIAL, EFFECT, ABOUT]


class MySideMenuWidget(QWidget):
	def __init__(self, parent, renderer=None, **kwargs):
		super(MySideMenuWidget, self).__init__()
		
		self._renderer = renderer

		self._parent = parent
		self._font = kwargs.get("font", QFont())
		self._panel = Panel.COLLAPSED
		self._currentButtonPressed = None

		self._panelLayout = QVBoxLayout()
		self._panelLayout.setContentsMargins(0, 0, 0, 0)
		self._panelLayout.setSpacing(0)

		self._mainLayout = QHBoxLayout()
		self._mainLayout.setContentsMargins(0, 0, 0, 0)
		self._mainLayout.setSpacing(0)

		self._buttonsLayout = QVBoxLayout()
		self._buttonsLayout.setContentsMargins(0, 0, 0, 0)
		self._buttonsLayout.setSpacing(0)

		self._brdfButton = QPushButton(self)
		icon = QPixmap("MySource/MyGUI/Icons/cube.png")
		self._brdfButton.setIcon(QIcon(icon))
		self._brdfButton.setIconSize(icon.rect().size())
		self._brdfButton.setFixedSize(QSize(66,66))
		self._brdfButton.setToolTip("BRDF")
		self._brdfButton.clicked.connect(self.btnShading_onClick)
		self._buttonsLayout.addWidget(self._brdfButton)

		self._modelButton = QPushButton(self)
		icon = QPixmap("MySource/MyGUI/Icons/geometry.png")
		self._modelButton.setIcon(QIcon(icon))
		self._modelButton.setIconSize(icon.rect().size())
		self._modelButton.setFixedSize(QSize(66,66))
		self._modelButton.setToolTip("3D Model")
		self._modelButton.clicked.connect(self.btnModel_onClick)
		self._buttonsLayout.addWidget(self._modelButton)

		self._lightButton = QPushButton(self)
		icon = QPixmap("MySource/MyGUI/Icons/light-bulb.png")
		self._lightButton.setIcon(QIcon(icon))
		self._lightButton.setIconSize(icon.rect().size())
		self._lightButton.setFixedSize(QSize(66,66))
		self._lightButton.setToolTip("Lighting")
		self._lightButton.clicked.connect(self.btnLighting_onClick)
		self._buttonsLayout.addWidget(self._lightButton)

		self._materialButton = QPushButton(self)
		icon = QPixmap("MySource/MyGUI/Icons/material.png")
		self._materialButton.setIcon(QIcon(icon))
		self._materialButton.setIconSize(icon.rect().size())
		self._materialButton.setFixedSize(QSize(66,66))
		self._materialButton.setToolTip("Material")
		self._materialButton.clicked.connect(self.btnMaterial_onClick)
		self._buttonsLayout.addWidget(self._materialButton)

		self._effectButton = QPushButton(self)
		icon = QPixmap("MySource/MyGUI/Icons/effect.png")
		self._effectButton.setIcon(QIcon(icon))
		self._effectButton.setIconSize(icon.rect().size())
		self._effectButton.setFixedSize(QSize(66,66))
		self._effectButton.setToolTip("Effect (normal mapping and spiky geometry)")
		self._effectButton.clicked.connect(self.btnEffect_onClick)
		self._buttonsLayout.addWidget(self._effectButton)

		self._aboutButton = QPushButton(self)
		icon = QPixmap("MySource/MyGUI/Icons/information.png")
		self._aboutButton.setIcon(QIcon(icon))
		self._aboutButton.setIconSize(icon.rect().size())
		self._aboutButton.setFixedSize(QSize(66,66))
		self._aboutButton.setToolTip("About")
		self._aboutButton.clicked.connect(self.btnAbout_onClick)
		self._buttonsLayout.addWidget(self._aboutButton)

		self.setFixedWidth(66)
		self._buttonsLayout.setAlignment(Qt.AlignTop)
		self._mainLayout.addLayout(self._buttonsLayout)
		self.setLayout(self._mainLayout)

	def btnShading_onClick(self):		
		if self._panel == Panel.SHADING:
			self._brdfButton.setStyleSheet(" ")
			self.cleanPanel()
		else:
			if self._panel != Panel.COLLAPSED:
				self._currentButtonPressed.setStyleSheet(" ")
				self.cleanPanel()

			self._currentButtonPressed = self._brdfButton
			self._panel = Panel.SHADING
			brdfPanel = MyPanelBRDF(parent=self, renderer=self._renderer)
			self._brdfButton.setStyleSheet("background-color: rgb(0,0,255);");
			self._panelLayout.addWidget(brdfPanel)
			self._mainLayout.addLayout(self._panelLayout)
			self.setFixedWidth(360)


	def btnModel_onClick(self):
		if self._panel == Panel.MODEL:
			self._modelButton.setStyleSheet(" ")
			self.cleanPanel()
		else:
			
			if self._panel != Panel.COLLAPSED:
				self._currentButtonPressed.setStyleSheet(" ")
				self.cleanPanel()
			
			self._panel = Panel.MODEL
			self._currentButtonPressed = self._modelButton
			modelPanel = MyPanelModel(parent=self, renderer=self._renderer)
			self._modelButton.setStyleSheet("background-color: rgb(0,0,255);");
			self._panelLayout.addWidget(modelPanel)
			self._mainLayout.addLayout(self._panelLayout)
			self.setFixedWidth(360)		

	def btnAbout_onClick(self):
		if self._panel == Panel.ABOUT:
			self._aboutButton.setStyleSheet(" ")
			self.cleanPanel()
		else:
			if self._panel != Panel.COLLAPSED:
				self._currentButtonPressed.setStyleSheet(" ")
				self.cleanPanel()

			self._panel = Panel.ABOUT
			self._currentButtonPressed = self._aboutButton
			aboutPanel = MyPanelAbout(parent=self, renderer=self._renderer)
			self._aboutButton.setStyleSheet("background-color: rgb(0,0,255);");
			self._panelLayout.addWidget(aboutPanel)
			self._mainLayout.addLayout(self._panelLayout)
			self.setFixedWidth(360)

	def btnEffect_onClick(self):
		if self._panel == Panel.EFFECT:
			self._effectButton.setStyleSheet(" ")
			self.cleanPanel()
		else:
			if self._panel != Panel.COLLAPSED:
				self._currentButtonPressed.setStyleSheet(" ")
				self.cleanPanel()

			self._panel = Panel.EFFECT
			self._currentButtonPressed = self._effectButton
			effectPanel = MyPanelEffect(parent=self, renderer=self._renderer)
			self._effectButton.setStyleSheet("background-color: rgb(0,0,255);")
			self._panelLayout.addWidget(effectPanel)
			self._mainLayout.addLayout(self._panelLayout)
			self.setFixedWidth(360)
	
	def btnLighting_onClick(self):
		if self._panel == Panel.LIGHT:
			self._lightButton.setStyleSheet(" ")
			self.cleanPanel()
		else:
			if self._panel != Panel.COLLAPSED:
				self._currentButtonPressed.setStyleSheet(" ")
				self.cleanPanel()

			self._panel = Panel.LIGHT
			self._currentButtonPressed = self._lightButton
			lightPanel = MyPanelLighting(parent=self, renderer=self._renderer)
			self._lightButton.setStyleSheet("background-color: rgb(0,0,255);")
			self._panelLayout.addWidget(lightPanel)
			self._mainLayout.addLayout(self._panelLayout)
			self.setFixedWidth(360)


	def btnMaterial_onClick(self):
		if self._panel == Panel.MATERIAL:
			self._materialButton.setStyleSheet(" ")
			self.cleanPanel()
		else:
			if self._panel != Panel.COLLAPSED:
				self._currentButtonPressed.setStyleSheet(" ")
				self.cleanPanel()

			self._panel = Panel.MATERIAL
			self._currentButtonPressed = self._materialButton
			materialPanel = MyPanelMaterial(parent=self, renderer=self._renderer)
			self._materialButton.setStyleSheet("background-color: rgb(0,0,255);")
			self._panelLayout.addWidget(materialPanel)
			self._mainLayout.addLayout(self._panelLayout)
			self.setFixedWidth(360)

	def cleanPanel(self):
		self._panel = Panel.COLLAPSED;
		self._mainLayout.removeItem(self._panelLayout)
		w = self._panelLayout.takeAt(0).widget()
		w.setParent(None)
		w.deleteLater()
		self._panelLayout = QVBoxLayout()
		self._panelLayout.setContentsMargins(0, 0, 0, 0)
		self._panelLayout.setSpacing(0)
		self.setFixedWidth(65)