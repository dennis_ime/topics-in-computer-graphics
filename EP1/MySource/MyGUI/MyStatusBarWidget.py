from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class MyStatusBarWidget(QWidget):
	def __init__(self, parent=None, renderer=None, **kwargs):
		super(MyStatusBarWidget, self).__init__(parent)
		self._parent = parent
		self._font = kwargs.get("font", QFont())
		self._renderer = renderer

		self._mainLayout = QHBoxLayout()
		self._mainLayout.setContentsMargins(0, 0, 0, 0)
		self._mainLayout.setSpacing(3)

		self._viewFunc = [
			self._renderer.viewLeft,
			self._renderer.viewRight,
			self._renderer.viewTop,
			self._renderer.viewBottom,
			self._renderer.viewFront,
			self._renderer.viewBack
		]

		## ======================= View  ================================ ##
		self._viewComboLayout = QHBoxLayout()
		label = QLabel("Axis: ")
		label.setFont(self._font)
		self._viewCombo = QComboBox(self)
		self._viewCombo.addItem("+x")
		self._viewCombo.addItem("-x")
		self._viewCombo.addItem("+y")
		self._viewCombo.addItem("-y")
		self._viewCombo.addItem("+z")
		self._viewCombo.addItem("-z")
		self._viewCombo.setFont(self._font)
		self._viewComboLayout.addWidget(label)
		self._viewComboLayout.addWidget(self._viewCombo)
		self._viewCombo.activated.connect(self.viewDirectionChanged)
		self._mainLayout.addLayout(self._viewComboLayout)
		
		## ======================= Camera  ================================ ##
		self._cameraLayout = QHBoxLayout()
		label = QLabel(" Camera: ")
		label.setFont(self._font)
		self._cameraLayout.addWidget(label)
		self._cameraLensCombo = QComboBox(self)
		self._cameraLensCombo.addItem("Perspective")
		self._cameraLensCombo.addItem("Orthographic")
		self._cameraLensCombo.setFont(self._font)
		self._cameraLensCombo.activated.connect(self._renderer.cameraLensChanged)
		self._cameraLayout.addWidget(self._cameraLensCombo)

		self._cameraCombo = QComboBox(self)
		self._cameraCombo.addItem("Store")
		self._cameraCombo.addItem("Recall")
		self._cameraCombo.addItem("Reset")
		self._cameraCombo.activated.connect(self.cameraOptionChanged)
		self._cameraLayout.addWidget(self._cameraCombo)
		self._mainLayout.addLayout(self._cameraLayout)

		## ======================= Render  ================================ ##
		self._renderLayout = QHBoxLayout()
		self._renderLayout.setContentsMargins(0, 0, 0, 0)
		self._renderLayout.setSpacing(3)
		label = QLabel(" Style: ")
		label.setFont(self._font)
		self._renderLayout.addWidget(label)
		self._drawStyleCombo = QComboBox(self)
		self._drawStyleCombo.addItem("Points")
		self._drawStyleCombo.addItem("Wireframe")
		self._drawStyleCombo.addItem("Solid")
		self._drawStyleCombo.addItem("Solid with edges")
		self._drawStyleCombo.setFont(self._font)
		self._drawStyleCombo.activated.connect(self._renderer.drawStyleChanged)
		self._drawStyleCombo.setCurrentIndex(2)
		self._renderLayout.addWidget(self._drawStyleCombo)
		self._mainLayout.addLayout(self._renderLayout)

		## ============================= Quality ======================================== ##
		self._qualityLayout = QHBoxLayout()
		self._qualityLayout.setContentsMargins(0, 0, 5, 0)
		self._qualityLayout.setSpacing(3)
		label = QLabel(" Quality: ")
		label.setFont(self._font)
		self._qualityLayout.addWidget(label)
		self._shadingCombo = QComboBox(self)
		self._shadingCombo.addItem("Low")
		self._shadingCombo.addItem("High")
		self._shadingCombo.setFont(self._font)
		self._shadingCombo.activated.connect(self._renderer.shadingChanged)
		self._shadingCombo.setCurrentIndex(1)
		self._qualityLayout.addWidget(self._shadingCombo)

		menu = QMenu()
		menu.setFont(self._font)
		lightingAction = QAction("Lighting", self)
		lightingAction.setCheckable(True)
		lightingAction.setChecked(True)
		lightingAction.triggered.connect(self._renderer.lightingChanged)
		menu.addAction(lightingAction)

		profilingAction = QAction("Profiling", self)
		profilingAction.setCheckable(True)
		profilingAction.setChecked(True)
		profilingAction.triggered.connect(self.profilingChanged)
		menu.addAction(profilingAction)

		menu.addSeparator()
		animateAction = QAction("Animate", self)
		animateAction.setCheckable(True)
		animateAction.setChecked(False)
		animateAction.triggered.connect(self.animateChanged)
		menu.addAction(animateAction)

		self._options = QPushButton()
		self._options.setText("Options")
		self._options.setFont(self._font)
		self._options.setMenu(menu)
		self._qualityLayout.addWidget(self._options)
		self._mainLayout.addLayout(self._qualityLayout)

		self._mainLayout.addStretch(1)
		self.setLayout(self._mainLayout)

	def clear(self):
		self._renderer.clear()

	def updateViewer(self):
		self._renderer.update()

	def viewDirectionChanged(self, index):
		self._viewFunc[index]()

	def cameraOptionChanged(self, index):
		if index == 0:
			self.storeViewerCamera()
		elif index == 1:
			self.recallViewerCamera()
		else:
			self.resetViewerCamera()

	def storeViewerCamera(self):
		self._renderer.storeCamera()

	def recallViewerCamera(self):
		self._renderer.recallCamera()
		self._cameraLensCombo.setCurrentIndex(self._renderer.activeSceneCamera().lens)

	def resetViewerCamera(self):
		self._renderer.resetCamera()
		self._cameraLensCombo.setCurrentIndex(self._renderer.activeSceneCamera().lens)

	def profilingChanged(self, state):
		if state:
			self._renderer.enableProfiling(True)
			self._parent._parent.restartTimer()
		else:
			self._renderer.enableProfiling(False)
			self._parent._parent.stopTimer()
			self._parent._parent.clearStatistics()

	def animateChanged(self, state):
		self._renderer.enableAnimation(state)

	def renderTimeEstimates(self):
		self._renderer.enableAnimation(state)

	def sizeHint(self):
		return QSize(1280, 800)