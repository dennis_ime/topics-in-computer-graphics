#version 330
			
#define POINT_LIGHT 0
#define DIRECTIONAL_LIGHT 1
#define HEMISPHERICAL_LIGHT 2

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 3) in vec3 color;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

#include "MySource/ShaderProgram/BlinnPhong/partial/light.glsl"

// lights 
// Enum in host (EpiclLightType): POINT=0, DIRECTIONAL=1, HEMISPHERICAL=2
uniform int lightType; 
uniform PointLight pointLight;
uniform DirectionalLight directionalLight;
uniform HemisphericalLight hemisphericalLight;

out VertexData
{
    smooth vec4 vertexPosition;
    #modifier vec4 vertexNormal;
    smooth LightData light;
    smooth vec2 texturedCoord;
    smooth vec3 vertexColor;
} vertexData;

void main()
{
    vertexData.vertexPosition = viewMatrix * modelMatrix * vec4(position, 1.0);
    vertexData.vertexNormal = viewMatrix * vec4(normalMatrix * normal, 0.0);

    switch(lightType) {
    	case 0:
    		computePointLight(pointLight, vertexData.vertexPosition, vertexData.light);
    		break;
    	case 1:
    		computeDirectionalLight(directionalLight, vertexData.light);
    		break;
    	case 2:
    		computeHemisphericalLight(hemisphericalLight, vertexData.vertexNormal, vertexData.light);
    		break;
    }
    vertexData.vertexColor = color;
    gl_Position = projectionMatrix * vertexData.vertexPosition;
}