struct PointLight {
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float radius;
	float attenuationConstant;
	float attenuationLinear;
	float attenuationQuad;
};

struct DirectionalLight {
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct HemisphericalLight {
	vec3 skyAmbient;
	vec3 skySpecular;
	vec3 skyDiffuse;
	vec3 groundAmbient;
	vec3 groundSpecular;
	vec3 groundDiffuse;
};

#include "MySource/ShaderProgram/BlinnPhong/partial/lightdata.glsl"

void computePointLight(in PointLight ilight,
	in vec4 vertexPosition,
	out LightData olight) 
{
	olight.direction = normalize(ilight.position.xyz - vertexPosition.xyz).xyz;
	olight.ambient = ilight.ambient;
	olight.diffuse = ilight.diffuse;
	olight.specular = ilight.specular;

	// attenuation
	float t = length(ilight.position.xyz - vertexPosition.xyz);
	float divisor = ilight.attenuationConstant;
	divisor += (ilight.attenuationLinear * t);
	divisor += (ilight.attenuationQuad *t*t);

	olight.attenuation = pow(clamp(1 - pow(t/ilight.radius,4), 0, 1), 2.0);
}

void computeDirectionalLight(in DirectionalLight ilight, out LightData olight)
{
	olight.direction = normalize(ilight.direction).xyz;
	olight.ambient = ilight.ambient;
	olight.diffuse = ilight.diffuse;
	olight.specular = ilight.specular;
	olight.attenuation = 1.0;
}

void computeHemisphericalLight(in HemisphericalLight ilight, in vec4 vertexNormal,
	out LightData olight)
{
	olight.direction = vertexNormal.xyz;

	olight.diffuse = mix(ilight.groundDiffuse, ilight.skyDiffuse, clamp(dot(vertexNormal.xyz, vec3(0,1,0)) * 0.5, 0, 1)).xyz;
	olight.specular = mix(ilight.groundSpecular, ilight.skySpecular, clamp(dot(vertexNormal.xyz, vec3(0,1,0)) * 0.5, 0, 1)).xyz;
	olight.ambient = mix(ilight.groundAmbient, ilight.skyAmbient, clamp(dot(vertexNormal.xyz, vec3(0,1,0)) * 0.5, 0, 1)).xyz;

	olight.attenuation = 1.0;
}

