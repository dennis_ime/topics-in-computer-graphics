struct LightData {
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float attenuation;
};