#version 330

#include "MySource/ShaderProgram/BlinnPhong/partial/material.glsl"
#include "MySource/ShaderProgram/BlinnPhong//partial/lightdata.glsl"

in Interpolators
{
	smooth vec4 vertexPosition;
	#modifier vec4 vertexNormal;
	smooth LightData light;
	smooth vec2 textureCoord;
	smooth vec3 vertexColor;
} interpolators;

uniform float selected;
uniform sampler2D texObject;
uniform Material material;
out vec4 fragColor;

void main()
{
	// ambient term
	vec3 ambient = material.ambient * interpolators.light.ambient;

	// diffuse term
	vec3 N = normalize(interpolators.vertexNormal.xyz);
	vec3 L = normalize(interpolators.light.direction);
	vec3 diffuse = interpolators.light.diffuse * material.diffuse * max(dot(N, L), 0.0);

	// specular term
	vec3 E = normalize(-interpolators.vertexPosition.xyz);
	vec3 R = normalize(-reflect(L, N)); 
	vec3 specular = interpolators.light.specular * material.specular * pow(max(dot(R, E), 0.0), material.shininess);

	// final intensity
	vec3 intensity = material.emission + clamp(ambient + interpolators.light.attenuation * (diffuse + specular), 0.0, 1.0);
	vec4 tex = texture(texObject, interpolators.textureCoord.st);
	fragColor = (1.0 - tex.a) * vec4(intensity, 1.0) + tex.a * vec4(selected * tex.rgb, 1.0);
}