#version 330

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

#include "MySource/ShaderProgram/BlinnPhong/partial/lightdata.glsl"

in VertexData
{
	smooth vec4 vertexPosition;
	#modifier vec4 vertexNormal;
	smooth LightData light;
	smooth vec2 textureCoord;
	smooth vec3 vertexColor;
} vertexData[];

out Interpolators
{
	smooth vec4 vertexPosition;
	#modifier vec4 vertexNormal;
	smooth LightData light;
	smooth vec2 textureCoord;
	smooth vec3 vertexColor;
} interpolators;

void main()
{
	for (int i = 0; i < gl_in.length(); ++i) {
		gl_Position = gl_in[i].gl_Position;
		interpolators.vertexPosition = vertexData[i].vertexPosition;
		interpolators.vertexNormal = vertexData[i].vertexNormal;		
		interpolators.light = vertexData[i].light;
		interpolators.textureCoord = vertexData[i].textureCoord;
		interpolators.vertexColor = vertexData[i].vertexColor;

		EmitVertex();
	}
	EndPrimitive();
}