#version 330

layout(triangles) in;
layout(triangle_strip, max_vertices=12) out;

#include "MySource/ShaderProgram/BlinnPhong/partial/lightdata.glsl"
#include "MySource/ShaderProgram/BlinnPhong/partial/light.glsl"

uniform int lightType; 
uniform PointLight pointLight;
uniform DirectionalLight directionalLight;
uniform HemisphericalLight hemisphericalLight;

uniform float height;
uniform float width;

in VertexData
{
	smooth vec4 vertexPosition;
	#modifier vec4 vertexNormal;
	smooth LightData light;
	smooth vec2 textureCoord;
	smooth vec3 vertexColor;
} vertexData[];

out Interpolators
{
	smooth vec4 vertexPosition;
	#modifier vec4 vertexNormal;
	smooth LightData light;
	smooth vec2 textureCoord;
	smooth vec3 vertexColor;
} interpolators;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

void generateVertex(int vidx, vec4 cv, vec3 dt, float size, vec3 normal, vec2 texCoord)
{
	interpolators.vertexPosition = vec4(cv.xyz + dt*size, 1);
	gl_Position = projectionMatrix * interpolators.vertexPosition;
	interpolators.vertexNormal = vec4(normal, 1);
	interpolators.vertexColor = vertexData[vidx].vertexColor;
	
	switch(lightType) {
  	case 0:
  		computePointLight(pointLight, interpolators.vertexPosition, interpolators.light);
  		break;
  	case 1:
  		computeDirectionalLight(directionalLight, interpolators.light);
  		break;
  	case 2:
  		computeHemisphericalLight(hemisphericalLight, interpolators.vertexNormal, interpolators.light);
  		break;
  }

	interpolators.textureCoord = texCoord;
	EmitVertex();	
}

vec2 interpolateTexCoord(float w, vec4 cv, vec4 dt, vec2 texct, vec2 texdt)
{
	/* Let's consider that w <= ||cv - dt||*/
	float D = (dt - cv).length();
	float k = abs(D - w); 
	return (texct*k) + (texdt*(1-k));
}

void generateSpikeFace(int vidx)
{
	vec4 n = vertexData[vidx].vertexNormal;
	vec4 cv = vertexData[vidx].vertexPosition;
	vec4 lv = vertexData[(vidx+1)%3].vertexPosition;
	vec4 rv = vertexData[(vidx+2)%3].vertexPosition;

	vec3 l = normalize(lv - cv).xyz;
	vec3 r = normalize(rv - cv).xyz;
	vec3 u = normalize(n).xyz;

	vec3 normal = cross(l, r);

	/* correct inversed normals. */
	if (dot(u, normal) < 0)
		normal = -normal;

	generateVertex(vidx, cv, l, width, normal, 
		interpolateTexCoord(width, cv, lv, vertexData[vidx].textureCoord, vertexData[(vidx+1)%3].textureCoord));
	generateVertex(vidx, cv, r, width, normal, 
		interpolateTexCoord(width, cv, rv, vertexData[vidx].textureCoord, vertexData[(vidx+2)%3].textureCoord));
	generateVertex(vidx, cv, u, height, normal, vertexData[vidx].textureCoord);

	EndPrimitive();
}

void main() 
{	
	for (int i = 0; i < gl_in.length(); ++i)
		generateSpikeFace(i);

	for (int i = 0; i < gl_in.length(); ++i) {
		gl_Position = gl_in[i].gl_Position;
		interpolators.vertexPosition = vertexData[i].vertexPosition;
		interpolators.vertexNormal = vertexData[i].vertexNormal;
		interpolators.light =  vertexData[i].light;
		interpolators.textureCoord = vertexData[i].textureCoord;
		interpolators.vertexColor = vertexData[i].vertexColor;

		EmitVertex();
	}
	EndPrimitive();
}