struct EpicMaterial {
	float metallic;
	float roughness;
	float specular_coefficient;
	vec3 cbase;
};

float G1(in vec3 v, in vec3 vertexNormal, in EpicMaterial material) {
	float k = pow(material.roughness + 1, 2) / 8.0;
	float divisor = (dot(vertexNormal.xyz, v)*(1-k)) + k;
	return dot(vertexNormal.xyz, v) / divisor;
}