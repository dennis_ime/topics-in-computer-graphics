struct PointLight {
  vec3 position;
  vec3 color;
  float radius;
  float attenuationConstant;
  float attenuationLinear;
  float attenuationQuad;
};

struct DirectionalLight {
  vec3 direction;
  vec3 color;
};

struct HemisphericalLight {
  vec3 skyColor;
  vec3 groundColor;
};

#include "MySource/ShaderProgram/Epic/partial/lightdata.glsl"

void computePointLight(in PointLight ilight, in vec4 vertexPosition, out LightData olight)
{
  olight.direction = normalize(ilight.position.xyz - vertexPosition.xyz).xyz;
  olight.color = ilight.color;
  
  //attenuation
	float t = length(ilight.position.xyz - vertexPosition.xyz);
	float divisor = ilight.attenuationConstant;
	divisor += (ilight.attenuationLinear * t);
	divisor += (ilight.attenuationQuad * t*t);

	olight.attenuation = pow(clamp(1 - pow(t/ilight.radius,4), 0, 1), 2.0)/divisor;
}

void computeDirectionalLight(in DirectionalLight ilight, out LightData olight)
{
  olight.direction = -ilight.direction;
  olight.color = ilight.color;
  olight.attenuation = 1.0;
}

void computeHemisphericalLight(in HemisphericalLight ilight, in vec4 vertexNormal, 
	in LightData olight)
{
  olight.direction = vertexNormal.xyz;

  olight.color = mix(ilight.groundColor, ilight.skyColor, 
    clamp(dot(vertexNormal.xyz, vec3(0,1,0)) * 0.5 + 0.5, 0, 1)); 
  olight.attenuation = 1.0;
}