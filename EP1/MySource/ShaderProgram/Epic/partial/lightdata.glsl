struct LightData {
  vec3 direction;
  vec3 color;
  float attenuation;
};