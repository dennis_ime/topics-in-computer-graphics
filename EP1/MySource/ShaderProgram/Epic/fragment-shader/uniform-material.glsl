#version 330
#define M_PI 3.1415926535897932384626433832795

#include "MySource/ShaderProgram/Epic/partial/material.glsl"
#include "MySource/ShaderProgram/Epic/partial/lightdata.glsl"

in Interpolators
{
	smooth vec4 vertexPosition;
	#modifier vec4 vertexNormal;
	smooth vec3 vertexColor;
	smooth vec3 cameraDirection;
	smooth vec2 textureCoord;
	smooth float attenuation;
	smooth LightData light;
} interpolators;

uniform EpicMaterial material;
uniform sampler2D cbaseTex;
uniform sampler2D metallicTex;
uniform sampler2D specularTex;
uniform sampler2D roughnessTex;

out vec4 fragColor;

EpicMaterial getMaterialSetup()
{
	EpicMaterial mymaterial;
	vec3 _cbaseTex = texture2D(cbaseTex, interpolators.textureCoord.st).rgb;
	float _metallicTex = texture2D(metallicTex, interpolators.textureCoord.st).g;
	float _specularTex = texture2D(specularTex, interpolators.textureCoord.st).g;
	float _roughnessTex = texture2D(roughnessTex, interpolators.textureCoord.st).g;

	mymaterial.cbase = material.cbase + _cbaseTex; 
	mymaterial.metallic = material.metallic + _metallicTex; 
	mymaterial.specular_coefficient = material.specular_coefficient + _specularTex;
	mymaterial.roughness = material.roughness + _roughnessTex;

	return mymaterial;
}

void main() {
	vec3 N = interpolators.vertexNormal.xyz;
	vec3 V = interpolators.cameraDirection;
	vec3 L = interpolators.light.direction;
	vec3 H = normalize(L + V).xyz;
	
	/* get material data considering texture. */
	EpicMaterial mymaterial = getMaterialSetup();				

	// diffuse term
	vec3 cdiff = (1 - mymaterial.metallic) * mymaterial.cbase.rgb;
	vec3 d = cdiff / M_PI;

	//specular term
	vec3 cspec = mix(0.08 * mymaterial.specular_coefficient * vec3(1,1,1), mymaterial.cbase.rgb, mymaterial.metallic);
	float alpha = mymaterial.roughness * mymaterial.roughness;

	float alpha2 = alpha*alpha;

	float D = alpha2 / (M_PI * pow((pow(dot(N, H), 2.0)) * (alpha2 - 1.0) + 1.0, 2.0));
	vec3 F = cspec + (1 - cspec)*pow(2.0, -5.55473*(dot(V,H)) - 6.98316*(dot(V,H)));
	float G = G1(L, N, mymaterial)*G1(V, N, mymaterial);

	vec3 s = (D*F*G) / (4.0 * (dot(N,L))*(dot(N, V)));

	vec3 fcolor = interpolators.light.color * dot(N, L) * (d + s); 
	fcolor = fcolor * interpolators.light.attenuation + vec3(0.3, 0.3, 0.3);
	fragColor = vec4(fcolor, 1.0);				
}