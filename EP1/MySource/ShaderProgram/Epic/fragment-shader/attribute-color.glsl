#version 330
#define M_PI 3.1415926535897932384626433832795

#include "MySource/ShaderProgram/Epic/partial/material.glsl"
#include "MySource/ShaderProgram/Epic/partial/lightdata.glsl"

in Interpolators
{
  smooth vec4 vertexPosition;
  #modifier vec4 vertexNormal;
  smooth vec3 vertexColor;
  smooth vec3 cameraDirection;
  smooth vec2 textureCoord;
  smooth float attenuation;
  smooth LightData light;
} interpolators;

uniform EpicMaterial material;

out vec4 fragColor;

void main() {
  vec3 N = interpolators.vertexNormal.xyz;
  vec3 V = interpolators.cameraDirection;
  vec3 L = interpolators.light.direction;
  vec3 H = normalize(L + V).xyz;
  
  // diffuse term
  vec3 cdiff = (1 - material.metallic) * material.cbase.rgb * interpolators.vertexColor;
  vec3 d = cdiff / M_PI;

  //specular term
  vec3 cspec = mix(0.08 * material.specular_coefficient*vec3(1,1,1), material.cbase, material.metallic);
  float alpha = material.roughness * material.roughness;

  float alpha2 = alpha*alpha;

  float D = alpha2 / (M_PI * pow((pow(dot(N, H), 2)) * (alpha2 - 1) + 1, 2));
  float F = cspec + (1 - cspec)*pow(2, -5.55473*(dot(V,H)) - 6.98316*(dot(V,H)));
  float G = G1(L, N, material)*G1(V, N, material);

  float s = (D*F*G) / (4 * (N*L)*(N*V));

  vec3 fcolor = interpolators.light.color * (N * L) * (d + s); 
  fcolor = fcolor * interpolators.light.attenuation;
  fragColor = vec4(fcolor, 1.0);
}