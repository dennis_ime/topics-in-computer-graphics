#version 330
#define POINT_LIGHT 0
#define DIRECTIONAL_LIGHT 1
#define HEMISPHERICAL_LIGHT 2

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 color;
layout(location = 3) in vec2 texcoord;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;
uniform vec3 cameraPosition;

#include "MySource/ShaderProgram/Epic/partial/light.glsl"

// lights 
// Enum in host (EpiclLightType): POINT=0, DIRECTIONAL=1, HEMISPHERICAL=2
uniform int lightType; 
uniform PointLight pointLight;
uniform DirectionalLight directionalLight;
uniform HemisphericalLight hemisphericalLight;

out VertexData {
  smooth vec4 vertexPosition;
  #modifier vec4 vertexNormal;
  smooth vec3 vertexColor;
  smooth vec3 cameraDirection;
  smooth vec2 textureCoord;
  smooth float attenuation;
  smooth LightData light;
} vertexData;

void main()
{
  // vertex data computation
  vertexData.vertexPosition = viewMatrix * modelMatrix * vec4(position, 1.0);
  vertexData.vertexNormal = viewMatrix * vec4(normalMatrix * normal, 0.0);

  // light data computation
  switch(lightType) {
    case 0:
      computePointLight(pointLight, vertexData.vertexPosition, vertexData.light);
      break;
    case 1:
      computeDirectionalLight(directionalLight, vertexData.light);
      break;
    case 2:
      computeHemisphericalLight(hemisphericalLight, vertexData.vertexPosition, vertexData.light);
      break;
  }

  // standard output 
  vertexData.textureCoord = texcoord;
  gl_Position = projectionMatrix * vertexData.vertexPosition;
}