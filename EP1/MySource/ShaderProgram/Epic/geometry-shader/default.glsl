#version 330
layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

#include "MySource/ShaderProgram/Epic/partial/lightdata.glsl"

in VertexData
{
	smooth vec4 vertexPosition;
	#modifier vec4 vertexNormal;
	smooth vec3 vertexColor;
	smooth vec3 cameraDirection;
	smooth vec2 textureCoord;
	smooth float attenuation;
	smooth LightData light;
 } vertexData[];

 out Interpolators
 {
 	smooth vec4 vertexPosition;
 	#modifier vec4 vertexNormal;
 	smooth vec3 vertexColor;
 	smooth vec3 cameraDirection;
 	smooth vec2 textureCoord;
 	smooth float attenuation;
 	smooth LightData light;
 } interpolations;

 void main()
 {
 	for (int i = 0; i < gl_in.length(); ++i) {
 		gl_Position = gl_in[i].gl_Position;
 		interpolations.vertexPosition = vertexData[i].vertexPosition;
 		interpolations.vertexNormal = vertexData[i].vertexNormal;
 		interpolations.vertexColor = vertexData[i].vertexColor;
 		interpolations.cameraDirection = vertexData[i].cameraDirection;
 		interpolations.textureCoord = vertexData[i].textureCoord;
 		interpolations.attenuation = vertexData[i].attenuation;
 		interpolations.light = vertexData[i].light;

 		EmitVertex();
 	}
 	EndPrimitive();
 }