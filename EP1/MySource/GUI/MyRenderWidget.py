from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from MySource.Graphics.MyRenderer import MyRenderer
from MySource.GUI.ColorInputWidget import ColorInputWidget
from MySource.Graphics.MyLight import MyLightType
from Source.Graphics.Material import Material
from MySource.Graphics.EpicMaterial import EpicMaterial
import os
#from Source.Graphics.Renderer import Renderer

class MyRenderWidget(QWidget):
	def __init__(self, parent=None, **kwargs):
		super(MyRenderWidget, self).__init__(parent)

		self._font = kwargs.get("font", QFont())
		self.setFont(self._font)

		self._parent = parent
		self._renderer = MyRenderer(self, antialiasing=True, **kwargs)
		#self._renderer = Renderer(self, antialiasing=True, **kwargs)

		self._mainLayout = QVBoxLayout()
		self._mainLayout.setContentsMargins(0, 0, 0, 0)
		self._mainLayout.setSpacing(3)

		self._mainLayout.addWidget(self._renderer)

		self._bottomLayout = QHBoxLayout()
		self._bottomLayout.setContentsMargins(3, 0, 3, 0)
		self._bottomLayout.setSpacing(3)

		self._viewLayout = QHBoxLayout()
		self._viewLayout.setContentsMargins(0, 0, 0, 0)
		self._viewLayout.setSpacing(3)

		self._viewFunc = [
			self._renderer.viewLeft,
			self._renderer.viewRight,
			self._renderer.viewTop,
			self._renderer.viewBottom,
			self._renderer.viewFront,
			self._renderer.viewBack
		]

		label = QLabel("Axis: ")
		label.setFont(self._font)
		self._viewLayout.addWidget(label)
		self._viewCombo = QComboBox(self)
		self._viewCombo.addItem("+x")
		self._viewCombo.addItem("-x")
		self._viewCombo.addItem("+y")
		self._viewCombo.addItem("-y")
		self._viewCombo.addItem("+z")
		self._viewCombo.addItem("-z")
		self._viewCombo.setFont(self._font)
		self._viewCombo.activated.connect(self.viewDirectionChanged)
		self._viewLayout.addWidget(self._viewCombo)
		
		self._bottomLayout.addLayout(self._viewLayout)

		self._cameraLayout = QHBoxLayout()
		self._cameraLayout.setContentsMargins(0, 0, 5, 0)
		self._cameraLayout.setSpacing(3)

		label = QLabel(" Camera: ")
		label.setFont(self._font)
		self._cameraLayout.addWidget(label)
		self._cameraLensCombo = QComboBox(self)
		self._cameraLensCombo.addItem("Perspective")
		self._cameraLensCombo.addItem("Orthographic")
		self._cameraLensCombo.setFont(self._font)
		self._cameraLensCombo.activated.connect(self._renderer.cameraLensChanged)
		self._cameraLayout.addWidget(self._cameraLensCombo)

		self._cameraCombo = QComboBox(self)
		self._cameraCombo.addItem("Store")
		self._cameraCombo.addItem("Recall")
		self._cameraCombo.addItem("Reset")
		self._cameraCombo.activated.connect(self.cameraOperationChanged)
		self._cameraLayout.addWidget(self._cameraCombo)

		self._bottomLayout.addLayout(self._cameraLayout)

		self._renderLayout = QHBoxLayout()
		self._renderLayout.setContentsMargins(0, 0, 5, 0)
		self._renderLayout.setSpacing(3)
		label = QLabel(" Style: ")
		label.setFont(self._font)
		self._renderLayout.addWidget(label)
		self._drawStyleCombo = QComboBox(self)
		self._drawStyleCombo.addItem("Points")
		self._drawStyleCombo.addItem("Wireframe")
		self._drawStyleCombo.addItem("Solid")
		self._drawStyleCombo.addItem("Solid with edges")
		self._drawStyleCombo.setFont(self._font)
		self._drawStyleCombo.activated.connect(self._renderer.drawStyleChanged)
		self._drawStyleCombo.setCurrentIndex(2)
		self._renderLayout.addWidget(self._drawStyleCombo)

		label = QLabel(" Quality: ")
		label.setFont(self._font)
		self._renderLayout.addWidget(label)
		self._shadingCombo = QComboBox(self)
		self._shadingCombo.addItem("Low")
		self._shadingCombo.addItem("High")
		self._shadingCombo.setFont(self._font)
		self._shadingCombo.activated.connect(self._renderer.shadingChanged)
		self._shadingCombo.setCurrentIndex(1)
		self._renderLayout.addWidget(self._shadingCombo)

		self._bottomLayout.addLayout(self._renderLayout)

		menu = QMenu()
		menu.setFont(self._font)
		lightingAction = QAction("Lighting", self)
		lightingAction.setCheckable(True)
		lightingAction.setChecked(True)
		lightingAction.triggered.connect(self._renderer.lightingChanged)
		menu.addAction(lightingAction)

		profilingAction = QAction("Profiling", self)
		profilingAction.setCheckable(True)
		profilingAction.setChecked(True)
		profilingAction.triggered.connect(self.profilingChanged)
		menu.addAction(profilingAction)

		menu.addSeparator()
		animateAction = QAction("Animate", self)
		animateAction.setCheckable(True)
		animateAction.setChecked(False)
		animateAction.triggered.connect(self.animateChanged)
		menu.addAction(animateAction)

		self._options = QPushButton()
		self._options.setText("Options")
		self._options.setFont(self._font)
		self._options.setMenu(menu)
		self._renderLayout.addWidget(self._options)

		self._bottomLayout.addStretch(1)
		self._mainLayout.addLayout(self._bottomLayout)
		self.setLayout(self._mainLayout)

		# ============================ new controllers ========================== #
		self._controllersLayout = QHBoxLayout()
		self._controllersLayout.setContentsMargins(0, 0, 0, 0)
		self._controllersLayout.setSpacing(1)

		self._actorShadingLayout = QVBoxLayout()
		self._actorShadingLayout.setContentsMargins(0, 0, 0, 0)
		self._actorShadingLayout.setSpacing(1)

		self._actorLayout = QHBoxLayout()

		label = QLabel("Actor ")
		label.setFont(self._font)
		self._actorLayout.addWidget(label)
		self._actorComboBox = QComboBox(self)
		self._actorComboBox.addItem("Cube")
		self._actorComboBox.addItem("Cone")
		self._actorComboBox.addItem("Cylinder")
		self._actorComboBox.addItem("Sphere")
		self._actorComboBox.addItem("Teapot")
		self._actorComboBox.addItem("Torus")
		self._actorComboBox.activated.connect(self.changeActor)

		self._actorLayout.addWidget(self._actorComboBox)
		self._actorShadingLayout.addLayout(self._actorLayout)
		
		# ================== Shading ============================================= #
		self._shadingProgramLayout = QHBoxLayout()

		label = QLabel("Shading Program ")
		label.setFont(self._font)
		self._shadingProgramLayout.addWidget(label)
		self._shadingProgramCombo = QComboBox(self)
		self._shadingProgramCombo.addItem("Blinn Phong")
		self._shadingProgramCombo.addItem("Epic")
		self._shadingProgramCombo.activated.connect(self.changeShadingProgram)

		self._shadingProgramLayout.addWidget(self._shadingProgramCombo)
		self._actorShadingLayout.addLayout(self._shadingProgramLayout)
		self._controllersLayout.addLayout(self._actorShadingLayout)
		self._mainLayout.addLayout(self._controllersLayout)

		# ======================= Light Light ==================================== #
		self._lightLayout = QVBoxLayout()
		self._lightTypeLayout = QHBoxLayout()
		self._lightLayout.setContentsMargins(1, 1, 1, 1)
		label = QLabel(" Light type ")
		label.setFont(self._font)
		self._lightTypeLayout.addWidget(label)

		self._lightTypeComboBox = QComboBox(self)
		self._lightTypeComboBox.addItem("Point Light")
		self._lightTypeComboBox.addItem("Directional Light")
		self._lightTypeComboBox.addItem("Hemispherical Light")
		self._lightTypeComboBox.setCurrentIndex(1)
		self._lightTypeComboBox.activated.connect(self.changeLightType)
		self._lightTypeLayout.addWidget(self._lightTypeComboBox)
		
		self._lightTypeLayout.addWidget(self._lightTypeComboBox)
		self._lightLayout.addLayout(self._lightTypeLayout)
		
		self._createDirectionalLightBlinnPhong()

		# =================== Material ====================================== #
		self._materialLayout = QVBoxLayout()
		self._createMaterialBlinnPhong()

		self._controllersLayout.addLayout(self._materialLayout)	

	@classmethod
	def textureDirPath(cls):
		return os.getcwd()+"/textures"

	def clear(self):
		self._renderer.clear()

	def updateViewer(self):
		self._renderer.update()

	def viewDirectionChanged(self, index):
		self._viewFunc[index]()

	def cameraOperationChanged(self, index):
		if index == 0:
			self.storeViewerCamera()
		elif index == 1:
			self.recallViewerCamera()
		else:
			self.resetViewerCamera()

	def storeViewerCamera(self):
		self._renderer.storeCamera()

	def recallViewerCamera(self):
		self._renderer.recallCamera()
		self._cameraLensCombo.setCurrentIndex(self._renderer.activeSceneCamera().lens)

	def resetViewerCamera(self):
		self._renderer.resetCamera()
		self._cameraLensCombo.setCurrentIndex(self._renderer.activeSceneCamera().lens)

	def profilingChanged(self, state):
		if state:
			self._renderer.enableProfiling(True)
			self._renderer.restartTimer()
		else:
			self._renderer.enableProfiling(False)
			self._parent.stopTimer()
			self._parent.clearStatistics()

	def animateChanged(self, state):
		self._renderer.enableAnimation(state)

	def renderTimeEstimates(self):
		return self._renderer.renderTimeEstimates()

	def sizeHint(self):
		return QSize(1280, 800)


	#============================= MY METHODS ================================ #
	#================== change actor ==================================== #
	def changeActor(self, index):
		material = None
		if self._shadingProgramCombo.currentIndex() == 0:
			material = Material()
			material.emissionColor = self._materialEmissionColor.getColor()
			material.ambientColor = self._materialAmbientColor.getColor()
			material.diffuseColor = self._materiaDiffuseColor.getColor()
			material.specularColor = self._materialSpecularColor.getColor()
			material.shininess = self._shininessSpin.value()
		elif self._shadingProgramCombo.currentIndex() == 1:
			material = EpicMaterial()
			material.cbase = self._cbase.getColor()
			material.roughness = self._roughnessSpin.value()
			material.metallic = self._metallicSpin.value()
			material.specularColor = self._specularCoefficientSpin.value()

		self._renderer.changeActor(index, material=material)


	def changeShadingProgram(self, index):
		self._clearLayout(self._materialLayout)
		self._clearLayout(self._activedLightLayout)
		self._actorComboBox.setCurrentIndex(0)
		self._lightTypeComboBox.setCurrentIndex(0)
		if index == 0:
			self._renderer.toShadingProgramBlinPhong()
			
			if self._normalMappingLayout is not None:
				self._clearLayout(self._normalMappingLayout)
				self._normalMappingLayout = None

			self._createMaterialBlinnPhong()
			self._createPointLightBlinnPhong()
		elif index == 1:
			self._renderer.toShadingProgramEpic()
			self._createMaterialEpic()
			self._createPointLightEpic()
			self._createNormalMappingLayout()
			
	

	# ===================== CLEAR LAYOUT ========================================= #
	def _clearLayout(self, layout):
		child = layout.takeAt(0)
		while child is not None:
			if child.layout() is not None:
				self._clearLayout(child.layout())
			elif child.widget() is not None:
				widget = child.widget()
				widget.hide()
				del widget
			child = layout.takeAt(0)
		layout.update()

	# =============== BLING PHONG =============================================== #
	# =============== CREATE DIRECTIONAL LIGHT BLINN PHONG ====================== #
	def _createDirectionalLightBlinnPhong(self):
		self._activedLightLayout = QVBoxLayout()
		self._lightDirectionLayout = QHBoxLayout()

		light = self._renderer.getLight()

		label = QLabel("Direction: ")
		label.setFont(self._font)
		self._lightDirectionLayout.addWidget(label)

		label = QLabel("  x: ")
		label.setFont(self._font)
		self._lightDirectionLayout.addWidget(label)
		self._directionalLightDirectionX = QDoubleSpinBox(self)
		self._directionalLightDirectionX.setRange(-1.0, 1.0)
		self._directionalLightDirectionX.setSingleStep(0.2)
		self._directionalLightDirectionX.setValue(light.direction[0])
		self._directionalLightDirectionX.valueChanged.connect(self._renderer.setDirectionalLightDirectionX)
		self._lightDirectionLayout.addWidget(self._directionalLightDirectionX)

		label = QLabel("  y: ")
		label.setFont(self._font)
		self._lightDirectionLayout.addWidget(label)
		self._directionalLightDirectionY = QDoubleSpinBox(self)
		self._directionalLightDirectionY.setRange(-1.0, 1.0)
		self._directionalLightDirectionY.setSingleStep(0.2)
		self._directionalLightDirectionY.setValue(light.direction[1])
		self._directionalLightDirectionY.valueChanged.connect(self._renderer.setDirectionalLightDirectionY)
		self._lightDirectionLayout.addWidget(self._directionalLightDirectionY)

		label = QLabel(" z: ")
		label.setFont(self._font)
		self._lightDirectionLayout.addWidget(label)
		self._directionalLightDirectionZ = QDoubleSpinBox(self)
		self._directionalLightDirectionZ.setRange(-1.0, 1.0)
		self._directionalLightDirectionZ.setSingleStep(0.2)
		self._directionalLightDirectionZ.setValue(light.direction[2])
		self._directionalLightDirectionZ.valueChanged.connect(self._renderer.setDirectionalLightDirectionZ)
		self._lightDirectionLayout.addWidget(self._directionalLightDirectionZ)

		self._directionalLightDiffuseColor = ColorInputWidget(self._parent, "Diffuse Color", self._font, 
			self._renderer.setDirectionalLightDiffuseColor, light.color.diffuseColor)
		self._directionalLightSpecularColor = ColorInputWidget(self._parent, "Specular Color", self._font, 
			self._renderer.setDirectionalLightSpecularColor, light.color.specularColor)
		self._directionalLightAmbientColor = ColorInputWidget(self._parent, "Ambient Color", self._font, 
			self._renderer.setDirectionalLightAmbientColor, light.color.ambientColor)

		# headlight
		self._lightPointHeadlight = QCheckBox("Headlight", self)
		self._lightPointHeadlight.setChecked(light.headlight)
		self._lightPointHeadlight.stateChanged.connect(self._renderer.setDirectionalLightHeadlight)

		self._activedLightLayout.addLayout(self._lightDirectionLayout)
		self._activedLightLayout.addLayout(self._directionalLightDiffuseColor.layout)
		self._activedLightLayout.addLayout(self._directionalLightSpecularColor.layout)
		self._activedLightLayout.addLayout(self._directionalLightAmbientColor.layout)		
		self._activedLightLayout.addWidget(self._lightPointHeadlight)

		self._controllersLayout.addLayout(self._lightLayout)
		self._lightLayout.addLayout(self._activedLightLayout)

	# =============== CREATE POINT LIGHT BLINN PHONG ====================== #
	def _createPointLightBlinnPhong(self):
		self._activedLightLayout = QVBoxLayout()
		self._lightPositionLayout = QHBoxLayout()

		light = self._renderer.getLight()

		label = QLabel("Position: ")
		label.setFont(self._font)
		self._lightPositionLayout.addWidget(label)
		
		label = QLabel("  x: ")
		label.setFont(self._font)
		self._lightPositionLayout.addWidget(label)
		self._lightPointPositionX = QDoubleSpinBox(self)
		self._lightPointPositionX.setRange(-20.0, 20.0)
		self._lightPointPositionX.setSingleStep(1.0)
		self._lightPointPositionX.setValue(light.position[0])
		self._lightPointPositionX.valueChanged.connect(self._renderer.setPointLightPositionX)
		self._lightPositionLayout.addWidget(self._lightPointPositionX)

		label = QLabel("  y: ")
		label.setFont(self._font)
		self._lightPositionLayout.addWidget(label)
		self._lightPointPositionY = QDoubleSpinBox(self)
		self._lightPointPositionY.setRange(-20.0, 20.0)
		self._lightPointPositionY.setSingleStep(1.0)
		self._lightPointPositionY.setValue(light.position[1])
		self._lightPointPositionY.valueChanged.connect(self._renderer.setPointLightPositionY)
		self._lightPositionLayout.addWidget(self._lightPointPositionY)

		label = QLabel("  z: ")
		label.setFont(self._font)
		self._lightPositionLayout.addWidget(label)
		self._lightPointPositionZ = QDoubleSpinBox(self)
		self._lightPointPositionZ.setRange(-20.0, 20.0)
		self._lightPointPositionZ.setSingleStep(1.0)
		self._lightPointPositionZ.setValue(light.position[2])
		self._lightPointPositionZ.valueChanged.connect(self._renderer.setPointLightPositionZ)
		self._lightPositionLayout.addWidget(self._lightPointPositionZ)

		self._pointLightDiffuseColor = ColorInputWidget(self, "Diffuse Color", self._font, 
			self._renderer.setPointLightDiffuseColor, light.color.diffuseColor)
		self._pointLightSpecularColor = ColorInputWidget(self, "Specular Color", self._font, 
			self._renderer.setPointLightSpecularColor, light.color.specularColor)
		self._pointLightAmbientColor = ColorInputWidget(self, "Ambient Color", self._font, 
			self._renderer.setPointLightAmbientColor, light.color.ambientColor)
		self._controllersLayout.addLayout(self._lightLayout)

		# Attenuation parameters
		self._lightPointRadiusLayout = QHBoxLayout()
		label = QLabel("Radius: ")
		label.setFont(self._font)
		self._lightPointRadiusLayout.addWidget(label)
		self._lightPointRadius = QDoubleSpinBox(self)
		self._lightPointRadius.setRange(0.0, 100.0)
		self._lightPointRadius.setSingleStep(5.0)
		self._lightPointRadius.setValue(light.radius)
		self._lightPointRadius.valueChanged.connect(self._renderer.setPointLightRadius)
		self._lightPointRadiusLayout.addWidget(self._lightPointRadius)

		self._lightPointConstantAttenuationLayout = QHBoxLayout()
		label = QLabel("Constant Attenuation: ")
		label.setFont(self._font)
		self._lightPointConstantAttenuationLayout.addWidget(label)
		self._lightPointConstantAttenuation = QDoubleSpinBox(self)
		self._lightPointConstantAttenuation.setRange(0.0, 1.0)
		self._lightPointConstantAttenuation.setSingleStep(0.1)
		self._lightPointConstantAttenuation.setValue(light.attenuationConstant)
		self._lightPointConstantAttenuation.valueChanged.connect(self._renderer.setPointLightConstantAttenuation)
		self._lightPointConstantAttenuationLayout.addWidget(self._lightPointConstantAttenuation)

		self._lightPointLinearAttenuationLayout = QHBoxLayout()
		label = QLabel("Linear Attenuation: ")
		label.setFont(self._font)
		self._lightPointLinearAttenuationLayout.addWidget(label)
		self._lightPointLinearAttenuation = QDoubleSpinBox(self)
		self._lightPointLinearAttenuation.setRange(0.0, 1.0)
		self._lightPointLinearAttenuation.setSingleStep(0.1)
		self._lightPointLinearAttenuation.setValue(light.attenuationLinear)
		self._lightPointLinearAttenuation.valueChanged.connect(self._renderer.setPointLightLinearAttenuation)
		self._lightPointLinearAttenuationLayout.addWidget(self._lightPointLinearAttenuation)

		self._lightPointQuadAttenuationLayout = QHBoxLayout()
		label = QLabel("Quadratic Attenuation: ")
		label.setFont(self._font)
		self._lightPointQuadAttenuationLayout.addWidget(label)
		self._lightPointQuadAttenuation = QDoubleSpinBox(self)
		self._lightPointQuadAttenuation.setRange(0.0, 1.0)
		self._lightPointQuadAttenuation.setSingleStep(0.1)
		self._lightPointQuadAttenuation.setValue(light.attenuationQuad)
		self._lightPointQuadAttenuation.valueChanged.connect(self._renderer.setPointLightQuadAttenuation)
		self._lightPointQuadAttenuationLayout.addWidget(self._lightPointQuadAttenuation)

		# headlight
		self._lightPointHeadlight = QCheckBox("Headlight", self)
		self._lightPointHeadlight.setChecked(light.headlight)
		self._lightPointHeadlight.stateChanged.connect(self._renderer.setPointLightHeadlight)

		self._activedLightLayout.addLayout(self._lightPositionLayout)
		self._activedLightLayout.addLayout(self._pointLightDiffuseColor.layout)
		self._activedLightLayout.addLayout(self._pointLightSpecularColor.layout)
		self._activedLightLayout.addLayout(self._pointLightAmbientColor.layout)		

		self._lightLayout.addLayout(self._activedLightLayout)
		self._activedLightLayout.addLayout(self._lightPointRadiusLayout)
		self._activedLightLayout.addLayout(self._lightPointConstantAttenuationLayout)
		self._activedLightLayout.addLayout(self._lightPointLinearAttenuationLayout)
		self._activedLightLayout.addLayout(self._lightPointQuadAttenuationLayout)
		self._activedLightLayout.addWidget(self._lightPointHeadlight)

	# =============== CREATE HEMISPHERICAL LIGHT BLINN PHONG ========================================== #
	def _createHemisphericalLightBlinnPhong(self):
		light = self._renderer.getLight()

		self._hemisphericalLightSkyDiffuseColor = ColorInputWidget(self, "Sky Diffuse Color", self._font, 
			self._renderer.setHemisphericalLightSkyDiffuseColor, light.skyColor.diffuseColor)
		self._hemisphericalLightSkySpecularColor = ColorInputWidget(self, "Sky Specular Color", self._font, 
			self._renderer.setHemisphericalLightSkySpecularColor, light.skyColor.specularColor)
		self._hemisphericalLightSkyAmbientColor = ColorInputWidget(self, "Sky Ambient Color", self._font, 
			self._renderer.setHemisphericalLightSkyAmbientColor, light.skyColor.ambientColor)

		self._hemisphericalLightGroundDiffuseColor = ColorInputWidget(self, "Ground Diffuse Color", self._font, 
			self._renderer.setHemisphericalLightGroundDiffuseColor, light.groundColor.diffuseColor)
		self._hemisphericalLightGroundSpecularColor = ColorInputWidget(self, "Ground Specular Color", self._font, 
			self._renderer.setHemisphericalLightGroundSpecularColor, light.groundColor.specularColor)
		self._hemisphericalLightGroundAmbientColor = ColorInputWidget(self, "Ground Ambient Color", self._font,
			self._renderer.setHemisphericalLightGroundAmbientColor, light.groundColor.ambientColor)

		self._activedLightLayout.addLayout(self._hemisphericalLightSkyDiffuseColor.layout)
		self._activedLightLayout.addLayout(self._hemisphericalLightSkySpecularColor.layout)
		self._activedLightLayout.addLayout(self._hemisphericalLightSkyAmbientColor.layout)
		self._activedLightLayout.addLayout(self._hemisphericalLightGroundDiffuseColor.layout)
		self._activedLightLayout.addLayout(self._hemisphericalLightGroundSpecularColor.layout)
		self._activedLightLayout.addLayout(self._hemisphericalLightGroundAmbientColor.layout)

		self._lightLayout.addLayout(self._activedLightLayout)

	# ================== CHANGE LIGHT TYPE ======================================================= #
	def changeLightType(self, index):
		self._clearLayout(self._activedLightLayout)
		if self._shadingProgramCombo.currentIndex() == 0: # Blinn Phong Shading
			if index == MyLightType.POINT:
				self._renderer.changeBPLight(MyLightType.POINT)
				self._createPointLightBlinnPhong()
			elif index == MyLightType.DIRECTIONAL:
				self._renderer.changeBPLight(MyLightType.DIRECTIONAL)
				self._createDirectionalLightBlinnPhong()
			else:
				self._renderer.changeBPLight(MyLightType.HEMISPHERICAL)
				self._createHemisphericalLightBlinnPhong()
		else:   # Epic Shading
			if index == MyLightType.POINT:
				self._renderer.changeEpicLight(MyLightType.POINT)
				self._createPointLightEpic()
			elif index == MyLightType.DIRECTIONAL:
				self._renderer.changeEpicLight(MyLightType.DIRECTIONAL)
				self._createDirectionalLightEpic()
			else:
				self._renderer.changeEpicLight(MyLightType.HEMISPHERICAL)
				self._createHemisphericalLightEpic()

	# =================== CREATE MATERIAL BLINN PHONG ============================================ #
	def _createMaterialBlinnPhong(self):
		material = self._renderer.initialDefaultBPMaterial()
		self._materialEmissionColor = ColorInputWidget(self._parent, "Emission Color", self._font, 
			self._renderer.setBPMaterialEmission, material.emissionColor)
		self._materialAmbientColor = ColorInputWidget(self._parent, "Ambient Color", self._font, 
			self._renderer.setBPMaterialAmbientColor, material.ambientColor)
		self._materiaDiffuseColor = ColorInputWidget(self._parent, "Diffuse Color", self._font, 
			self._renderer.setBPMaterialDiffuseColor, material.diffuseColor)
		self._materialSpecularColor = ColorInputWidget(self._parent, "Specular Color", self._font, 
			self._renderer.setBPMaterialSpecularColor, material.specularColor)
		
		self._shininessLayout = QHBoxLayout()
		label = QLabel( "Shininess ")
		label.setFont(self._font)
		self._shininessLayout.addWidget(label)
		self._shininessSpin = QDoubleSpinBox(self)
		self._shininessSpin.setRange(0.0, 30.0)
		self._shininessSpin.setSingleStep(1.0)
		self._shininessSpin.setValue(material.shininess)
		self._shininessSpin.valueChanged.connect(self._renderer.setBPMaterialShininess)
		self._shininessLayout.addWidget(self._shininessSpin)

		#self._materialLayout.addLayout(self._materialLabelLayout)
		self._materialLayout.addLayout(self._materialEmissionColor.layout)
		self._materialLayout.addLayout(self._materialAmbientColor.layout)
		self._materialLayout.addLayout(self._materiaDiffuseColor.layout)
		self._materialLayout.addLayout(self._materialSpecularColor.layout)
		self._materialLayout.addLayout(self._shininessLayout)

	# =============== EPIC ====================================================================== #
	# =============== EPIC MATERIAL ============================================================= #
	def _createMaterialEpic(self):
		material = self._renderer.initialDefaultEpicMaterial()

		self._roughnessLayout = QHBoxLayout()
		label = QLabel("Roughness: ")
		label.setFont(self._font)
		self._roughnessLayout.addWidget(label)
		self._roughnessSpin = QDoubleSpinBox(self)
		self._roughnessSpin.setRange(0.0, 1.0)
		self._roughnessSpin.setSingleStep(0.05)
		self._roughnessSpin.setValue(material.roughness)
		self._roughnessSpin.valueChanged.connect(self._renderer.setEpicMaterialRoughness)
		self._roughnessLayout.addWidget(self._roughnessSpin)
		
		self._btnRougnessTex = QPushButton("Add Texture")
		self._btnRougnessTex.clicked.connect(self.btnRoughnessTex_onClick)
		self._btnRemoveRoughnessTex = QPushButton("Remove Texture")
		self._btnRemoveRoughnessTex.setEnabled(False)
		self._btnRemoveRoughnessTex.clicked.connect(self.btnRemoveRoughnessTex_onClick)
		self._roughnessLayout.addWidget(self._btnRougnessTex)
		self._roughnessLayout.addWidget(self._btnRemoveRoughnessTex)

		self._metallicLayout = QHBoxLayout(self)
		label = QLabel("Metallic: ")
		label.setFont(self._font)
		self._metallicLayout.addWidget(label)
		self._metallicSpin = QDoubleSpinBox(self)
		self._metallicSpin.setRange(0.0, 1.0)
		self._metallicSpin.setSingleStep(0.05)
		self._metallicSpin.setValue(material.metallic)
		self._metallicSpin.valueChanged.connect(self._renderer.setEpicMaterialMetallic)
		self._metallicLayout.addWidget(self._metallicSpin)

		self._btnMetallicTex = QPushButton("Add Texture")
		self._btnMetallicTex.clicked.connect(self.btnMetallicTex_onClick)
		self._btnRemoveMetallicTex = QPushButton("Remove Texture")
		self._btnRemoveMetallicTex.setEnabled(False)
		self._btnRemoveMetallicTex.clicked.connect(self.btnRemoveMetallicTex_onClick)
		self._metallicLayout.addWidget(self._btnMetallicTex)
		self._metallicLayout.addWidget(self._btnRemoveMetallicTex)

		self._specularCoefficientLayout = QHBoxLayout()
		label = QLabel("Specular Coefficient: ")
		label.setFont(self._font)
		self._specularCoefficientLayout.addWidget(label)
		self._specularCoefficientSpin = QDoubleSpinBox(self)
		self._specularCoefficientSpin.setRange(0.0, 1.0)
		self._specularCoefficientSpin.setSingleStep(0.05)
		self._specularCoefficientSpin.setValue(material.specular_coefficient)
		self._specularCoefficientSpin.valueChanged.connect(self._renderer.setEpicMaterialSpecularCoefficient)
		self._specularCoefficientLayout.addWidget(self._specularCoefficientSpin)
		
		self._btnSpecularCoefficientTex = QPushButton("Add Texture")
		self._btnSpecularCoefficientTex.clicked.connect(self.btnSpecularCoefficientTex_onClick)
		self._btnRemoveSpecularCoefficientTex = QPushButton("Remove Texture")
		self._btnRemoveSpecularCoefficientTex.setEnabled(False)
		self._btnRemoveSpecularCoefficientTex.clicked.connect(self.btnRemoveSpecularCoefficientTex_onClick)
		self._specularCoefficientLayout.addWidget(self._btnSpecularCoefficientTex)
		self._specularCoefficientLayout.addWidget(self._btnRemoveSpecularCoefficientTex)

		self._cbase = ColorInputWidget(self, "Base Color", self._font, self._renderer.setEpicMaterialCBase,
			material.cbase)
		self._btnCBaseTex = QPushButton("Add Texture", self)
		self._btnCBaseTex.clicked.connect(self.btnCBaseTex_onClick)
		self._btnRemoveCBaseTex = QPushButton("Remove Texture", self)
		self._btnRemoveCBaseTex.setEnabled(False)
		self._btnRemoveCBaseTex.clicked.connect(self.btnRemoveCBaseTex_onClick)
		self._cbase.layout.addWidget(self._btnCBaseTex)
		self._cbase.layout.addWidget(self._btnRemoveCBaseTex)

		self._materialLayout.addLayout(self._roughnessLayout)
		self._materialLayout.addLayout(self._metallicLayout)
		self._materialLayout.addLayout(self._specularCoefficientLayout)
		self._materialLayout.addLayout(self._cbase.layout)

	def openFileDialog(self):
		options = QFileDialog.Options()
		options |= QFileDialog.DontUseNativeDialog
		filename, _ = QFileDialog.getOpenFileName(self, "Open Texture", MyRenderWidget.textureDirPath(), 
			"Images files (*.jpg *.gif *.png)", options=options)
		return filename

	def btnRoughnessTex_onClick(self):
		texPath = self.openFileDialog()
		if texPath is not "":
			self._renderer.setEpicRoughnessTexture(texPath)
			self._roughnessSpin.setEnabled(False)
			self._btnRougnessTex.setEnabled(False)
			self._btnRemoveRoughnessTex.setEnabled(True)

	def btnRemoveRoughnessTex_onClick(self):
		self._renderer.destroyEpicRoughnessTexture()
		self._roughnessSpin.setEnabled(True)
		self._renderer.setEpicMaterialRoughness(self._roughnessSpin.value())
		self._btnRougnessTex.setEnabled(True)
		self._btnRemoveRoughnessTex.setEnabled(False)

	def btnMetallicTex_onClick(self):
		texPath = self.openFileDialog()
		if texPath is not "":
			self._renderer.setEpicMetallicTexture(texPath)
			self._metallicSpin.setEnabled(False)
			self._btnMetallicTex.setEnabled(False)
			self._btnRemoveMetallicTex.setEnabled(True)

	def btnRemoveMetallicTex_onClick(self):
		self._renderer.destroyEpicMetallicTexture()
		self._metallicSpin.setEnabled(True)
		self._btnMetallicTex.setEnabled(True)
		self._renderer.setEpicMaterialMetallic(self._metallicSpin.value())
		self._btnRemoveMetallicTex.setEnabled(False)

	def btnSpecularCoefficientTex_onClick(self):
		texPath = self.openFileDialog()
		if texPath is not "":
			self._renderer.setEpicSpecularTexture(texPath)
			self._specularCoefficientSpin.setEnabled(False)
			self._btnSpecularCoefficientTex.setEnabled(False)
			self._btnRemoveSpecularCoefficientTex.setEnabled(True)

	def btnRemoveSpecularCoefficientTex_onClick(self):
		self._renderer.destroySpecularTexture()
		self._specularCoefficientSpin.setEnabled(True)
		self._btnSpecularCoefficientTex.setEnabled(True)
		self._renderer.setEpicMaterialSpecularCoefficient(self._specularCoefficientSpin.value())
		self._btnRemoveSpecularCoefficientTex.setEnabled(False)


	def btnCBaseTex_onClick(self):
		texPath = self.openFileDialog()		
		if texPath is not "":
			self._renderer.setEpicCBaseTexture(texPath)
			self._cbase.disable()
			self._btnCBaseTex.setEnabled(False)
			self._btnRemoveCBaseTex.setEnabled(True)

	def btnRemoveCBaseTex_onClick(self):
		self._renderer.destroyCBaseTexture()
		self._cbase.enable()
		self._btnCBaseTex.setEnabled(True)
		self._renderer.setEpicMaterialCBase(self._cbase.getColor())
		self._btnRemoveCBaseTex.setEnabled(False)


	def _createDirectionalLightEpic(self):
		self._activedLightLayout = QVBoxLayout()
		self._lightDirectionLayout = QHBoxLayout()

		light = self._renderer.getLight()

		label = QLabel("Direction: ")
		label.setFont(self._font)
		self._lightDirectionLayout.addWidget(label)

		label = QLabel("  x: ")
		label.setFont(self._font)
		self._lightDirectionLayout.addWidget(label)
		self._directionalLightDirectionX = QDoubleSpinBox(self)
		self._directionalLightDirectionX.setRange(-1.0, 1.0)
		self._directionalLightDirectionX.setSingleStep(0.2)
		self._directionalLightDirectionX.setValue(light.direction[0])
		self._directionalLightDirectionX.valueChanged.connect(self._renderer.setDirectionalLightDirectionX)
		self._lightDirectionLayout.addWidget(self._directionalLightDirectionX)

		label = QLabel("  y: ")
		label.setFont(self._font)
		self._lightDirectionLayout.addWidget(label)
		self._directionalLightDirectionY = QDoubleSpinBox(self)
		self._directionalLightDirectionY.setRange(-1.0, 1.0)
		self._directionalLightDirectionY.setSingleStep(0.2)
		self._directionalLightDirectionY.setValue(light.direction[1])
		self._directionalLightDirectionY.valueChanged.connect(self._renderer.setDirectionalLightDirectionY)
		self._lightDirectionLayout.addWidget(self._directionalLightDirectionY)

		label = QLabel(" z: ")
		label.setFont(self._font)
		self._lightDirectionLayout.addWidget(label)
		self._directionalLightDirectionZ = QDoubleSpinBox(self)
		self._directionalLightDirectionZ.setRange(-1.0, 1.0)
		self._directionalLightDirectionZ.setSingleStep(0.2)
		self._directionalLightDirectionZ.setValue(light.direction[2])
		self._directionalLightDirectionZ.valueChanged.connect(self._renderer.setDirectionalLightDirectionZ)
		self._lightDirectionLayout.addWidget(self._directionalLightDirectionZ)

		self._directionalLightColor = ColorInputWidget(self, "Color ", self._font, 
			self._renderer.setDirectionalLightColor, light.color.color)

		# headlight
		self._lightPointHeadlight = QCheckBox("Headlight", self)
		self._lightPointHeadlight.setChecked(light.headlight)
		self._lightPointHeadlight.stateChanged.connect(self._renderer.setDirectionalLightHeadlight)

		self._activedLightLayout.addLayout(self._lightDirectionLayout)
		self._activedLightLayout.addLayout(self._directionalLightColor.layout)
		self._activedLightLayout.addWidget(self._lightPointHeadlight)

		self._lightLayout.addLayout(self._activedLightLayout)

	def _createPointLightEpic(self):
		self._activedLightLayout = QVBoxLayout()
		self._lightPositionLayout = QHBoxLayout()

		light = self._renderer.getLight()

		label = QLabel("Position: ")
		label.setFont(self._font)
		self._lightPositionLayout.addWidget(label)

		label = QLabel("  x: ")
		label.setFont(self._font)
		self._lightPositionLayout.addWidget(label)
		self._pointLightPositionX = QDoubleSpinBox(self)
		self._pointLightPositionX.setRange(-20.0, 20.0)
		self._pointLightPositionX.setSingleStep(1.0)
		self._pointLightPositionX.setValue(light.position[0])
		self._pointLightPositionX.valueChanged.connect(self._renderer.setPointLightPositionX)
		self._lightPositionLayout.addWidget(self._pointLightPositionX)

		label = QLabel("  y: ")
		label.setFont(self._font)
		self._lightPositionLayout.addWidget(label)
		self._pointLightPositionY = QDoubleSpinBox(self)
		self._pointLightPositionY.setRange(-20.0, 20.0)
		self._pointLightPositionY.setSingleStep(1.0)
		self._pointLightPositionY.setValue(light.position[1])
		self._pointLightPositionY.valueChanged.connect(self._renderer.setPointLightPositionY)
		self._lightPositionLayout.addWidget(self._pointLightPositionY)

		label = QLabel(" z: ")
		label.setFont(self._font)
		self._lightPositionLayout.addWidget(label)
		self._pointLightPositionZ = QDoubleSpinBox(self)
		self._pointLightPositionZ.setRange(-20.0, 20.0)
		self._pointLightPositionZ.setSingleStep(1.0)
		self._pointLightPositionZ.setValue(light.position[2])
		self._pointLightPositionZ.valueChanged.connect(self._renderer.setPointLightPositionZ)
		self._lightPositionLayout.addWidget(self._pointLightPositionZ)


		# Attenuation parameters
		self._lightPointRadiusLayout = QHBoxLayout()
		label = QLabel("Radius: ")
		label.setFont(self._font)
		self._lightPointRadiusLayout.addWidget(label)
		self._lightPointRadius = QDoubleSpinBox(self)
		self._lightPointRadius.setRange(0.0, 100.0)
		self._lightPointRadius.setSingleStep(5.0)
		self._lightPointRadius.setValue(light.radius)
		self._lightPointRadius.valueChanged.connect(self._renderer.setPointLightRadius)
		self._lightPointRadiusLayout.addWidget(self._lightPointRadius)

		self._lightPointConstantAttenuationLayout = QHBoxLayout()
		label = QLabel("Constant Attenuation: ")
		label.setFont(self._font)
		self._lightPointConstantAttenuationLayout.addWidget(label)
		self._lightPointConstantAttenuation = QDoubleSpinBox(self)
		self._lightPointConstantAttenuation.setRange(0.0, 1.0)
		self._lightPointConstantAttenuation.setSingleStep(0.1)
		self._lightPointConstantAttenuation.setValue(light.attenuationConstant)
		self._lightPointConstantAttenuation.valueChanged.connect(self._renderer.setPointLightConstantAttenuation)
		self._lightPointConstantAttenuationLayout.addWidget(self._lightPointConstantAttenuation)

		self._lightPointLinearAttenuationLayout = QHBoxLayout()
		label = QLabel("Linear Attenuation: ")
		label.setFont(self._font)
		self._lightPointLinearAttenuationLayout.addWidget(label)
		self._lightPointLinearAttenuation = QDoubleSpinBox(self)
		self._lightPointLinearAttenuation.setRange(0.0, 1.0)
		self._lightPointLinearAttenuation.setSingleStep(0.1)
		self._lightPointLinearAttenuation.setValue(light.attenuationLinear)
		self._lightPointLinearAttenuation.valueChanged.connect(self._renderer.setPointLightLinearAttenuation)
		self._lightPointLinearAttenuationLayout.addWidget(self._lightPointLinearAttenuation)

		self._lightPointQuadAttenuationLayout = QHBoxLayout()
		label = QLabel("Quadratic Attenuation: ")
		label.setFont(self._font)
		self._lightPointQuadAttenuationLayout.addWidget(label)
		self._lightPointQuadAttenuation = QDoubleSpinBox(self)
		self._lightPointQuadAttenuation.setRange(0.0, 1.0)
		self._lightPointQuadAttenuation.setSingleStep(0.1)
		self._lightPointQuadAttenuation.setValue(light.attenuationQuad)
		self._lightPointQuadAttenuation.valueChanged.connect(self._renderer.setPointLightQuadAttenuation)
		self._lightPointQuadAttenuationLayout.addWidget(self._lightPointQuadAttenuation)

		# headlight
		self._lightPointHeadlight = QCheckBox("Headlight", self)
		self._lightPointHeadlight.setChecked(light.headlight)
		self._lightPointHeadlight.stateChanged.connect(self._renderer.setPointLightHeadlight)

		self._pointLightColor = ColorInputWidget(self, "Color ", self._font, 
			self._renderer.setPointLightColor, light.color.color)

		self._activedLightLayout.addLayout(self._lightPositionLayout)
		self._activedLightLayout.addLayout(self._pointLightColor.layout)

		self._activedLightLayout.addLayout(self._lightPointRadiusLayout)
		self._activedLightLayout.addLayout(self._lightPointConstantAttenuationLayout)
		self._activedLightLayout.addLayout(self._lightPointLinearAttenuationLayout)
		self._activedLightLayout.addLayout(self._lightPointQuadAttenuationLayout)
		self._activedLightLayout.addWidget(self._lightPointHeadlight)

		self._lightLayout.addLayout(self._activedLightLayout)

	def _createHemisphericalLightEpic(self):
		self._activedLightLayout = QVBoxLayout()
		
		light = self._renderer.getLight()
		
		self._hemisphericalLightSkyColor = ColorInputWidget(self, "Sky Color ", self._font, 
			self._renderer.setHemisphericalLightSkyColor, light.skyColor.color)
		self._hemisphericalLightGroundColor = ColorInputWidget(self, "Ground Color", self._font,
			self._renderer.setHemisphericalLightGroundColor, light.groundColor.color)

		self._activedLightLayout.addLayout(self._hemisphericalLightSkyColor.layout)
		self._activedLightLayout.addLayout(self._hemisphericalLightGroundColor.layout)

		self._lightLayout.addLayout(self._activedLightLayout)



	def _createNormalMappingLayout(self):
		self._normalMappingLayout = QVBoxLayout()

		self._checkBoxNormalMapping = QCheckBox("&Normal mapping", self)
		self._checkBoxNormalMapping.stateChanged.connect(self.checkBoxNormalMapping_onStateChanged)

		self._normalMappingLayout.addWidget(self._checkBoxNormalMapping)
		
		self._controllersLayout.addLayout(self._normalMappingLayout)


	def checkBoxNormalMapping_onStateChanged(self, isChecked):
		if isChecked:
			texPath = self.openFileDialog()		
			if texPath is not "":
				self._renderer.setEpicNormalMappingTexture(texPath)
		else:
			self._renderer.destroyNormalMappingTexture()
		

	
