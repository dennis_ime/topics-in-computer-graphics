from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import PyQt5.QtGui as QtGui

class ColorInputWidget(QWidget):
	"""docstring for ColorInputWidget"""
	def __init__(self, parent, text, font, callbackSpinChanged, initial_value=QVector3D(0.0,0.0,0.0)):
		super(ColorInputWidget, self).__init__(parent)
		self._label = QLabel(text + ":  ")
		self._label.setFont(font)
		self._font = font
		self._layout = QHBoxLayout()
		self._layout.addWidget(self._label)
		self._parent = parent
		self._initial_value = initial_value
		self._setUp()
		self._callbackSpinChanged = callbackSpinChanged
		

	def _createInput(self, text, initial_value):
		label = QLabel(text, self)
		label.setFont(self._font)
		self._layout.addWidget(label)
		color = QDoubleSpinBox(self)
		color.setRange(0.0, 1.0)
		color.setSingleStep(0.05)
		color.setValue(initial_value)
		self._layout.addWidget(color)
		return color

	def spinChanged(self, n):
		color = self.getColor()
		self._setUpSpinsColor(color)
		self._callbackSpinChanged(color)
	
	
	def _setUpSpinsColor(self, color):
		self._setBackground(self._redSpin, QColor(255*color[0], 255*color[1], 255*color[2]))
		self._setBackground(self._greenSpin, QColor(255*color[0], 255*color[1], 255*color[2]))
		self._setBackground(self._blueSpin, QColor(255*color[0], 255*color[1], 255*color[2]))
		
		if color[0] < 0.5 and color[1] < 0.5 and color[2] < 0.5:
			self._setFontColor(self._redSpin, QColor(255,255,255))
			self._setFontColor(self._greenSpin, QColor(255,255,255))
			self._setFontColor(self._blueSpin, QColor(255,255,255))
		else:
			self._setFontColor(self._redSpin, QColor(0,0,0))
			self._setFontColor(self._greenSpin, QColor(0,0,0))
			self._setFontColor(self._blueSpin, QColor(0,0,0))
	

	def _setBackground(self, widget, color):
		pal = widget.palette()
		pal.setColor(QPalette.Base, color)
		widget.setPalette(pal)

	def _setFontColor(self, widget, color):
		pal = widget.palette()
		pal.setColor(QPalette.Text, color)
		widget.setPalette(pal)

	def _setUp(self):
		self._redSpin = self._createInput("R:", self._initial_value[0])
		self._greenSpin = self._createInput("G:", self._initial_value[1])
		self._blueSpin = self._createInput("B:", self._initial_value[2])

		self._redSpin.valueChanged.connect(self.spinChanged)
		self._greenSpin.valueChanged.connect(self.spinChanged)
		self._blueSpin.valueChanged.connect(self.spinChanged)

		self._setUpSpinsColor(self.getColor())

	def getColor(self):
		return QVector3D(self._redSpin.value(), self._greenSpin.value(), self._blueSpin.value())

	@property
	def layout(self):
		return self._layout

	# EP 02 Method
	def disable(self):
		self._redSpin.setEnabled(False)
		self._greenSpin.setEnabled(False)
		self._blueSpin.setEnabled(False)

	def enable(self):
		self._redSpin.setEnabled(True)
		self._greenSpin.setEnabled(True)
		self._blueSpin.setEnabled(True)