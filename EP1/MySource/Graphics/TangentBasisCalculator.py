import numpy as np

 # EP 2 CODE
 # This class is used to compute tangent and bitagents vectors from vertices, normals and texture coordinates.
 # It was designed to be used in the normal mapping implementation.
 # this implementation is based on http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-13-normal-mapping/
 # and https://learnopengl.com/Advanced-Lighting/Normal-Mapping

class TangentBasisCalculator(object):
	def __init__(self):
		super(TangentBasisCalculator, self).__init__()
		
	def calculate(self, vertices, uvs, normals, indices=None):
		if indices == None:
			return self._calculate_tangents_bitagents(vertices, uvs, normals, range(len(vertices)))
		else:
			return self._calculate_tangents_bitagents(vertices, uvs, normals, indices)


	def _calculate_tangents_bitagents(self, vertices, uvs, normals, indices):
		tangents = np.zeros(normals.shape)
		bitangents = np.zeros(normals.shape)
		
		for i in range(0,len(indices), 3):
			(i0, i1, i2) = (indices[i+0], indices[i+1], indices[i+2])

			v0 = vertices[i0]
			v1 = vertices[i1]
			v2 = vertices[i2]

			uv0 = uvs[i0]
			uv1 = uvs[i1]
			uv2 = uvs[i2]

			dPos1 = v1 - v0
			dPos2 = v2 - v0

			duv1 = uv1 - uv0
			duv2 = uv2 - uv0

			r = 1.0 / (duv1[0] * duv2[1] - duv1[1] * duv2[0])
			tangent = (dPos1 * duv2[1] - dPos2 * duv1[1])*r
			bitangent = (dPos2 * duv1[0] - dPos1 * duv2[0])*r

			# if tangents and bitagents at i0,i1,i2 have not been computed it assigns the first value
			# since tangents and bitagents are filled up with zeros, otherwise it will accumulate to compute the
			# "average". I am following the referencexd algorithm that's why I am only accumulate the vectors. 
			
			tangents[i0] += self._finalizeTangentComputation(normals[i0], tangent, bitangent)
			tangents[i1] += self._finalizeTangentComputation(normals[i1], tangent, bitangent)
			tangents[i2] += self._finalizeTangentComputation(normals[i2], tangent, bitangent)

			bitangents[i0] += bitangent
			bitangents[i1] += bitangent
			bitangents[i2] += bitangent

		return (np.array(tangents), np.array(bitangents))


	def normalize(self, v):
		return v / np.linalg.norm(v)

	def _finalizeTangentComputation(self, normal, tangent, bitangent):
		t = self._orthogonalize(normal, tangent)
		return self._invertTangentIfNeeded(normal, t, bitangent)

	def _orthogonalize(self, normal, tangent):
		return self.normalize(tangent - normal * np.dot(normal, tangent))

	def _invertTangentIfNeeded(self, normal, tangent, bitangent):
		if np.dot(np.cross(normal, tangent), bitangent) < 0.0:
			return -tangent
		return tangent