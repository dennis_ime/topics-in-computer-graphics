import math
import numpy as np
from OpenGL import GL
from MySource.Graphics.MyActor import MyActor
from MySource.Graphics.FactoryActor.ActorType import ActorType

class Torus(MyActor):
	def __init__(self, renderer, **kwargs):
		super(Torus, self).__init__(renderer, **kwargs)

		self._outerRadius = kwargs.get("outerRadius", 1.0)
		self._innerRadius = kwargs.get("innerRadius", 0.5)
		self._nlat = kwargs.get("nlat", 10) # 97 is the maximum
		self._nlng = kwargs.get("nlng", 10) # 97 is the maximum
		self._tangentBasisCalculator = kwargs.get("tangentBasisCalculator", None)
		self._vertices = None
		self._normals = None

		self.initialize()

	@property
	def innerRadius(self):
		return self._innerRadius

	@property
	def outerRadius(self):
		return self._outerRadius

	@property
	def nlng(self):
		return self._nlng

	@property
	def nlat(self):
		return self._nlat


	def generateGeometry(self):
		# implementation based on http://www.it.hiof.no/~borres/j3d/JOGL/torus/p-torus.html
		vertices = []
		normals = []
		texCoords = []
		indices = []

		di = (2.0 * math.pi) / float(self._nlat)
		dj = (2.0 * math.pi) / float(self._nlng)
		jangle = 0.0

		rin = self._innerRadius
		rout = self._outerRadius


		ni = 0
		nj = 0

		while jangle < ((2.0*math.pi) + dj):
			iangle = 0.0
			ni = 0	 

			while iangle < ((2.0*math.pi) + di):
				# vector position
				vx = math.cos(jangle) * (rout + math.cos(iangle)*rin)
				vy = math.sin(jangle) * (rout + math.cos(iangle)*rin)
				vz = math.sin(iangle) * rin

				# tangent vector with respect to big circle				
				tx = -math.sin(jangle)
				ty = math.cos(jangle)
				tz = 0

				# tangent vector wutg respect to littel circel
				sx = math.cos(jangle)*(-math.sin(iangle))
				sy = math.sin(jangle)*(-math.sin(iangle))
				sz = math.cos(iangle)

				# normal is cross-product of tangents
				nx = ty*sz - tz*sy
				ny = tz*sx - tx*sz
				nz = tx*sy - ty*sx

				# normalize normal
				l = math.sqrt(nx*nx + ny*ny + nz*nz)
				normals.append([nx/l, ny/l, nz/l])

				vertices.append([vx, vy, vz])
				texCoords.append([iangle, jangle])
				ni += 1
				iangle += di
				# End of inner loop (iangle)
			
			nj += 1
			jangle += dj
			# End of outer loop (jangle)

		#ni = self._nlat
		#nj = self._nlng

		for i in range(ni-1):
			for j in range(nj-1):
				# make face 
				indices.append(j*ni + i)
				indices.append(((j+1)*ni) + i)
				indices.append(j*ni + (i+1)) 

				# make other face
				indices.append(((j+1)*ni) + i)
				indices.append(j*ni + (i+1))
				indices.append(((j+1)*ni) + (i+1)) 

		#for i in range(ni):
		#	indices.append((ni*(nj))+i)
		#	indices.append(i)
		#	indices.append((ni*(nj))+(i+1))
			

			#indices.append(i+1)
			#indices.append((ni*nj)+(i+1))
			#indices.append((ni*(nj-1))+i)


		self._vertices = np.array(vertices, dtype=np.float32)
		self._normals = np.array(normals, dtype=np.float32)
		self._texCoords = np.array(texCoords, dtype=np.float32)
		self._indices = np.array(indices, dtype=np.uint32)


	def initialize(self):
		if self._vertices is None:
			self.generateGeometry()

		self.create(self._vertices, normals=self._normals, texcoords=self._texCoords, indices=self._indices,
			tangentBasisCalculator=self._tangentBasisCalculator)

	def render(self):
		GL.glDrawElements(GL.GL_TRIANGLES, self.numberOfIndices, GL.GL_UNSIGNED_INT, None)
		#GL.glDrawArrays(GL.GL_TRIANGLES, 0, len(self._vertices))

	def actorType():
		return ActorType.TORUS