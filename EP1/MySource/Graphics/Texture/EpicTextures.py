from MySource.Graphics.MyTexture import MyTexture2D
import numpy as np
import scipy.misc as mi

from OpenGL import GL

class EpicTextures(object):	

	class ActiveId:
		CBaseId = 0		
		MetallicId = 1
		SpecularId = 2
		RoughnessId = 3
		NormalMappingId = 4 

	def __init__(self, **kwargs):	
		super(EpicTextures, self).__init__()
		self._roughnessTexture = kwargs.get("roughnessTexture", EpicTextures.EmptyTexture(EpicTextures.ActiveId.RoughnessId))
		self._metallicTexture = kwargs.get("metallicTexture",  EpicTextures.EmptyTexture(EpicTextures.ActiveId.MetallicId))
		self._specularTexture = kwargs.get("specularTexture", EpicTextures.EmptyTexture(EpicTextures.ActiveId.SpecularId))
		self._cbaseTexture = kwargs.get("cbaseTexture",  EpicTextures.EmptyTexture(EpicTextures.ActiveId.CBaseId))
		self._normalMappingTexture = kwargs.get("normalMappingTextures", None)

		self._hasRoughnessTexture = True if "roughnessTexture" in kwargs else False
		self._hasMetallicTexture  = True if "metallicTexture" in kwargs else False
		self._hasSpecularTexture = True if "specularTexture" in kwargs else False
		self._hasCBaseTexture = True if "cbaseTexture" in kwargs else False
		self._hasNormalMappingTexture = True if "normalMappingTexture" in kwargs else False

	@classmethod
	def EmptyTexture(cls, activeId):
		data = np.zeros((1,1)).astype(np.uint8)
		pid = GL.glGenTextures(1)
		tex = MyTexture2D(id=pid, data=data, texUnit=activeId)

	@property
	def hasRoughnessTexture(self):
		return self._hasRoughnessTexture

	@property
	def hasMetallicTexture(self):
		return self._hasMetallicTexture

	@property
	def hasSpecularTexture(self):
		return self._hasSpecularTexture

	@property
	def hasCBaseTexture(self):
		return self._hasCBaseTexture

	@property
	def hasNormalMappingTexture(self):
		return self._hasNormalMappingTexture

	def _loadImage(self, path):
		return mi.imread(path) 

	def _createTexture2D(self, path, activeId=None):
		data = self._loadImage(path)
		id = GL.glGenTextures(1)
		return MyTexture2D(id=id, data=data, texUnit=activeId)

	def setCBaseTexture(self, cbaseTexturePath):				
		self._cbaseTexture = self._createTexture2D(cbaseTexturePath, activeId=EpicTextures.ActiveId.CBaseId)
		self._hasCBaseTexture = True

	def destroyCBaseTexture(self):
		self._cbaseTexture.destroy()
		self._cbaseTexture = EpicTextures.EmptyTexture(EpicTextures.ActiveId.CBaseId)
		self._hasCBaseTexture = False

	def releaseCBaseTexture(self):
		self._cbaseTexture.destroy()
		#self._hasCBaseTexture = False		

	def setMetallicTexture(self, metallicTexturePath):
		self._metallicTexture = self._createTexture2D(metallicTexturePath, activeId=EpicTextures.ActiveId.MetallicId)
		self._hasMetallicTexture = True

	def destroyMetallicTexture(self):
		self._metallicTexture.destroy()
		self._metallicTexture = EpicTextures.EmptyTexture(EpicTextures.ActiveId.MetallicId)
		self._hasMetallicTexture = False

	def releaseMetallicTexture(self):
		self._metallicTexture.destroy()
		#self._hasMetallicTexture = False

	def setSpecularTexture(self, specularTexturePath):
		self._specularTexture = self._createTexture2D(specularTexturePath, activeId=EpicTextures.ActiveId.SpecularId)
		self._hasSpecularTexture = True

	def destroySpecularTexture(self):
		self._specularTexture.destroy()
		self._specularTexture = EpicTextures.EmptyTexture(EpicTextures.ActiveId.SpecularId)
		self._hasSpecularTexture = False

	def releaseSpecularTexture(self):
		self._specularTexture.destroy()
		#self._hasSpecularTexture = False

	def setRoughnessTexture(self, roughnessTexturePath):
		self._roughnessTexture = self._createTexture2D(roughnessTexturePath, activeId=EpicTextures.ActiveId.RoughnessId)
		self._hasRoughnessTexture = True

	def destroyRoughnessTexture(self):
		self._roughnessTexture.destroy()
		self._roughnessTexture = EpicTextures.EmptyTexture(EpicTextures.ActiveId.RoughnessId)
		self._hasRoughnessTexture = False

	def releaseRoughnessTexture(self):
		self._roughnessTexture.destroy()
		#self._hasRoughnessTexture = False

	def setNormalMappingTexture(self, normalMappingTexturePath):
		self._normalMappingTexture = self._createTexture2D(normalMappingTexturePath, activeId=EpicTextures.ActiveId.NormalMappingId)
		self._hasNormalMappingTexture = True

	def destroyNormalMappingTexture(self):
		self._normalMappingTexture.destroy()
		self._normalMappingTexture = None
		self._normalMappingTexture = False

	def releaseNormalMappingTexture(self):
		self._normalMappingTexture.destroy()

	def destroyTextures(self):		
		if self._cbaseTexture is not None:	self.destroyCBaseTexture() 
		if self._metallicTexture is not None: self.destroyMetallicTexture() 
		if self._specularTexture is not None: self.destroySpecularTexture() 
		if self._roughnessTexture is not None: self.destroyRoughnessTexture()
		if self._normalMappingTexture is not None: self.destroyNormalMappingTexture()

	def releaseTextures(self):
		if self._cbaseTexture is not None: self.releaseCBaseTexture() 
		if self._metallicTexture is not None: self.releaseMetallicTexture()
		if self._specularTexture is not None: self.releaseSpecularTexture() 
		if self._roughnessTexture is not None: self.releaseRoughnessTexture() 
		if self._normalMappingTexture is not None: self.releaseNormalMappingTexture()

	def activeTextures(self):
		if self._hasCBaseTexture: self._cbaseTexture.setUpTextureAndActive() 
		if self._hasMetallicTexture: self._metallicTexture.setUpTextureAndActive()
		if self._hasSpecularTexture: self._specularTexture.setUpTextureAndActive() 
		if self._hasRoughnessTexture: self._roughnessTexture.setUpTextureAndActive() 
		if self._hasNormalMappingTexture: self._normalMappingTexture.setUpTextureAndActive()

	def cbaseTextureActiveId(self):
		return self._cbaseTexture.texUnit

	def metallicTextureActiveId(self):
		return self._metallicTexture.texUnit

	def specularTextureActiveId(self):
		return self._specularTexture.texUnit

	def roughnessTextureActiveId(self):
		return self._roughnessTexture.texUnit

	def normalMappingTextureActiveId(self):
		return self._normalMappingTexture.texUnit