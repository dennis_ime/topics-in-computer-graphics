from MySource.Graphics.MyCone import MyCone
from MySource.Graphics.MyCube import MyCube 
from MySource.Graphics.MyCylinder import MyCylinder
from MySource.Graphics.MyIcosahedron import MyIcosahedron
from MySource.Graphics.Teapot import Teapot
from MySource.Graphics.Torus import Torus

from MySource.Graphics.FactoryActor.FactoryActor import FactoryActor
from MySource.Graphics.MyShaders import MyShaders
from Source.Graphics.Material import Material

from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect

from PyQt5.QtGui import QVector3D, QMatrix4x4

class FactoryActorBlinnPhong(FactoryActor):
	"""docstring for FactoryActorBlinnPhong"""
	def __init__(self):
		super(FactoryActorBlinnPhong, self).__init__()
		self._shaders_collection = MyShaders(GeometryShaderEffect.DEFAULT)

	def _setUpMaterial(self, args={}):
		args["material"] = Material()

	def _setUpShaders(self, withColor, args={}):
		if withColor:
			args["solid_shader"] = self._shaders_collection.attributeColorPhongShader()
			args["solid_flat_shader"] = self._shaders_collection.attributeColorPhongFlatShader()
			args["nolight_solid_shader"] = self._shaders_collection.attributeColorShader()
			args["wireframe_shader"] = self._shaders_collection.uniformMaterialShader()
			args["nolight_wireframe_shader"] = self._shaders_collection.uniformMaterialShader()
		else:
			args["solid_shader"] = self._shaders_collection.uniformMaterialPhongShader()
			args["solid_flat_shader"] = self._shaders_collection.uniformMaterialPhongFlatShader()
			args["nolight_solid_shader"] = self._shaders_collection.uniformMaterialShader()
			args["wireframe_shader"] = self._shaders_collection.uniformMaterialPhongShader()
			args["nolight_wireframe_shader"] = self._shaders_collection.uniformMaterialShader()
			
	def _setupVerticalTransform(self, value=0.5, args={}):
		xform = QMatrix4x4()
		xform.translate(0.0, value, 0.0)
		args["transform"] = xform

	def _createCube(self, scene, withColor, args={}):
		args["name"] = "main-actor"
		self._setUpMaterial(args=args)
		self._setUpShaders(withColor, args=args)
		self._setupVerticalTransform(args=args)
		return MyCube(scene, **args)

	def _createCone(self, scene, withColor, args={}):
		args["name"] = "main-actor"
		self._setUpMaterial(args=args)
		self._setUpShaders(withColor, args=args)
		args["resolution"] = 30
		self._setupVerticalTransform(value=1.0, args=args)
		return MyCone(scene, **args)

	def _createCylinder(self, scene, withColor, args={}):
		args["name"] = "main-actor"
		self._setUpMaterial(args=args)
		self._setUpShaders(withColor, args=args)
		args["resolution"] = 30
		self._setupVerticalTransform(value=1.0, args=args)
		return MyCylinder(scene, **args)

	def _createSphere(self, scene, withColor, args={}):
		args["name"] = "main-actor"
		self._setUpMaterial(args=args)
		self._setUpShaders(withColor, args=args)
		args["level"] = 4
		self._setupVerticalTransform(value=1.0, args=args)
		return MyIcosahedron(scene, **args)

	def _createTeapot(self, scene, withColor, args={}):
		args["name"] = "main-actor"
		xform = QMatrix4x4()
		xform.rotate(-90.0, 1.0, 0.0, 0.0)
		args["transform"] = xform
		
		self._setUpMaterial(args)
		self._setUpShaders(withColor, args=args)
		return Teapot(scene, **args)

	def _createTorus(self, scene, withColor, args={}):
		args["name"] = "main-actor"
		xform = QMatrix4x4()
		xform.rotate(-90.0, 1.0, 0.0, 0.0)
		xform.translate(0.0, 0.0, 0.5)
		args["transform"] = xform
		self._setUpMaterial(args)
		self._setUpShaders(withColor, args=args)
		return Torus(scene, **args)

	def setGeometryShaderEffect(self, geometricShaderEffect=GeometryShaderEffect.NONE):
		args = {}
		self._shaders_collection = MyShaders(geometricShaderEffect)
		self._setUpShaders(withColor=False, args=args)
		return args