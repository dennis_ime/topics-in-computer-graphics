from MySource.Graphics.MyCone import MyCone
from MySource.Graphics.MyCube import MyCube 
from MySource.Graphics.MyCylinder import MyCylinder
from MySource.Graphics.MyIcosahedron import MyIcosahedron
from MySource.Graphics.Teapot import Teapot
from MySource.Graphics.FactoryActor.ActorType import ActorType
from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect
from enum import IntEnum

class FactoryActor(object):
	"""docstring for FactoryActor"""
	def __init__(self):
		super(FactoryActor, self).__init__()
		
	def _createCube(self, scene, withColor, args={}):
		raise NotImplementedError("createCube() must be implemented in child class")

	def _createCone(self, scene, withColor, args={}):
		raise NotImplementedError("setUpCone() must be implemented in child class")

	def _createCylinder(self, scene, withColor, args={}):
		raise NotImplementedError("setUpCylinder() must be implemented in child class")

	def _createSphere(self, scene, withColor, args={}):
		raise NotImplementedError("setUpSphere() must be implemented in child class")

	def _createTeapot(self, scene, withColor, args={}):
		raise NotImplementedError("setUpTeapot() must be implemented in child class")

	def setGeometryShaderEffect(self, geometricShaderEffect=GeometryShaderEffect.NONE):
		raise NotImplementedError("setGeometryShaderEffect() must be implemented in child class")		

	def create(self, actorType, scene, withColor=False, args={}):
		if actorType == ActorType.CUBE:
			return self._createCube(scene, withColor, args=args)
		elif actorType == ActorType.CONE:
			return self._createCone(scene, withColor, args=args)
		elif actorType == ActorType.CYLINDER:
			return self._createCylinder(scene, withColor, args=args)
		elif actorType == ActorType.SPHERE:
			return self._createSphere(scene, withColor, args=args)
		elif actorType == ActorType.TEAPOT:
			return self._createTeapot(scene, withColor, args=args)
		elif actorType == ActorType.TORUS:
			return self._createTorus(scene, withColor, args=args)
		else:
			raise ValueError("Invalid error type enumeration.")
