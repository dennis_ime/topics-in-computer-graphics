from PyQt5.QtGui import QVector3D, QMatrix4x4

from MySource.Graphics.MyCone import MyCone
from MySource.Graphics.MyCube import MyCube
from MySource.Graphics.MyCylinder import MyCylinder
from MySource.Graphics.MyIcosahedron import MyIcosahedron
from MySource.Graphics.Teapot import Teapot

from MySource.Graphics.FactoryActor.FactoryActor import FactoryActor
from MySource.Graphics.EpicShaders import EpicShaders
from MySource.Graphics.EpicMaterial import EpicMaterial

from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect
from MySource.Graphics.Texture.EpicTextures import EpicTextures

class FactoryActorEpic(FactoryActor):
	"""docstring for FactoryActorEpic"""
	def __init__(self):
		super(FactoryActorEpic, self).__init__()
		self._shaders_collection = EpicShaders()

	def _setUpMaterial(self, args):
		args["material"] = args.get("material", EpicMaterial())
		args["wireframe"] = args.get("wireframe", EpicMaterial(cbase=QVector3D(1.0, 0.0, 0.0)))

	def _setUpShaders(self, withColor, args={}):
		if withColor:
			args["solid_shader"] = args.get("solid_shader", self._shaders_collection.attributeColorEpicShader())
			args["solid_flat_shader"] = args.get("solid_flat_shader", self._shaders_collection.attributeColorEpicFlatShader())
			args["nolight_solid_shader"] = args.get("nolight_solid_shader", self._shaders_collection.attributeColorEpicShader())
			args["wireframe_shader"] = args.get("wireframe_shader", self._shaders_collection.uniformMaterialEpicShader())
			args["nolight_wireframe_shader"] = args.get("nolight_wireframe_shader", self._shaders_collection.uniformMaterialEpicShader())
		else:
			args["solid_shader"] = args.get("solid_shader", self._shaders_collection.uniformMaterialEpicShader())
			args["solid_flat_shader"] = args.get("solid_flat_shader", self._shaders_collection.uniformMaterialEpicFlatShader())
			args["nolight_solid_shader"] = args.get("nolight_solid_shader", self._shaders_collection.uniformMaterialEpicShader())
			args["wireframe_shader"] = args.get("wireframe_shader", self._shaders_collection.uniformMaterialEpicShader())
			args["nolight_wireframe_shader"] = args.get("nolight_wireframe_shader", self._shaders_collection.uniformMaterialEpicShader())

	def _setupVerticalTransform(self, value=0.5, args={}):
		xform = QMatrix4x4()
		xform.translate(0.0, value, 0.0)
		args["transform"] = args.get("transform", xform)

	def _setUpTextures(self, args):
		args["textures"] = args.get("textures", EpicTextures())

	def _createCube(self, scene, withColor, args={}):
		args["name"] = "main-actor"
		self._setUpTextures(args)
		self._setUpMaterial(args)
		self._setUpShaders(withColor, args)
		self._setupVerticalTransform(args=args)
		return MyCube(scene, **args)

	def _createCone(self, scene, withColor, args={}):
		args["name"] = "main-actor"
		args["resolution"] = args.get("resolution", 30)
		self._setUpTextures(args)
		self._setUpMaterial(args)
		self._setUpShaders(withColor, args)		
		self._setupVerticalTransform(value=1.0, args=args)
		return MyCone(scene, **args)

	def _createCylinder(self, scene, withColor, args={}):
		args["name"] = "main-actor"
		args["resolution"] = args.get("resolution", 30)
		self._setUpTextures(args)
		self._setUpMaterial(args)		
		self._setUpShaders(withColor, args)		
		self._setupVerticalTransform(value=1.0, args=args)
		return MyCylinder(scene, **args)

	def _createSphere(self, scene, withColor, args={}):
		args["name"] = "main-actor"
		args["level"] = args.get("level", 4)
		self._setUpTextures(args)
		self._setUpMaterial(args)
		self._setUpShaders(withColor, args)
		self._setupVerticalTransform(value=1.0, args=args)
		return MyIcosahedron(scene, **args)

	def _createTeapot(self, scene, withColor, args={}):
		args["name"] = "main-actor"
		args["textures"] =  args.get("textures", EpicTextures())
		
		xform = QMatrix4x4()
		xform.rotate(-90.0, 1.0, 0.0, 0.0)
		args["transform"] = args.get("transform", xform)
		
		self._setUpMaterial(args)
		self._setUpShaders(withColor, args)
		return Teapot(scene, **args)

	def setGeometryShaderEffect(self, geometricShaderEffect=GeometryShaderEffect.NONE):
		args = {}
		self._shaders_collection = EpicShaders(geometricShaderEffect)
		self._setUpShaders(withColor=False, args=args)
		return arg