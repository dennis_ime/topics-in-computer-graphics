from PyQt5.QtCore import QObject

from MySource.Graphics.Shader.Epic.EpicAttributeColorFlatShader import EpicAttributeColorFlatShader
from MySource.Graphics.Shader.Epic.EpicAttributeColorShader import EpicAttributeColorShader
from MySource.Graphics.Shader.Epic.EpicTexturedFlatShader import EpicTexturedFlatShader
from MySource.Graphics.Shader.Epic.EpicTexturedShader import EpicTexturedShader
from MySource.Graphics.Shader.Epic.EpicUniformMaterialFlatShader import EpicUniformMaterialFlatShader
from MySource.Graphics.Shader.Epic.EpicUniformMaterialShader import EpicUniformMaterialShader

from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect


class EpicShaders(QObject):

	def __init__(self, geometryEffect=GeometryShaderEffect.DEFAULT):
		super(EpicShaders).__init__()
		self.initialize(geometryEffect)

	def initialize(self, geometryEffect):
		self._uniformMaterialEpicShader = EpicUniformMaterialShader(geometryEffect)
		self._attributeColorEpicShader = EpicAttributeColorShader(geometryEffect)
		self._uniformMaterialEpicFlatShader = EpicUniformMaterialShader(geometryEffect)
		self._attributeColorEpicFlatShader = EpicAttributeColorFlatShader(geometryEffect)
		self._texturedShader = EpicTexturedShader(geometryEffect)
		self._texturedFlatShader = EpicTexturedFlatShader(geometryEffect)

	def uniformMaterialEpicShader(self):
		return self._uniformMaterialEpicShader

	def attributeColorEpicShader(self):
		return self._attributeColorEpicShader

	def uniformMaterialEpicFlatShader(self):
		return self._uniformMaterialEpicFlatShader

	def attributeColorEpicFlatShader(self):
		return self._attributeColorEpicFlatShader

	def texturedShader(self):
		return self._texturedShader

	def texturedFlatShader(self):
		return self._texturedFlatShader
		