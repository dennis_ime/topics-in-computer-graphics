import numpy as np
from PyQt5.QtGui import QVector3D

from OpenGL import GL
from MySource.Graphics.MyActor import MyActor
from MySource.Graphics.FactoryActor.ActorType import ActorType

class MyCube(MyActor):

    ## initialization
    def __init__(self, scene,  **kwargs):
        """Initialize actor."""
        super(MyCube, self).__init__(scene, mode=MyActor.MyRenderMode.Triangles, **kwargs)

        self._tangentBasisCalculator = kwargs.get("tangentBasisCalculator", None)
        self._vertices = None
        
        ## create actor
        self.initialize()


    @classmethod
    def isSelectable(self):
        """Returns true if actor is selectable"""
        return True


    def generateGeometry(self):
        """Generate geometry"""
        self._vertices = np.array([
            [-0.5, -0.5, -0.5],  
            [0.5, -0.5, -0.5], 
            [0.5,  0.5, -0.5],  
            [0.5,  0.5, -0.5],  
            [-0.5,  0.5, -0.5], 
            [-0.5, -0.5, -0.5],  

            [-0.5, -0.5,  0.5], 
            [0.5, -0.5,  0.5], 
            [0.5,  0.5,  0.5],  
            [0.5,  0.5,  0.5],  
            [-0.5,  0.5,  0.5],  
            [-0.5, -0.5,  0.5], 

            [-0.5,  0.5,  0.5], 
            [-0.5,  0.5, -0.5], 
            [-0.5, -0.5, -0.5], 
            [-0.5, -0.5, -0.5], 
            [-0.5, -0.5,  0.5], 
            [-0.5,  0.5,  0.5], 

            [0.5,  0.5,  0.5],  
            [0.5,  0.5, -0.5],  
            [0.5, -0.5, -0.5],  
            [0.5, -0.5, -0.5],  
            [0.5, -0.5,  0.5], 
            [0.5,  0.5,  0.5],  

            [-0.5, -0.5, -0.5], 
            [0.5, -0.5, -0.5],  
            [0.5, -0.5,  0.5],  
            [0.5, -0.5,  0.5],  
            [-0.5, -0.5,  0.5],  
            [-0.5, -0.5, -0.5],  

            [-0.5,  0.5, -0.5],  
            [0.5,  0.5, -0.5], 
            [0.5,  0.5,  0.5],  
            [0.5,  0.5,  0.5],  
            [-0.5,  0.5,  0.5],  
            [-0.5,  0.5, -0.5]], dtype=np.float32)

        self._normals = np.array([
            [0.0,  0.0, -1.0],
            [0.0,  0.0, -1.0],
            [0.0,  0.0, -1.0],
            [0.0,  0.0, -1.0],
            [0.0,  0.0, -1.0],
            [0.0,  0.0, -1.0],

            [0.0,  0.0,  1.0],
            [0.0,  0.0,  1.0],
            [0.0,  0.0,  1.0],
            [0.0,  0.0,  1.0],
            [0.0,  0.0,  1.0],
            [0.0,  0.0,  1.0],

            [-1.0,  0.0,  0.0],
            [-1.0,  0.0,  0.0],
            [-1.0,  0.0,  0.0],
            [-1.0,  0.0,  0.0],
            [-1.0,  0.0,  0.0],
            [-1.0,  0.0,  0.0],

            [1.0,  0.0,  0.0],
            [1.0,  0.0,  0.0],
            [1.0,  0.0,  0.0],
            [1.0,  0.0,  0.0],
            [1.0,  0.0,  0.0],
            [1.0,  0.0,  0.0],

            [0.0, -1.0,  0.0],
            [0.0, -1.0,  0.0],
            [0.0, -1.0,  0.0],
            [0.0, -1.0,  0.0],
            [0.0, -1.0,  0.0],
            [0.0, -1.0,  0.0],

            [0.0,  1.0,  0.0],
            [0.0,  1.0,  0.0],
            [0.0,  1.0,  0.0],
            [0.0,  1.0,  0.0],
            [0.0,  1.0,  0.0],
            [0.0,  1.0,  0.0]], dtype=np.float32)


        self._texCoords = np.array([
            [0.0, 0.0],
            [1.0, 0.0],
            [1.0, 1.0],
            [1.0, 1.0],
            [0.0, 1.0],
            [0.0, 0.0],

            [0.0, 0.0],
            [1.0, 0.0],
            [1.0, 1.0],
            [1.0, 1.0],
            [0.0, 1.0],
            [0.0, 0.0],

            [1.0, 0.0],
            [1.0, 1.0],
            [0.0, 1.0],
            [0.0, 1.0],
            [0.0, 0.0],
            [1.0, 0.0],

            [1.0, 0.0],
            [1.0, 1.0],
            [0.0, 1.0],
            [0.0, 1.0],
            [0.0, 0.0],
            [1.0, 0.0],

            [0.0, 1.0],
            [1.0, 1.0],
            [1.0, 0.0],
            [1.0, 0.0],
            [0.0, 0.0],
            [0.0, 1.0],

            [0.0, 1.0],
            [1.0, 1.0],
            [1.0, 0.0],
            [1.0, 0.0],
            [0.0, 0.0],
            [0.0, 1.0]],np.float32)


    def initialize(self):
        """Creates cube's geometry"""
        if self._vertices is None:
            self.generateGeometry()

        #texCoords = self.generateUnitSphericalTexCoordFromVertices(self._vertices)
        ## create object
        self.create(self._vertices, normals=self._normals, texcoords=self._texCoords, tangentBasisCalculator=self._tangentBasisCalculator)    

    def render(self):
        """Render cube"""
        GL.glDrawArrays(self._render_mode, 0, len(self._vertices))

    def actorType(self):
        return ActorType.CUBE