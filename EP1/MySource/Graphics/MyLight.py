from abc import ABC, abstractmethod
from enum import IntEnum

from PyQt5.QtCore import QObject
from PyQt5.QtGui import QVector3D, QVector4D

# ================ EPIC LIGHT TYPE ======================================
class MyLightType(IntEnum):
	POINT = 0,
	DIRECTIONAL = 1
	HEMISPHERICAL = 2

# ========================== EPIC LIGHT ================================
class MyLight(QObject):
	@abstractmethod
	def type(self):
		pass

# ====================== LIGHT COLOR ====================================
class MyLightColor(QObject):
	def __init__(self, **kwargs):
		super(MyLightColor, self).__init__()
		color = kwargs.get("color", None)
		if color is None:
			self._ambientColor = kwargs.get("ambient", QVector3D(0.2, 0.2, 0.2))
			self._diffuseColor = kwargs.get("diffuse", QVector3D(0.8, 0.8, 0.8))
			self._specularColor = kwargs.get("specular", QVector3D(0.0, 0.0, 0.0))
		else:
			self._ambientColor = self._diffuseColor = self._specularColor = color

	@property
	def ambientColor(self):
		return self._ambientColor

	@ambientColor.setter
	def ambientColor(self, value):
		self._ambientColor = value

	@property
	def diffuseColor(self):
		return self._diffuseColor

	@diffuseColor.setter
	def diffuseColor(self, value):
		self._diffuseColor = value

	@property
	def specularColor(self):
		return self._specularColor

	@specularColor.setter
	def specularColor(self, value):
		self._specularColor = value

	@property
	def color(self):
		return self._diffuseColor

	@color.setter
	def color(self, color):
		self._diffuseColor = self._specularColor = self._ambientColor = color

# ====================== EPIC POINT LIGHT ===============================
class MyPointLight(MyLight):
	def __init__(self, **kwargs):
		super(MyPointLight, self).__init__()
		self._position = kwargs.get("position", QVector3D(0.0, 0.0, 0.0))	
		self._color = kwargs.get("color", MyLightColor())
		self._attenuation = kwargs.get("attenuation", QVector3D(1.0, 0.02, 0.002))
		self._headlight = kwargs.get("headlight", False)
		self._radius = kwargs.get("radius", 40.0)
		self._attenuationConstant = kwargs.get("attenuationConstant", 0.1)
		self._attenuationLinear = kwargs.get("attenuationLinear", 0.1)
		self._attenuationQuad = kwargs.get("attenuationQuad", 0.1)

	@property
	def position(self):
		return self._position

	@position.setter
	def position(self, value):
		self._position = value

	@property
	def color(self):
		return self._color

	@color.setter
	def color(self, value):
		self._color = value

	@property
	def headlight(self):
		return self._headlight

	@headlight.setter
	def headlight(self, value):
		self._headlight = value

	@property
	def radius(self):
		return self._radius

	@radius.setter
	def radius(self, value):
		self._radius = value

	@property
	def attenuationConstant(self):
		return self._attenuationConstant

	@attenuationConstant.setter
	def attenuationConstant(self, value):
		self._attenuationConstant = value

	@property
	def attenuationLinear(self):
		return self._attenuationLinear

	@attenuationLinear.setter
	def attenuationLinear(self, value):
		self._attenuationLinear = value

	@property
	def attenuationQuad(self):
		return self._attenuationQuad

	@attenuationQuad.setter
	def attenuationQuad(self, value):
		self._attenuationQuad = value

	def type(self):
		return MyLightType.POINT

# ====================  EPIC DIRECTIONAL LIGHT ============================ #
class MyDirectionalLight(MyLight):
	def __init__(self, **kwargs):
		super(MyDirectionalLight, self).__init__()
		self._direction = kwargs.get("direction", QVector3D(0.0, 0.0, 0.0))
		self._color = kwargs.get("color", MyLightColor())
		self._headlight = kwargs.get("headlight", False)

	@property
	def direction(self):
		return self._direction

	@direction.setter
	def direction(self, value):
		self._direction = value

	@property
	def color(self):
		return self._color

	@color.setter
	def color(self, value):
		self._color = value

	@property
	def headlight(self):
		return self._headlight

	@headlight.setter
	def headlight(self, value):
		self._headlight = value

	def type(self):
		return MyLightType.DIRECTIONAL

# ==================== EPIC HEMISPHECAL LIGHT ============================= #
class MyHemisphericalLight(MyLight):
	def __init__(self, skyColor=QVector3D(1,1,1), groundColor=QVector3D(0,0,0)):
		super(MyHemisphericalLight, self).__init__()
		self._skyColor = skyColor
		self._groundColor = groundColor

	@property
	def skyColor(self):
		return self._skyColor

	@skyColor.setter
	def skyColor(self, value):
		self._skyColor = value

	@property
	def groundColor(self):
		return self._groundColor

	@groundColor.setter
	def groundColor(self, value):
		self._groundColor = value

	def type(self):
		return MyLightType.HEMISPHERICAL