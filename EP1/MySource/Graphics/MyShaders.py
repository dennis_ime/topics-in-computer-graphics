from PyQt5.QtCore import QObject

from MySource.Graphics.Shader.BlinnPhong.BPAttributeColorFlatShader import BPAttributeColorFlatShader
from MySource.Graphics.Shader.BlinnPhong.BPAttributeColorShader import BPAttributeColorShader
from MySource.Graphics.Shader.BlinnPhong.BPTexturedFlatShader import BPTexturedFlatShader
from MySource.Graphics.Shader.BlinnPhong.BPTexturedShader import BPTexturedShader
from MySource.Graphics.Shader.BlinnPhong.BPUniformMaterialFlatShader import BPUniformMaterialFlatShader
from MySource.Graphics.Shader.BlinnPhong.BPUniformMaterialShader import BPUniformMaterialShader

from MySource.Graphics.Shader.Objects.AttributeColorShader import AttributeColorShader
from MySource.Graphics.Shader.Objects.BackgroundShader import BackgroundShader
from MySource.Graphics.Shader.Objects.UniformMaterialShader import UniformMaterialShader
from MySource.Graphics.Shader.Objects.WireframeMaterialShader import WireframeMaterialShader
from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect


## singleton shader class 
class MyShaders(QObject):

	def __init__(self, geometryEffect=GeometryShaderEffect.NONE):
		super(MyShaders).__init__()
		self.initialize(geometryEffect)

	def initialize(self, geometryEffect=GeometryShaderEffect.NONE):
		## create background shader program
		self._backgroundShader = BackgroundShader(geometryEffect)

		## create uniform material shader with no lighting 
		self._wireframeMaterialShader = WireframeMaterialShader(geometryEffect)

		## create uniform material shader with no lighting 
		self._uniformMaterialShader = UniformMaterialShader(geometryEffect)

		## create uniform material with no lighting calculations
		self._attributeColorShader = AttributeColorShader(geometryEffect)

		## create Phong mesh shader
		self._uniformMaterialPhongShader = BPUniformMaterialShader(geometryEffect)

		## create color-based Phong mesh shader
		self._attributeColorPhongShader = BPAttributeColorShader(geometryEffect)

		## create Phong mesh shader
		self._uniformMaterialPhongFlatShader = BPUniformMaterialFlatShader(geometryEffect)

		## create color-based Phong mesh shader
		self._attributeColorPhongFlatShader = BPAttributeColorFlatShader(geometryEffect)

		## create simple textured-based mesh shader
		self._texturedShader = BPTexturedShader(geometryEffect)

		## create simple textured-based mesh flat shader
		self._texturedFlatShader = BPTexturedFlatShader(geometryEffect)

	def backgroundShader(self):
		return self._backgroundShader

	def uniformMaterialShader(self):
		return self._uniformMaterialShader

	def wireframeMaterialShader(self):
		return self._wireframeMaterialShader

	def attributeColorShader(self):
		return self._attributeColorShader
		
	def uniformMaterialPhongShader(self):
		return self._uniformMaterialPhongShader

	def attributeColorPhongShader(self):
		return self._attributeColorPhongShader

	def uniformMaterialPhongFlatShader(self):
		return self._uniformMaterialPhongFlatShader

	def attributeColorPhongFlatShader(self):
		return self._attributeColorPhongFlatShader
		
	def texturedShader(self):
		return self._texturedShader

	def texturedFlatShader(self):
		return self._texturedFlatShader


