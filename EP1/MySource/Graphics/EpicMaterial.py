from PyQt5.QtCore import QObject
from PyQt5.QtGui import QVector3D

class EpicMaterial(QObject):
	def __init__(self, **kargs):
		super(EpicMaterial, self).__init__()

		self._roughness = kargs.get("roughness", 0.1)
		self._metallic = kargs.get("metallic", 0.5)
		self._specular_coefficient = kargs.get("specular_coefficient", 0.7)
		self._cbase = kargs.get("cbase", QVector3D(1.0, 0.0, 0.0))

	@property
	def roughness(self):
		return self._roughness

	@roughness.setter
	def roughness(self, value):
		self._roughness = value

	@property
	def metallic(self):
		return self._metallic

	@metallic.setter
	def metallic(self, value):
		self._metallic = value

	@property
	def specular_coefficient(self):
		return self._specular_coefficient

	@specular_coefficient.setter
	def specular_coefficient(self, value):
		self._specular_coefficient = value

	@property
	def cbase(self):
		return self._cbase

	@cbase.setter
	def cbase(self, value):
		self._cbase = value

	def diffuseColor(self):
		return (1 - self._metallic) * self._cbase

	def _learp(self, x1, x2, m):
		return (x1 * m) + (x2 * (1 - m))

	#def specularColor(self):
	#	sc = self._specular_coefficient
	#	cbase = self._cbase
	#	m = self._metallic
	#	return self._learp(0.05 * sc, cbase, m)