import math
import numpy as np

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from OpenGL import GL

from Source.Graphics.Trackball import Trackball
from Source.Graphics.Camera import Camera
from MySource.Graphics.MyScene import MyScene
from Source.Graphics.Group import Group
from MySource.Graphics.MyGnomon import MyGnomon
from MySource.Graphics.FactoryActor.ActorType import ActorType
from MySource.Graphics.MyLight import *
from Source.Graphics.Material import Material
from MySource.Graphics.MyActor import MyActor
from MySource.Graphics.MyWorld import MyWorld

# TODO: Include an EpicActor
from MySource.Graphics.MyCube import MyCube
from MySource.Graphics.MyIcosahedron import MyIcosahedron
from MySource.Graphics.Teapot import Teapot
from MySource.Graphics.FactoryActor.FactoryActorBlinnPhong import FactoryActorBlinnPhong
from MySource.Graphics.FactoryActor.FactoryActorEpic import FactoryActorEpic
from MySource.Graphics.FactoryActor.FactoryActor import FactoryActor 

from MySource.Graphics.EpicMaterial import EpicMaterial

from MySource.Graphics.Shader.Epic.EpicNormalMappingShader import EpicNormalMappingShader
from MySource.Graphics.Shader.Epic.EpicUniformMaterialShader import EpicUniformMaterialShader

from MySource.Graphics.FactoryActor.FactoryActor import ActorType
from MySource.Graphics.TangentBasisCalculator import TangentBasisCalculator

from enum import IntEnum

class MyRenderer(QOpenGLWidget):		

	def __init__(self, parent=None, **kwargs):
		super(MyRenderer, self).__init__(parent)

		self._parent = parent

		self._lighting = kwargs.get("lighting", True)
		self._antialiasing = kwargs.get("antialiasing", False)
		self._statistics = kwargs.get("statistics", True)
		self._actorType = kwargs.get("actorType", ActorType.CUBE)
		self._material = kwargs.get("material", Material())

		self._home_rotation = QQuaternion.fromAxisAndAngle(QVector3D(1.0, 0.0, 0.0), 25.0) * QQuaternion.fromAxisAndAngle(QVector3D(0.0, 1.0, 0.0), -50.0)

		self._trackball = Trackball(velocity=0.05, axis=QVector3D(0.0, 1.0, 0.0), mode=Trackball.TrackballMode.Planar, 
			rotation=self._home_rotation, paused=True)

		self._world = MyWorld(self, home_position=QVector3D(0, 0, 3.5))
		self._animating = True
		self._initialized = False
		self.setAutoFillBackground(False)

	def printOpenGLInformation(self, format, verbosity=0):
		print("\n*** OpenGL context information ***")
		print("Vendor: {}".format(GL.glGetString(GL.GL_VENDOR).decode('UTF-8')))
		print("Renderer: {}".format(GL.glGetString(GL.GL_RENDERER).decode('UTF-8')))
		print("OpenGL version: {}".format(GL.glGetString(GL.GL_VERSION).decode('UTF-8')))
		print("Shader version: {}".format(GL.glGetString(GL.GL_SHADING_LANGUAGE_VERSION).decode('UTF-8')))
		print("Maximum samples: {}".format(GL.glGetInteger(GL.GL_MAX_SAMPLES)))
		print("\n*** QSurfaceFormat from context ***")
		print("Depth buffer size: {}".format(format.depthBufferSize()))
		print("Stencil buffer size: {}".format(format.stencilBufferSize()))
		print("Samples: {}".format(format.samples()))
		print("Red buffer size: {}".format(format.redBufferSize()))
		print("Green buffer size: {}".format(format.greenBufferSize()))
		print("Blue buffer size: {}".format(format.blueBufferSize()))
		print("Alpha buffer size: {}".format(format.alphaBufferSize()))

	def initializeGL(self):
		if not self._initialized:
			self.printOpenGLInformation(self.context().format())

			self._gnomon = MyGnomon(self)

			self._world.camera.setRotation(self._trackball.rotation().inverted())
			self._gnomon.camera.setRotation(self._trackball.rotation().inverted())

			GL.glEnable(GL.GL_DEPTH_TEST)
			GL.glEnable(GL.GL_DEPTH_CLAMP)
			GL.glEnable(GL.GL_MULTISAMPLE)
			GL.glEnable(GL.GL_FRAMEBUFFER_SRGB)

			if self._antialiasing:
				GL.glEnable(GL.GL_POLYGON_SMOOTH)
				GL.glEnable(GL.GL_BLEND)

				GL.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)
				GL.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_NICEST)
				GL.glHint(GL.GL_POLYGON_SMOOTH_HINT, GL.GL_NICEST)

				GL.glPointSize(5)
				GL.glLineWidth(1)

			GL.glClearColor(0.75, 0.76, 0.76, 0.0)

			self._world.initialize()
			self._gnomon.initialize()

			self._timer = QTimer(self)
			self._timer.setTimerType(Qt.PreciseTimer)
			self._timer.timeout.connect(self.updateScene)
			self._timer.start()

			self._elapsed_timer = QElapsedTimer()
			self._elapsed_timer.restart()
			self._frameElapsed = 0
			self._gpuElapsed = 0

			self._initialized = True

			#xform = QMatrix4x4()
			#xform.rotate(-90.0, 1.0, 0.0, 0.0)
			#self._actorFactory = FactoryActorEpic()
			self._actorFactory = FactoryActorBlinnPhong()
			self._actor = self._actorFactory.create(self._actorType, self._world, withColor=False)
			self._world.addActor(self._actor)
			self._actor.setMaterial(self._material)
			#self._world.addActor(MyCube(self._world))
			#self._world.addActor(MyIcosahedron(self._world, level=2))

		else:
			self._world.initialize()
			self._gnomon.initialize()

		self._query = GL.glGenQueries(1)

	def clear(self):
		self._world.clear()
		self.update()

	def renderTimeEstimates(self):
		return [self._frameElapsed, self._gpuElapsed]

	@property
	def lighting(self):
		return self._lighting

	def setDrawStyle(self, style):
		self._draw_style = style

	def activeSceneCamera(self):
		return self._world.camera

	def setAnimating(self, value):
		self._animating = value

	def isAnimating(self):
		return self._animating

	def updateScene(self):
		if self.isAnimating():
			self.update()

	def renderScene(self):
		self._world.camera.setRotation(self._trackball.rotation().inverted())
		self._gnomon.camera.setRotation(self._trackball.rotation().inverted())

		self._world.render()
		self._gnomon.render()

	def paintGL(self):
		if self._statistics:
			GL.glBeginQuery(GL.GL_TIME_ELAPSED, self._query)
			self.renderScene()
			GL.glEndQuery(GL.GL_TIME_ELAPSED)

			ready = False
			while not ready:
				ready = GL.glGetQueryObjectiv(self._query, GL.GL_QUERY_RESULT_AVAILABLE)
			self._gpuElapsed = GL.glGetQueryObjectuiv(self._query, GL.GL_QUERY_RESULT) / 10000000.0

		else:
			self.renderScene()

	def resizeGL(self, width, height):
		self._world.camera.setAspectRatio(width / float(height if height > 0.0 else 1.0))

	def pan(self, point, state="start"):
		if state == "start":
			self._lastPanningPos = point
		elif state == "move":
			delta = QLineF(self._lastPanningPos, point)
			self._lastPanningPos = point
			direction = QVector3D(-delta.dx(), -delta.dy(), 0.0).normalized()
			newpos = self._world.camera.position + delta.length() * 2.0 * direction
			self._world.camera.setPosition(newpos)

	def mousePressEvent(self, event):
		super(MyRenderer, self).mousePressEvent(event)

		if event.isAccepted():
			return

		if event.buttons() & Qt.LeftButton:
			self._trackball.press(self._pixelPosToViewPos(event.localPos()), QQuaternion())
			self._trackball.start()
			event.accept()
			if not self.isAnimating():
				self.update()

		elif event.buttons() & Qt.RightButton:
			self.pan(self._pixelPosToViewPos(event.localPos()), state="start")
			self.update()

	def mouseMoveEvent(self, event):
		super(MyRenderer, self).mouseMoveEvent(event)

		if event.isAccepted():
			return

		if event.buttons() & Qt.LeftButton:
			self._trackball.move(self._pixelPosToViewPos(event.localPos()), QQuaternion())
			event.accept()
			if not self.isAnimating():
				self.update()

		elif event.buttons() & Qt.RightButton:
			self.pan(self._pixelPosToViewPos(event.localPos()), state="move")
			self.update()

	def mouseReleaseEvent(self, event):
		super(MyRenderer, self).mouseReleaseEvent(event)

		if event.isAccepted():
			return

		if event.button() == Qt.LeftButton:
			self._trackball.release(self._pixelPosToViewPos(event.localPos()), QQuaternion())
			event.accept()
			if not self.isAnimating():
				self._trackball.stop()
				self.update()

	def wheelEvent(self, event):
		super(MyRenderer, self).wheelEvent(event)

		self.zoom(-event.angleDelta().y() / 950.0)
		event.accept()
		self.update()

	def zoom(self, diffvalue):
		multiplicator = math.exp(diffvalue)

		camera = self._world.camera
		if camera.lens == Camera.Lens.Orthographic:
			camera.scaleHeight(multiplicator)
		else:
			old_focal_dist = camera.focalDistance
			new_focal_dist = old_focal_dist * multiplicator

			direction = camera.orientation * QVector3D(0.0, 0.0, -1.0)
			newpos = camera.position + (new_focal_dist - old_focal_dist) * -direction

			camera.setPosition(newpos)
			camera.setFocalDistance(new_focal_dist)

	def viewFront(self):
		self._trackball.reset(QQuaternion())
		self.update()

	def viewBack(self):
		self._trackball.reset(QQuaternion.fromAxisAndAngle(QVector3D(0.0, 1.0, 0.0), 180.0))
		self.update()

	def viewLeft(self):
		self._trackball.reset(QQuaternion.fromAxisAndAngle(QVector3D(0.0, 1.0, 0.0), -90.0))
		self.update()

	def viewRight(self):
		self._trackball.reset(QQuaternion.fromAxisAndAngle(QVector3D(0.0, 1.0, 0.0), 90.0))
		self.update()

	def viewTop(self):
		self._trackball.reset(QQuaternion.fromAxisAndAngle(QVector3D(1.0, 0.0, 0.0), 90.0))
		self.update()

	def viewBottom(self):
		self._trackball.reset(QQuaternion.fromAxisAndAngle(QVector3D(1.0, 0.0, 0.0), -90.0))
		self.update()

	def createGridLines(self):
		self.makeCurrent()
		self._world.createGridLines()
		self.doneCurrent()

	def cameraLensChanged(self, lens):
		self._world.setCameraLens(lens)
		self._gnomon.setCameraLens(lens)
		self.update()

	def storeCamera(self):
		self._world.storeCamera()

	def recallCamera(self):
		self._world.recallCamera()
		self._trackball.reset(self._world.camera.rotation.inverted())
		self.update()

	def resetCamera(self):
		self._world.resetCamera()
		self._trackball.reset(self._home_rotation)
		self.update()

	def drawStyleChanged(self, index):
		self._world.setDrawStyle(MyScene.DrawStyle.Styles[index])
		self.update()

	def lightingChanged(self, state):
		self._world.setLighting(state)
		self.update()

	def shadingChanged(self, index):
		self._world.setShading(MyScene.Shading.Types[index])
		self.update()

	def headLightChanged(self, state):
		self._world.light.setHeadLight(state)
		self.update()

	def directionalLightChanged(self, state):
		self._world.light.setDirectional(state)
		self.update()

	def enableProfiling(self, enable):
		self._statistics = enable

	def enableAnimation(self, enable):
		self.setAnimating(enable)
		if not enable:
			self._trackball.stop()

	def _pixelPosToViewPos(self, point):
		return QPointF(2.0 * float(point.x()) / self.width() - 1.0, 1.0 - 2.0 * float(point.y()) / self.height())


	# ================ NEW METHODS FOR THE EXERCISE ======================================================= #
	def changeActor(self, index, material=Material(), pargs={}):
		self.makeCurrent()
		self._actor.destroy()
		self._world.removeActor(self._actor)
		self._actor = self._actorFactory.create(ActorType.Types[index], self._world, withColor=False, args=pargs)
		self._actor.setMaterial(material)
		self._world.addActor(self._actor)

	# =============================== Blinn Phong Material set data ======================================= #
	def setBPMaterialEmission(self, color):
		self._actor.material.emissionColor = color

	def setBPMaterialAmbientColor(self, color):
		self._actor.material.ambientColor = color

	def setBPMaterialDiffuseColor(self, color):
		self._actor.material.diffuseColor = color

	def setBPMaterialSpecularColor(self, color):
		self._actor.material.specularColor = color

	def setBPMaterialShininess(self, value):
		self._actor.material.shininess = value

	def initialDefaultBPMaterial(self):
		return Material()

	# ================================== Blinn Phong Point Light set data ========================================= #
	def setPointLightAmbientColor(self, color):
		self._world.light.color.ambientColor = color

	def setPointLightDiffuseColor(self, color):
		self._world.light.color.diffuseColor = color

	def setPointLightSpecularColor(self, color):
		self._world.light.color.specularColor = color

	def setPointLightPositionX(self, value):
		self._world.light.position.setX(value)

	def setPointLightPositionY(self, value):
		self._world.light.position.setY(value)

	def setPointLightPositionZ(self, value):
		self._world.light.position.setZ(value)

	def setPointLightAttenuation(self, color):
		self._world.light.attenuation = color

	def setPointLightHeadlight(self, value):
		self._world.light.headlight = value

	def setPointLightRadius(self, value):
		self._world.light.radius = value

	def setPointLightConstantAttenuation(self, value):
		self._world.light.attenuationConstant = value

	def setPointLightLinearAttenuation(self, value):
		self._world.light.attenuationLinear = value

	def setPointLightQuadAttenuation(self, value):
		self._world.light.attenuationQuad = value

	# ============================= Blinn Phong Directional Light set data ========================================= #
	def setDirectionalLightAmbientColor(self, color):
		self._world.light.color.ambientColor = color

	def setDirectionalLightDiffuseColor(self, color):
		self._world.light.color.diffuseColor = color

	def setDirectionalLightSpecularColor(self, color):
		self._world.light.color.specularColor = color

	def setDirectionalLightDirectionX(self, value):
		self._world.light.direction.setX(value)

	def setDirectionalLightDirectionY(self, value):
		self._world.light.direction.setY(value)

	def setDirectionalLightDirectionZ(self, value):
		self._world.light.direction.setZ(value)

	def setDirectionalLightHeadlight(self, value):
		self._world.light.headlight = value

	# ========================== Blinn Phong Hemispherical Light set data ========================================== #
	def setHemisphericalLightSkyDiffuseColor(self, color):
		self._world.light.skyColor.diffuseColor = color

	def setHemisphericalLightSkySpecularColor(self, color):
		self._world.light.skyColor.specularColor = color

	def setHemisphericalLightSkyAmbientColor(self, color):
		self._world.light.skyColor.ambientColor = color

	def setHemisphericalLightGroundDiffuseColor(self, color):
		self._world.light.groundColor.diffuseColor = color

	def setHemisphericalLightGroundSpecularColor(self, color):
		self._world.light.groundColor.specularColor = color

	def setHemisphericalLightGroundAmbientColor(self, color):
		self._world.light.groundColor.ambientColor = color

	def changeBPLight(self, lightType):
		lightColor = MyLightColor(
			ambient=QVector3D(0.5, 0.5, 0.5),
			diffuse=QVector3D(1.0, 1.0, 1.0),
			specular=QVector3D(1.0, 1.0, 1.0))

		if lightType == MyLightType.POINT:
			light = MyPointLight(color=lightColor,
				position=QVector3D(2.0, 2.0, 0.5),
				headlight=False)

		elif lightType == MyLightType.DIRECTIONAL:
			light = MyDirectionalLight(color=lightColor,
				direction=QVector3D(1.0, 1.0, 0.5),
				headlight=False)

		elif lightType == MyLightType.HEMISPHERICAL:
			groundColor = MyLightColor(
			ambient=QVector3D(0.2, 0.2, 0.2),
			diffuse=QVector3D(0.2, 0.2, 0.2),
			specular=QVector3D(0.2, 0.2, 0.2))

			light = MyHemisphericalLight(skyColor=lightColor, 
				groundColor=groundColor)

		self._world.setLight(light)

	def changeEpicLight(self, lightType):
		lightColor = MyLightColor(color=QVector3D(0.5, 0.5, 0.5))

		if lightType == MyLightType.POINT:
			light = MyPointLight(color=lightColor,
				position=QVector3D(2.0, 2.0, 0.5),
				headlight=False)

		elif lightType == MyLightType.DIRECTIONAL:
			light = MyDirectionalLight(color=lightColor,
				direction=QVector3D(1.0, 1.0, 0.5),
				headlight=False)

		elif lightType == MyLightType.HEMISPHERICAL:
			groundColor = MyLightColor(color=QVector3D(0.1, 0.1, 0.1))

			light = MyHemisphericalLight(skyColor=lightColor, 
				groundColor=groundColor)

		self._world.setLight(light)


	def getLight(self):
		return self._world.light

	# ====================== EPIC LIGHT =============================================== #
	def setPointLightColor(self, color):
		self._world.light.color.color = color

	def setDirectionalLightColor(self, color):
		self._world.light.color.color = color

	def setHemisphericalLightGroundColor(self, color):
		self._world.light.groundColor.color = color

	def setHemisphericalLightSkyColor(self, color):
		self._world.light.skyColor.color = color

	# ================================== EPIC MATERIAL ================================== #
	def setEpicMaterialRoughness(self, roughness):
		self._actor.material.roughness = roughness

	def setEpicMaterialMetallic(self, metallic):
		self._actor.material.metallic = metallic

	def setEpicMaterialSpecularCoefficient(self, specularCoefficient):
		self._actor.material.specular_coefficient = specularCoefficient

	def setEpicMaterialCBase(self, cbase):
		self._actor.material.cbase = cbase

	def initialDefaultEpicMaterial(self):
		return self._actor.material

	# ====================== CHANGE SHADING PROGRAM =================================== #
	def toShadingProgramBlinPhong(self):
		self.makeCurrent()
		self._actorFactory = FactoryActorBlinnPhong()
		self._actor.destroy()
		self._world.removeActor(self._actor)
		self._actor = self._actorFactory.create(ActorType.CUBE, self._world, withColor=False, args={})
		self._world.addActor(self._actor)
		self.changeBPLight(MyLightType.POINT)

	def toShadingProgramEpic(self):
		self.makeCurrent()
		self._actorFactory = FactoryActorEpic()
		self._actor.destroy()
		self._world.removeActor(self._actor)
		self._actor = self._actorFactory.create(ActorType.CUBE, self._world, withColor=False, args={})
		self._world.addActor(self._actor)
		self._world.setLight(MyPointLight())
		self.changeEpicLight(MyLightType.POINT)

	# ================================== EPIC TEXTURES ========================================== #
	# ========= set 'roughness' =================================== #
	def setEpicRoughnessTexture(self, filepath):
		self.makeCurrent()
		self._actor.textures.setRoughnessTexture(filepath)

	def setEpicMetallicTexture(self, filepath):
		self.makeCurrent()
		self._actor.textures.setMetallicTexture(filepath)

	def setEpicSpecularTexture(self, filepath):
		self.makeCurrent()
		self._actor.textures.setSpecularTexture(filepath)

	def setEpicCBaseTexture(self, filepath):
		self.makeCurrent()
		self._actor.textures.setCBaseTexture(filepath)

	def setEpicNormalMappingTexture(self, filepath):
		self.makeCurrent()

		textures = self._actor.textures
		material = self._actor.material
		transform = self._actor.transform()
		actorType = self._actor.actorType()
		shader = EpicNormalMappingShader()
		textures.setNormalMappingTexture(filepath)
		
		args = { "textures": textures, "material": material, "transform": transform, 
			"tangentBasisCalculator": TangentBasisCalculator(), "solid_shader": shader }

		self.makeCurrent()
		self._actor.destroy()
		self._world.removeActor(self._actor)
		self._actor = self._actorFactory.create(actorType, self._world, args=args)
		self._actor.setMaterial(material)
		self._actor.setTransform(transform)
		self._actor.setSolidShader(shader)
		self._world.addActor(self._actor)

	# ======================= EPIC TEXTURE DESTROY =============================================== #
	def destroyEpicRoughnessTexture(self):
		self.makeCurrent()
		self._actor.textures.destroyRoughnessTexture()

	def destroyEpicMetallicTexture(self):
		self.makeCurrent()
		self._actor.textures.destroyMetallicTexture()

	def destroyEpicSpecularTexture(self):
		self.makeCurrent()
		self._actor.textures.destroySpecularTexture()

	def destroyCBaseTexture(self):
		self.makeCurrent()
		self._actor.textures.destroyCBaseTexture()

	def destroyNormalMappingTexture(self):
		self.makeCurrent()

		textures = self._actor.textures
		material = self._actor.material
		transform = self._actor.transform
		actorType = self._actor.actorType()
		shader = EpicUniformMaterialShader()

		#textures.destroyNormalMappingTexture()

		args = {"textures": textures, "material": material, "transform": transform, 
			"solid_shader": shader}
		
		self._world.removeActor(self._actor)
		self._actor = self._actorFactory.create(actorType, self._world, args)
		self._world.addActor(self._actor)

	# ============== GEOMETRY SHADER ============================== #
	def setGeometryShaderEffect(self, geometryShaderEffect):
		args = self._actorFactory.setGeometryShaderEffect(geometryShaderEffect)
		self._actor.setSolidShader(args["solid_shader"])
		self._actor.setSolidFlatShader(args["solid_flat_shader"])
		self._actor.setNoLightSolidShader(args["nolight_solid_shader"])
		self._actor.setWireframeShader(args["wireframe_shader"])
		self._actor.setNoLightWireframeShader(args["nolight_wireframe_shader"])

	def setSpikyWidth(self, value):
		self.makeCurrent()
		self._actor.solidShader.geometryShader.width = value
		self._actor.solidFlatShader.geometryShader.width = value

		#self._actor.solidShader.updateGeometryShaderUniforms()
		#self._actor.solidFlatShader.updateGeometryShaderUniforms()
		

	def setSpikyHeight(self, value):
		self.makeCurrent()
		self._actor.solidShader.geometryShader.height = value
		self._actor.solidFlatShader.geometryShader.height = value		

		#self._actor.solidShader.updateGeometryShaderUniforms()
		#self._actor.solidFlatShader.updateGeometryShaderUniforms()		
