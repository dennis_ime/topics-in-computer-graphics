import math
import ctypes
import numpy as np

from PyQt5.QtCore import QObject
from PyQt5.QtGui import QMatrix4x4, QVector3D, QVector4D, QOpenGLShader, QOpenGLShaderProgram, QOpenGLBuffer
from PyQt5.QtGui import QOpenGLVertexArrayObject

from OpenGL import GL
#from Source.Graphics.Shaders import Shaders
from Source.Graphics.Material import Material
#from MySource.Graphics.EpicMaterial import EpicMaterial
from MySource.Graphics.MyShaders import MyShaders

class MyActor(QObject):

	class MyRenderType:
		NoType = 0        ## No special state changes are done.
		Solid = 1         ## Depth testing and depth writing are enabled.
		Transparent = 2   ## Depth testing is enabled, depth writing is disabled.
		Overlay = 3       ## Depth testing is disabled.
		Types = [NoType, Solid, Transparent, Overlay]

	## The mode to render objects in. These correspond to OpenGL render modes.
	class MyRenderMode:
		Points = GL.GL_POINTS
		Lines = GL.GL_LINES
		LineLoop = GL.GL_LINE_LOOP
		LineStrip = GL.GL_LINE_STRIP
		Triangles = GL.GL_TRIANGLES
		TriangleStrip = GL.GL_TRIANGLE_STRIP
		TriangleFan = GL.GL_TRIANGLE_FAN
		Modes = [Points, Lines, LineLoop, LineStrip, Triangles, TriangleStrip, TriangleFan]

	## initialization
	def __init__(self, scene, **kwargs):
		""" Initialize actor. """
		super(MyActor).__init__()

		self._scene = scene
		self._transform = kwargs.get("transform", QMatrix4x4())
		self._render_mode = kwargs.get("mode", MyActor.MyRenderMode.Triangles)
		self._render_type = kwargs.get("type", MyActor.MyRenderType.Solid)
		self._material = kwargs.get("material", Material())
		self._wireframe = kwargs.get("wireframe", Material(diffuse=QVector3D(0.25, 0.25, 0.25)))
		self._viewport = kwargs.get("viewport", (0.0, 0.0, 1.0, 1.0))

		self._name = kwargs.get("name", "EpicActor" + str(id(self)))
		shader_collection = kwargs.get("shader_collection", MyShaders())
		
		# TODO: Verify other shaders.
		self._solid_shader = kwargs.get("solid_shader", shader_collection.uniformMaterialPhongShader())
		self._solid_flat_shader = kwargs.get("solid_flat_shader", shader_collection.uniformMaterialPhongShader())
		self._nolight_solid_shader =  kwargs.get("nolight_solid_shader", shader_collection.uniformMaterialShader())
		self._wireframe_shader = kwargs.get("wireframe_shader", shader_collection.uniformMaterialPhongShader())
		self._nolight_wireframe_shader = kwargs.get("nolight_wireframe_shader", shader_collection.uniformMaterialShader())
		self._active_shader = self._solid_shader
		self._active_material = self._material

		self._textures = kwargs.get("textures", None)

		self._vao = QOpenGLVertexArrayObject()
		self._vbo = QOpenGLBuffer(QOpenGLBuffer.VertexBuffer)
		self._ibo = QOpenGLBuffer(QOpenGLBuffer.IndexBuffer)
		self._num_vertices = 0
		self._num_indices = 0

		self._hasNormals = False
		self._hasColors = False
		self._hasTextureCoords = False
		self._hasIndices = False
		self._hasNormalMapping = False    # Attribute included in EP 2

		self._visible = True
		self._enabled = False
		self._pickable = True
		self._selectable = False
		self._selected = False
		self._highlighted = False
		# self._errorMaterial = ERROR MATERIAL 
		self._errorHighlight = False
		# self._warningMaterial = ERROR MATERIAL
		self._warningHightlight = False

		self._pickFactor = 1.0		

	def update(self, **kwargs):
		""" Update this node """
		self._transform = kwargs.get("transform", QMatrix4x4())
		self._render_mode = kwargs.get("mode", MyActor.MyRenderMode.Triangles)
		self._render_type = kwargs.get("type", MyActor.MyRenderType.Solid)
		self._material = kwargs.get("material", Material())
		self._wireframe = kwargs.get("wireframe", Material())

	def scene(self):
		return self._scene

	@property
	def name(self):
		"""Returns the name of this actor"""
		return self._name

	def setName(self, name):
		"""Sets this actor's name"""
		self._name = name

	@property
	def material(self):
		"""Returns the material of this node."""
		return self._material

	#EP code
	@property
	def textures(self):
		return self._textures

	def setTextures(self, textures):
		self._textures = textures

	def setMaterial(self, value):
		self._material = value

	def setTransform(self, xform):
		self._transform = xform

	def transform(self):
		return self._transform

	def position(self):
		xform = self.transform()
		return QVector3D(xform[0,3], xform[1,3], xform[2,3])

	def setPosition(self, pos):
		self._transform = QMatrix4x4()
		self._transform.translate(pos.x(), pos.y(), pos.z())

	def texture(self):
		"""Returns the texture image"""
		return self._texture

	def setTexture(self, texture):
		"""Sets the current texture"""
		self._texture = texture

	def isPickable(self):
		"""Sets whether or not this actor is pickable"""
		return self._pickable

	def setPickable(self, value):
		"""Sets whether this actor is pickable"""
		self._pickable = value

	def isVisible(self):
		"""Sets the visibility of this actor"""
		return self._visible

	def setVisible(self, value):
		"""Sets the visibility of this actor"""
		self._visible = value

	def isEnabled(self):
		"""Returns whether this actor is enabled or not"""
		return self._enabled

	def setEnabled(self, value):
		"""Sets whether this actor is enabled or not"""
		self._enabled = value

	def setSelectable(self, value):
		"""Sets whether or not this actor is selectable"""
		self._selectable = value

	def isSelectable(self):
		"""Returns true if actor is selectable"""
		return self._selectable

	def setSelected(self, value):
		"""Sets selection to value"""
		self._selected = value

	def isSelected(self):
		"""Returns true if it is selected"""
		return self._selected

	def setHighlighted(self, value):
		"""Sets the hightlight value"""
		self._highlighted = value

	def isHighlighted(self):
		"""Returns true if it is highlighted"""
		return self._highlighted

	def setErrorMaterial(self, material):
		"""Sets the error material"""
		self._errorMaterial = material

	def setErrorHighlight(self, value):
		"""Sets the error highlight"""
		self._errorHighlight = value

	def setWarningMaterial(self, material):
		self._warningMaterial = material

	def setWarningHighlight(self, value):
		self._warningHightlight = value

	@property
	def shaderCollection(self):
		return self._shader_collection

	@property
	def renderType(self):
		return self._render_type

	@property
	def solidShader(self):
		return self._solid_shader

	def setSolidShader(self, shader):
		self._solid_shader = shader

	@property
	def solidFlatShader(self):
		return self._solid_flat_shader

	def setSolidFlatShader(self, shader):
		self._solid_flat_shader = shader

	@property
	def noLightSolidShader(self):
		return self._nolight_solid_shader

	def setNoLightSolidShader(self, shader):
		self._nolight_solid_shader = shader


	@property
	def wireframeShader(self):
		return self._wireframe_shader

	def setWireframeShader(self, shader):
		self._wireframe_shader = shader

	@property
	def noLightWireframeShader(self):
		return self._nolight_wireframe_shader

	def setNoLightWireframeShader(self, shader):
		self._nolight_wireframe_shader = shader

	@property
	def numberOfVertices(self):
		self._num_vertices

	@property
	def numberOfIndices(self):
		return self._num_indices

	def mapBuffer(self, offset, count, access):
		vbo_ptr = self._vbo.mapRange(offset, count, access)
		vp_array = ctypes.cast(ctypes.c_void_p(int(vbo_ptr)), ctypes.POINTER(ctypes.c_byte * self._vbo.size())).contents
		return np.frombuffer(vp_array, "B")

	def unmapBuffer(self):
		self._vbo.unmap()

	def updateBuffer(self, vertices=None, normals=None, colors=None, texcoords=None):
		self._vbo.bind()
		if vertices is not None:
			vertices = vertices.tostring()
			self._vbo.write(0, vertices, len(vertices))

		if normals is not None:
			normals = normals.tostring()
			self._vbo.write(self._offsetNormals, normals, len(normals))
		if colors is not None:
			colors = colors.tostring()
			self._vbo.write(self._offsetColors, colors, len(colors))
		if texcoords is not None:
			texcoords = texcoords.tostring()
			self._vbo.write(self._offsetTexCoords, texcoords, len(texcoords))
		self._vbo.release()


	def create(self, vertices, normals=None, colors=None, texcoords=None, indices=None, 
		tangentBasisCalculator=None, usage=QOpenGLBuffer.StaticDraw):
		shaders = [self._solid_shader, self._wireframe_shader, self._nolight_wireframe_shader, self._nolight_wireframe_shader]
		uvs = texcoords
		n = normals
		i = indices
		v = vertices

		self._vao.create()
		self._vao.bind()

		vertices = vertices.tostring()
		total_vertices = len(vertices)
		total_normals = 0
		total_colors = 0
		total_texcoords = 0
		total_tangents = 0
		total_bitangents = 0
		self._num_vertices = total_vertices // (np.dtype(np.float32).itemsize * 3)

		if normals is not None:
			self._hasNormals = True
			normals = normals.tostring()
			total_normals = len(normals)

		if colors is not None:
			self._hasColors = True
			colors = colors.tostring()
			total_colors = len(colors)

		if texcoords is not None:
			self._hasTextureCoords = True
			texcoords = texcoords.tostring()
			total_texcoords = len(texcoords)

		if indices is not None:
			self._hasIndices = True
			indices = indices.tostring()
			total_indices = len(indices)
			self._num_indices = total_indices // np.dtype(np.uint32).itemsize

		if tangentBasisCalculator is not None:
			self._hasNormalMapping = True
			tangents, bitangents = tangentBasisCalculator.calculate(v, uvs, n, i)
			tangents = tangents.tostring()
			bitangents = bitangents.tostring()
			total_tangents = len(tangents)
			total_bitangents = len(bitangents)

		self._vbo.setUsagePattern(usage)
		self._vbo.create()
		self._vbo.bind()

		offset = 0
		self._vbo.allocate(total_vertices + total_normals + total_colors + total_texcoords + total_tangents 
			+ total_bitangents)
		self._vbo.write(offset, vertices, total_vertices)

		for each in shaders:
			each.setAttributeBuffer("position", GL.GL_FLOAT, offset, 3, 3 * np.dtype(np.float32).itemsize)
		offset += total_vertices
		self._offsetNormals = offset

		if self._hasNormals:
			self._vbo.write(offset, normals, total_normals)
			for each in shaders:
				each.setAttributeBuffer("normal", GL.GL_FLOAT, offset, 3, 3 * np.dtype(np.float32).itemsize)
			offset += total_normals
		if self._hasColors:
			self._offsetColors = offset
			self._vbo.write(offset, colors, total_colors)
			for each in shaders:
				each.setAttributeBuffer("color", GL.GL_FLOAT, offset, 3, 3 * np.dtype(np.float32).itemsize)
			offset += total_colors
		if self._hasTextureCoords:
			self._offsetTexCoords = offset
			self._vbo.write(offset, texcoords, total_texcoords)
			for each in shaders:
				each.setAttributeBuffer("texcoord", GL.GL_FLOAT, offset, 2, 2 * np.dtype(np.float32).itemsize)
			offset += total_texcoords
		if self._hasNormalMapping:
			# TANGENTS
			self._offsetTangents = offset
			self._vbo.write(offset, tangents, total_tangents)
			for each in shaders:
				each.setAttributeBuffer("tangent", GL.GL_FLOAT, offset, 3, 3 * np.dtype(np.float32).itemsize)
			offset += total_tangents 
			
			# BITANGENTS
			self._offsetBitangents = offset
			self._vbo.write(offset, bitangents, total_bitangents)
			for each in shaders:
				each.setAttributeBuffer("bitangent", GL.GL_FLOAT, offset, 3, 3 * np.dtype(np.float32).itemsize)
			offset += total_bitangents

		self._vbo.release(QOpenGLBuffer.VertexBuffer)

		for each in shaders:
			each.enableAttributeArray("position")

		if self._hasNormals:
			for each in shaders:
				each.enableAttributeArray("normal")
		if self._hasColors:
			for each in shaders:
				each.enableAttributeArray("color")
		if self._hasTextureCoords:
			for each in shaders:
				each.enableAttributeArray("texcoord")

		if self._hasNormalMapping:
			for each in shaders:
				each.enableAttributeArray("tangent")
				each.enableAttributeArray("bitangent")

		if self._hasIndices:
			self._ibo.setUsagePattern(usage)
			self._ibo.create()
			self._ibo.bind()

			self._ibo.allocate(total_indices)
			self._ibo.write(0, indices, total_indices)

		self._vao.release()

		if self._hasIndices:
			self._ibo.release(QOpenGLBuffer.IndexBuffer)

	def setUniformBindings(self):
		self._active_shader.setUpSceneIllumination(self._scene)
		self._active_shader.setUpSceneCamera(self._scene)
		self._active_shader.setUpActorTransform(self._transform)

		if self.isSelectable() and self.isSelected():
			self._active_shader.setUniformValue("selected", 1.0)
		else:
			self._active_shader.setUniformValue("selected", 0.65)

		if self._errorHighlight:
			self._active_shader.setUpMaterial(None, self._errorMaterial)
		if self._warningHightlight:
			self._active_shader.setUpMaterial(None, self._warningMaterial)
		else:
			self._active_shader.setUpMaterial(self._textures, self._active_material)

		if self._textures is not None:
			self._active_shader.setUpNormalMapping(self._textures)

		if self._active_shader._geometryShader is not None:
			self._active_shader.updateGeometryShaderUniforms()

	def beginRendering(self, draw_style, lighting, shading, passNumber):
		if lighting:
			if draw_style == GL.GL_LINE:
				self._active_shader = self._wireframe_shader
				self._active_material = self._material if passNumber == 0 else self._wireframe
			else:
				if shading == GL.GL_SMOOTH:
					self._active_shader = self._solid_shader
				else:
					self._active_shader = self._solid_flat_shader
				self._active_material = self._material
		else:
			if draw_style == GL.GL_LINE:
				self._active_shader = self._nolight_wireframe_shader
				self._active_material = self._material if passNumber == 0 else self._wireframe
			else:
				self._active_shader = self._nolight_solid_shader
				self._active_material = self._material

		GL.glPolygonMode(GL.GL_FRONT_AND_BACK, draw_style)

		if self._render_type == self.MyRenderType.Solid:
			GL.glEnable(GL.GL_DEPTH_TEST)
			GL.glDepthMask(GL.GL_TRUE)
		elif self._render_type == self.MyRenderType.Transparent:
			GL.glEnable(GL.GL_DEPTH_TEST)
			GL.glDepthMask(GL.GL_FALSE)
		elif self._render_type == self.MyRenderType.Overlay:
			GL.glDisable(GL.GL_DEPTH_TEST)

		self._active_shader.bind()


		if self._textures is not None:
			self._textures.activeTextures()

		self.setUniformBindings()

		self._vao.bind()


	def render(self):
		raise NotImplementedError("render() must be implemented in child class")

	def endRendering(self):
		self._vao.release()

		if self._textures is not None:
			self._textures.releaseTextures()

		self._active_shader.release()

	def pickFactor(self):
		return self._pickFactor

	def setPickFactor(self, value):
		self._pickFactor = value

	def intersect(self, ray):
		tMin = -math.inf
		tMax = math.inf
		obb_xform = self.transform()
		obb_center = QVector3D(obb_xform[0,3], obb_xform[1,3], obb_xform[2,3])
		point - obb_center - ray.origin()
		for i in range(3):
			axis = QVector3D(obb_xform[0,i], obb_xform[1,i], obb_xform[2,i]).normalized()
			half_length = QVector3D(obb_xform[i,0], obb_xform[i,1], obb_xform[i,2]).length() / 2.0
			e = QVector3D.dotProduct(axis, point)
			f = QVector3D.dotProduct(axis, ray.direction())
			if abs(f) > 10E-6:
				t1 = (e + half_length*self._pickFactor) / f
				t2 = (e - half_length*self._pickFactor) / f
				if t1 > t2:
					t1, t2 = t2, t1
				if t1 > tMin:
					tMin = t1
				if t2 < tMax:
					tMax = t2
				if tMin > tMax:
					return (False, math.inf)
				if tMax < 0:
					return (False, math.inf)
			elif -e-half_length > 0.0 or -e+half_length < 0.0:
				return (False, math.inf)
		if tMin > 0:
			return (True, tMin)

		return (True, tMax)

	def destroy(self):
		self._vao.destroy()
		self._vbo.destroy()
		self._ibo.destroy()


	# EP 2 - Code
	# This method generates texture coordinates from the vertices array, so it 
	# must be called after the vertices are computed. Then, the method will 
	# compute the texture coordinates (u,v) for each vertex using the method (spherical projection)
	# presented at section 11.2 from the textbook (SM 11).
	def generateUnitSphericalTexCoordFromVertices(self, vertices):		
		(x, y, z) = (vertices[:, 0], vertices[:, 1], vertices[:, 2])		
		
		# I remove the division by |x| because, it was resulted in unexaplained texturing mapping.
		# reviewing previous book section I guess the |x| was mistaken and the right denominator is
		# ||vertice||
		theta = np.arccos(y / np.linalg.norm(vertices, axis=1))
		phi = np.arctan2(z, x)

		u = (np.pi + phi) / (2.0 * np.pi)
		v = (np.pi - theta) / np.pi

		return np.dstack((u, v))[0]


	# EP 2 - Code
	# return Actor type.
	def actorType(self):
		raise NotImplementedError("render() must be implemented in child class")
