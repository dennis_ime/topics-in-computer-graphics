import numpy as np

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from OpenGL import GL
from MySource.Graphics.MyScene import MyScene
from Source.Graphics.Camera import Camera
from MySource.Graphics.MyBackground import MyBackground
from MySource.Graphics.MyFloor import MyFloor
from MySource.Graphics.MyGrid import MyGrid
from MySource.Graphics.MyAxis import MyAxis

from MySource.Graphics.MyLight import MyPointLight, MyDirectionalLight, MyLightColor
from Source.Graphics.Material import Material
#from MySource.Graphics.EpicMaterial import EpicMaterial

class MyWorld(MyScene):

	def __init__(self, viewer, **kwargs):
		self._home_position = kwargs.get("home_position", QVector3D(0.0, 0.0, 1.0))

		self._camera = Camera(name="main", position=self._home_position, lens=Camera.Lens.Perspective)
		self._camera.pointAt(QVector3D(0.0, 0.0, 0.0))

		# code developed for EP 1
		lightColor = MyLightColor(
			ambient=QVector3D(0.5, 0.5, 0.5),
			diffuse=QVector3D(1.0, 1.0, 1.0),
			specular=QVector3D(1.0, 1.0, 1.0))

		#light = MyPointLight(color=lightColor,
		#	position=QVector3D(2.0, 2.0, 0.5),
		#	headlight=True)

		light = MyDirectionalLight(color=lightColor,
			direction=QVector3D(1.0, 1.0, 0.5),
			headlight=True)

		gnomonCamera = Camera(name="main", position=QVector3D(0.0, 0.0, 4.0), lens=Camera.Lens.Perspective)
		gnomonCamera.pointAt(QVector3D(0.0, 0.0, 0.0))

		self._background = {
			'top_left': QColor(107, 128, 140),
			'top_center': QColor(107, 128, 140),
			'top_right': QColor(107, 128, 140),
			'mid_left': QColor(149, 164, 164),
			'mid_center': QColor(149, 164, 164),
			'mid_right': QColor(149, 164, 164),
			'bot_left': QColor(191, 199, 199),
			'bot_center': QColor(191, 199, 199),
			'bot_right': QColor(191, 199, 199)
		}

		self._gridProperties = {
			'color': QColor(128, 128, 128), 
			'length_rows': 10.0, 
			'length_cols': 10.0,
			'rows': 24,
			'cols': 24
		}

		self._gridActor = None
		self._axisActor = None
		self._backgroundActor = None

		super(MyWorld, self).__init__(viewer, light=light, camera=gnomonCamera, **kwargs)

	def initialize(self):
		if self._backgroundActor is not None:
			self.removeSystemActor(self._backgroundActor)

		self._backgroundActor = MyBackground(self, name="background", pallete=self._background)
		self.addSystemActor(self._backgroundActor)
		self.createGridLines()

		for each in self.actors():
			each.initialize()

	def background(self):
		return self._background

	def setBackground(self, background):
		self._background = background

	def updateBackground(self):
		if self._backgroundActor is not None:
			self._backgroundActor.setPallete(self._background)

	def gridProperties(self):
		return self._gridProperties

	def setGridProperties(self, properties):
		self._gridProperties = properties

	def createGridLines(self, properties=None):
		gridVisible = axisVisible = True
		if self._gridActor is not None:
			gridVisible = self._gridActor.isVisible()
			self.removeSystemActor(self._gridActor)
		if self._axisActor is not None:
			axisVisible = self._axisActor.isVisible()
			self.removeSystemActor(self._axisActor)

		self._gridActor = MyGrid(self, name="floor", 
			length_rows = self._gridProperties["length_rows"],
			length_cols = self._gridProperties["length_cols"],
			rows = self._gridProperties["rows"],
			cols = self._gridProperties["cols"],
			material = Material(cbase=QVector3D(
				self._gridProperties['color'].redF(), 
				self._gridProperties['color'].greenF(),
				self._gridProperties['color'].blueF())))
		self._gridActor.setVisible(gridVisible)
		self.addSystemActor(self._gridActor)

		self._axisActor = MyAxis(self, name="axis", length_row=self._gridProperties['length_rows'],
			length_col=self._gridProperties['length_cols'])
		self._axisActor.setVisible(axisVisible)
		self.addSystemActor(self._axisActor)

	def enableGridLines(self, state):
		if self._gridActor is not None:
			self._gridActor.setVisible(state)

	def enableAxes(self, state):
		if self._axisActor is not None:
			self._axisActor.setVisible(state)

	def setCameraLens(self, lens):
		self.camera.setLens(lens, adjust=True)

	def storeCamera(self):
		self.camera.store()

	def recallCamera(self):
		self.camera.recall(self.viewer.width() / float(self.viewer.height() if self.viewer.height() > 0.0 else 1.0))

	def resetCamera(self):
		self.camera.setPosition(self._home_position)
		self.camera.pointAt(QVector3D(0.0, 0.0, 0.0))
		self.camera.setAspectRatio(self.viewer.width() / float(self.viewer.height() if self.viewer.height() > 0.0 else 1.0))
		self.camera.setLens(Camera.Lens.Perspective)
