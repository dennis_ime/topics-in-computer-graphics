from MySource.Graphics.Shader.MyShader import MyShader
from MySource.Graphics.MyLight import MyLightType

from MySource.Graphics.Shader.BlinnPhong.Geometry.BPShaderDefault import BPShaderDefault
from MySource.Graphics.Shader.BlinnPhong.Geometry.BPShaderSpiky import BPShaderSpiky
from PyQt5.QtGui import QVector3D

from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect

class BPShader(MyShader):
	"""Bling phong shader program base class"""
	def __init__(self, geometryEffect=GeometryShaderEffect.NONE):
		super(BPShader, self).__init__(geometryEffect)
		
	def setUpMaterial(self, textures, material):
		if material is not None:
			self.setUniformValue("material.emission", material.emissionColor)
			self.setUniformValue("material.ambient", material.ambientColor)
			self.setUniformValue("material.diffuse", material.diffuseColor)
			self.setUniformValue("material.specular", material.specularColor)
			self.setUniformValue("material.shininess", material.shininess)

	def setUpSceneIllumination(self, scene):
		light = scene.light
		viewMatrix = scene.camera.viewMatrix

		if light.type() == MyLightType.POINT:
			self.setUniformValue("lightType", 0)
			if light.headlight:
				self.setUniformValue("pointLight.position", QVector3D(0.0, 0.0, 0.0))
			else:
				self.setUniformValue("pointLight.position", light.position * viewMatrix)
			
			self.setUniformValue("pointLight.ambient", light.color.ambientColor)
			self.setUniformValue("pointLight.diffuse", light.color.diffuseColor)
			self.setUniformValue("pointLight.specular", light.color.specularColor)
			self.setUniformValue("pointLight.radius", light.radius)
			self.setUniformValue("pointLight.attenuationConstant", light.attenuationConstant)
			self.setUniformValue("pointLight.attenuationLinear", light.attenuationLinear)
			self.setUniformValue("pointLight.attenuationQuad", light.attenuationQuad)

		elif light.type() == MyLightType.DIRECTIONAL:
			self.setUniformValue("lightType", 1)
			if light.headlight:
				self.setUniformValue("directionalLight.direction", QVector3D(0.0, 0.0, 1.0))
			else:
				self.setUniformValue("directionalLight.direction", viewMatrix * light.direction)
			
			self.setUniformValue("directionalLight.ambient", light.color.ambientColor)
			self.setUniformValue("directionalLight.diffuse", light.color.diffuseColor)
			self.setUniformValue("directionalLight.specular", light.color.specularColor)

		elif light.type() == MyLightType.HEMISPHERICAL:
			self.setUniformValue("lightType", 2)
			self.setUniformValue("hemisphericalLight.skyDiffuse", light.skyColor.diffuseColor)
			self.setUniformValue("hemisphericalLight.skySpecular", light.skyColor.ambientColor)
			self.setUniformValue("hemisphericalLight.skyAmbient", light.skyColor.ambientColor)

			self.setUniformValue("hemisphericalLight.groundDiffuse", light.groundColor.diffuseColor)
			self.setUniformValue("hemisphericalLight.groundSpecular", light.groundColor.specularColor)
			self.setUniformValue("hemisphericalLight.groundAmbient", light.groundColor.ambientColor)

	def setUpActorTransform(self, transform):
		self.setUniformValue("modelMatrix", transform)
		self.setUniformValue("normalMatrix", transform.normalMatrix())

	def setUpSceneCamera(self, scene):
		self.setUniformValue("viewMatrix", scene.camera.viewMatrix)
		self.setUniformValue("projectionMatrix", scene.camera.projectionMatrix)

	def createGeometryShader(self, geometryEffect):
		if geometryEffect == GeometryShaderEffect.NONE or geometryEffect == GeometryShaderEffect.DEFAULT:
			return BPShaderDefault()
		elif geometryEffect == GeometryShaderEffect.SPIKY:
			return BPShaderSpiky()

	# =============== Abstract methods ========================================
	def vertexSource(self):
		raise NotImplementedError("vertexSource() must be implemented in child class")		

	def fragmentSource(self):
		raise NotImplementedError("fragmentSource() must be implemented in child class")