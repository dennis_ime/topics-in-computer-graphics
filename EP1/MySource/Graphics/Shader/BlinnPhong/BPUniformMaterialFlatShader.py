from MySource.Graphics.Shader.BlinnPhong.BPShader import BPShader

from MySource.Graphics.Shader.PreProcessor import PreProcessor
from MySource.Graphics.Shader.PreProcessor import PreProcessorExtensionIncludeSourceFile
from MySource.Graphics.Shader.PreProcessor import PreProcessorExtensionTypeModifier
from MySource.Graphics.Shader.PreProcessor import TypeModifier

from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect

class BPUniformMaterialFlatShader(BPShader):
	"""Uniform Material Vertex Flat Shader"""
	def __init__(self, geometryEffect=GeometryShaderEffect.NONE):
		super(BPUniformMaterialFlatShader, self).__init__(geometryEffect)


	def setUpPreProcessor(self):
		self._preProcessor = PreProcessor([
			PreProcessorExtensionIncludeSourceFile(),
			PreProcessorExtensionTypeModifier(TypeModifier.FLAT)
		])
	
	def vertexSource(self):
		return self.readFile("MySource/ShaderProgram/BlinnPhong/vertex-shader/uniform-material.glsl")

	def fragmentSource(self):
		return self.readFile("MySource/ShaderProgram/BlinnPhong/fragment-shader/uniform-material.glsl")