from MySource.Graphics.Shader.GeometryShader import GeometryShader

class BPShaderDefault(GeometryShader):
	def __init__(self, **kwargs):
		super(BPShaderDefault, self).__init__()
		self._width = kwargs.get("width", 0.4)
		self._height = kwargs.get("height", 0.4)

	@property
	def width(self):
		return self._width

	@width.setter
	def width(self, value):
		self._width = value

	@property
	def height(self):
		return self._width

	@height.setter
	def height(self, value):
		self._height = value

	def updateUniforms(self, program):
		# DefaultGeometryShader does not have uniform variables
		pass

	def src(self):
		return self.readFile("MySource/ShaderProgram/BlinnPhong/geometry-shader/default.glsl")
