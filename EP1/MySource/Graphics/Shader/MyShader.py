from PyQt5.QtCore import QObject
from PyQt5.QtGui import QOpenGLShader, QOpenGLShaderProgram, QVector3D

from MySource.Graphics.Shader.PreProcessor import PreProcessor
from MySource.Graphics.Shader.PreProcessor import PreProcessorExtensionIncludeSourceFile
from MySource.Graphics.Shader.PreProcessor import PreProcessorExtensionTypeModifier

from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect

## Abstract base class to provide interface of shader programs
## and handle specific shader program uniform input.
class MyShader(QObject):
	def __init__(self, geometryEffect=GeometryShaderEffect.NONE):
		self._preProcessor = PreProcessor([])
		self.setUpPreProcessor()

		if geometryEffect == GeometryShaderEffect.NONE:
			self._geometryShader = None
		else:
			self._geometryShader = self.createGeometryShader(geometryEffect)

		self.updateProgramCode()

	def updateProgramCode(self):
		self._program = QOpenGLShaderProgram()
		self._program.addShaderFromSourceCode(QOpenGLShader.Vertex, self._preProcessor.process(self.vertexSource()))	
		self._program.addShaderFromSourceCode(QOpenGLShader.Fragment, self._preProcessor.process(self.fragmentSource()))
		
		if self._geometryShader is not None:
			self._program.addShaderFromSourceCode(QOpenGLShader.Geometry, self._preProcessor.process(self._geometryShader.src()))
		
		self._program.link()

	@property
	def geometryShader(self):
		return self._geometryShader

	@geometryShader.setter
	def geoemtryShader(self, value):
		self._geometryShader = value
		self.updateProgramCode()

	def updateGeometryShaderUniforms(self):		
			self._geometryShader.updateUniforms(self._program)

	@property 
	def program(self):
		return self._program

	# TODO: Continues implementation,
	def setAttributeBuffer(self, name, type, offset, tupleSize, stride):
		self._program.setAttributeBuffer(name, type, offset, tupleSize, stride)

	def enableAttributeArray(self, name):
		self._program.enableAttributeArray(name)

	def disableAttributeArray(self, name):
		self._program.disableAttributeArray(name)

	def release(self):
		self._program.release()

	def setUniformValue(self, name, value):
		self._program.setUniformValue(name, value)

	def bind(self):
		self._program.bind()

	def readFile(self, filename):
		with open(filename) as f:
			return f.read()

	# =============== Abstract methods ========================================================
	def setUpTexture(self, textures):
		raise NotImplementedError("setUpTexture() must be implemented in child class")

	def setUpMaterial(self, material):
		raise NotImplementedError("setUpMaterial() must be implemented in child class")

	def setUpSceneIllumination(self, scene):
		raise NotImplementedError("setUpSceneIllumination() must be implemented in child class")

	def setUpActorTransform(self, transform):
		raise NotImplementedError("setUpActorTransform() must be implemented in child class")

	def setUpSceneCamera(self, scene):
		raise NotImplementedError("setUpSceneCamera() must be implemented in child class")

	def vertexSource(self):
		raise NotImplementedError("vertexSource() must be implemented in child class")		

	def fragmentSource(self):
		raise NotImplementedError("fragmentSource() must be implemented in child class")

	def setUpPreProcessor(self):
		#raise NotImplementedError("setUpPreProcessor() must be implemented in child class")
		pass

	def createGeometryShader(self, geometryEffect):
		raise NotImplementedError("createGeometryShader() must be implemented in child class")