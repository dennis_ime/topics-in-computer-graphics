from PyQt5.QtCore import QObject

class GeometryShader(QObject):
	def __init__(self):
		super(GeometryShader, self).__init__()
		
	def readFile(self, filename):
		with open(filename) as f:
			return f.read()

	def updateUniforms(self, program):
		raise NotImplementedError("GeometryShader::updateUniforms() must be implemented in a child class.")

	def src(self):
		raise NotImplementedError("GeometryShader::src() must be implemented in a child class.")