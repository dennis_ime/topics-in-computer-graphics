from MySource.Graphics.Shader.Epic.EpicShader import EpicShader

from MySource.Graphics.Shader.PreProcessor import PreProcessor
from MySource.Graphics.Shader.PreProcessor import PreProcessorExtensionIncludeSourceFile
from MySource.Graphics.Shader.PreProcessor import PreProcessorExtensionTypeModifier
from MySource.Graphics.Shader.PreProcessor import TypeModifier
from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect

class EpicUniformMaterialFlatShader(EpicShader):
	"""docstring for EpicUniformMaterialFlatShader"""
	def __init__(self, geometryEffect=GeometryShaderEffect.NONE):
		super(EpicUniformMaterialFlatShader, self).__init__(geometryEffect)

	def setUpPreProcessor(self):
		self._preProcessor = PreProcessor([
			PreProcessorExtensionIncludeSourceFile(),
			PreProcessorExtensionTypeModifier(TypeModifier.FLAT)
		])

	def vertexSource(self):
		return self.readFile("MySource/ShaderProgram/Epic/vertex-shader/uniform-material.glsl")

	def fragmentSource(self):
		return self.readFile("MySource/ShaderProgram/Epic/fragment-shader/uniform-material.glsl")
