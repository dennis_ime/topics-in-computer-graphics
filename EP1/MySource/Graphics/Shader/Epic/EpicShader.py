from MySource.Graphics.Shader.MyShader import MyShader
from MySource.Graphics.MyLight import MyLight
from MySource.Graphics.MyLight import MyLightType

from MySource.Graphics.Shader.Epic.Geometry.EpicShaderDefault import EpicShaderDefault
from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect

from PyQt5.QtGui import QVector3D

class EpicShader(MyShader):
	"""docstring for EpicShader"""
	def __init__(self, geometryEffect=GeometryShaderEffect.NONE):
		super(EpicShader, self).__init__(geometryEffect)

	def setUpCBaseUniform(self, textures, material):			
		if textures is not None and textures.hasCBaseTexture:
			self.setUniformValue("cbaseTex", textures.cbaseTextureActiveId())
			self.setUniformValue("material.cbase", QVector3D(0.0, 0.0, 0.0))
		else:
			self.setUniformValue("material.cbase", material.cbase)

	def setUpMetallicUniform(self, textures, material):
		if textures is not None and textures.hasMetallicTexture:
			self.setUniformValue("metallicTex", textures.metallicTextureActiveId())
			self.setUniformValue("material.metallic", 0.0)
		else:
			self.setUniformValue("material.metallic", material.metallic)

	def setUpSpecularUniform(self, textures, material):
		if textures is not None and textures.hasSpecularTexture:
			self.setUniformValue("specularTex", textures.specularTextureActiveId())
			self.setUniformValue("material.specular_coefficient", 0.0)
		else:
			self.setUniformValue("material.specular_coefficient", material.specular_coefficient)

	def setUpRoughness(self, textures, material):
		if textures is not None and textures.hasRoughnessTexture:
			self.setUniformValue("roughnessTex", textures.roughnessTextureActiveId())
			self.setUniformValue("material.roughness", 0.0)
		else:
			self.setUniformValue("material.roughness", material.roughness)
	
	def setUpNormalMapping(self, textures):
		if textures is not None and textures.hasNormalMappingTexture:
			self.setUniformValue("normalTex", textures.normalMappingTextureActiveId())

	def setUpMaterial(self, textures, material):		
		self.setUpCBaseUniform(textures, material)
		self.setUpMetallicUniform(textures, material)
		self.setUpSpecularUniform(textures, material)
		self.setUpRoughness(textures, material)

	def setUpSceneIllumination(self, scene):
		light = scene.light
		viewMatrix = scene.camera.viewMatrix

		if light.type() == MyLightType.POINT:
			self.setUniformValue("lightType", 0)
			if light.headlight:
				self.setUniformValue("pointLight.position", QVector3D(0.0, 0.0, 0.0))
			else:
				self.setUniformValue("pointLight.position", light.position * viewMatrix)

			self.setUniformValue("pointLight.color", light.color.diffuseColor)
			self.setUniformValue("pointLight.radius", light.radius)
			self.setUniformValue("pointLight.attenuationConstant", light.attenuationConstant)
			self.setUniformValue("pointLight.attenuationLinear", light.attenuationLinear)
			self.setUniformValue("pointLight.attenuationQuad", light.attenuationQuad)

		elif light.type() == MyLightType.DIRECTIONAL:
			self.setUniformValue("lightType", 1)
			if light.headlight:
				self.setUniformValue("directionalLight.direction", QVector3D(0.0, 0.0, 1.0))
			else:
				self.setUniformValue("directionalLight.direction", viewMatrix * light.direction)

			self.setUniformValue("directionalLight.color", light.color.color)

		elif light.type() == MyLightType.HEMISPHERICAL:
			self.setUniformValue("lightType", 2)
			self.setUniformValue("hemisphericalLight.skyColor", light.skyColor.color)
			self.setUniformValue("hemisphericalLight.groundColor", light.groundColor.color)

	def setUpActorTransform(self, transform):
		self.setUniformValue("modelMatrix", transform)
		self.setUniformValue("normalMatrix", transform.normalMatrix())

	def setUpSceneCamera(self, scene):
		self.setUniformValue("viewMatrix", scene.camera.viewMatrix)
		self.setUniformValue("projectionMatrix", scene.camera.projectionMatrix)
		self.setUniformValue("cameraPosition", scene.camera.position)

	def createGeometryShader(self, geometryEffect):
		return EpicShaderDefault()

	# ================= Abstract methods =============================================
	def vertexSource(self):
		raise NotImplementedError("vertexSource() must be implemented in child class")

	def fragmentSource(self):
		raise NotImplementedError("fragmentSource() must be implemented in child class")
