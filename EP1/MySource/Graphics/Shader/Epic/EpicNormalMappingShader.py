from MySource.Graphics.Shader.Epic.EpicShader import EpicShader
from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect

class EpicNormalMappingShader(EpicShader):
	"""docstring for EpicUniformMaterialShader"""
	def __init__(self, geometryEffect=GeometryShaderEffect.NONE):
		super(EpicNormalMappingShader, self).__init__(geometryEffect)

	def vertexSource(self):
		return """
			#version 330
			#define POINT_LIGHT 0
			#define DIRECTIONAL_LIGHT 1
			#define HEMISPHERICAL_LIGHT 2

			layout(location = 0) in vec3 position;
			layout(location = 1) in vec3 normal;			
			layout(location = 2) in vec2 texcoord;
			layout(location = 3) in vec3 tangent;
			layout(location = 4) in vec3 bitangent;

			uniform mat4 modelMatrix;
			uniform mat4 viewMatrix;
			uniform mat4 projectionMatrix;
			uniform mat3 normalMatrix;
			uniform vec3 cameraPosition;

			struct PointLight {
			  vec3 position;
			  vec3 color;
			  float radius;
			  float attenuationConstant;
			  float attenuationLinear;
			  float attenuationQuad;
			};

			struct DirectionalLight {
			  vec3 direction;
			  vec3 color;
			};

			struct HemisphericalLight {
			  vec3 skyColor;
			  vec3 groundColor;
			};

			struct LightData {
			  vec3 direction;
			  vec3 color;
			  float attenuation;
			};

			// lights 
			// Enum in host (EpiclLightType): POINT=0, DIRECTIONAL=1, HEMISPHERICAL=2
			uniform int lightType; 
			uniform PointLight pointLight;
			uniform DirectionalLight directionalLight;
			uniform HemisphericalLight hemisphericalLight;

			smooth out vec4 vertexNormal;
			smooth out vec4 vertexPosition;
			smooth out vec3 cameraDirection;
			smooth out float attenuation;
			smooth out LightData light;
			smooth out vec2 textureCoord;


			// DEBUG - VARIABLES
			smooth out vec3 otangent;
			smooth out vec3 obitangent;


			void computePointLight()
			{
			  light.direction = normalize(pointLight.position.xyz - vertexPosition.xyz).xyz;
			  light.color = pointLight.color;
			  
			  //attenuation
				float t = length(pointLight.position.xyz - vertexPosition.xyz);
				float divisor = pointLight.attenuationConstant;
				divisor += (pointLight.attenuationLinear * t);
				divisor += (pointLight.attenuationQuad * t * t);

				light.attenuation = pow(clamp(1 - pow(t/pointLight.radius,4), 0, 1), 2.0)/divisor;
			}

			void computeDirectionalLight()
			{
			  light.direction = -directionalLight.direction;
			  light.color = directionalLight.color;
			  light.attenuation = 1.0;
			}

			void computeHemisphericalLight()
			{
			  light.direction = vertexNormal.xyz;

			  light.color = mix(hemisphericalLight.groundColor, hemisphericalLight.skyColor, 
			    clamp(dot(vertexNormal.xyz, vec3(0,1,0)) * 0.5 + 0.5, 0, 1)); 
			  light.attenuation = 1.0;
			}

			void main()
			{
			  
				mat4 modelview = viewMatrix * modelMatrix;
			  vertexPosition = modelview * vec4(position, 1.0);
			  vertexNormal = viewMatrix * vec4(normalMatrix * normal, 0.0);
			  cameraDirection = normalize(cameraPosition.xyz - vertexPosition.xyz).xyz;

			  mat3 modelview3x3 = mat3(modelview);

			  vec3 normal_cameraspace = modelview3x3 * normalize(normal);
			  vec3 tangent_cameraspace = modelview3x3 * normalize(tangent);
			  vec3 bitangent_cameraspace = modelview3x3 * normalize(bitangent);

			  mat3 TBN = transpose(mat3(tangent_cameraspace, bitangent_cameraspace, normal_cameraspace));

			  // light data computation
			  switch(lightType) {
			    case 0:
			      computePointLight();
			      break;
			    case 1:
			      computeDirectionalLight();
			      break;
			    case 2:
			      computeHemisphericalLight();
			      break;
			  }

			  light.direction = TBN * light.direction;
			 	cameraDirection = TBN * normalize(cameraPosition.xyz - vertexPosition.xyz);

			  // standard output 
			  textureCoord = texcoord;
			  gl_Position = projectionMatrix * vertexPosition;			  

			  otangent = tangent;
			  obitangent = bitangent;
			}
		"""

	def fragmentSource(self):
		return """
			#version 330
			#define M_PI 3.1415926535897932384626433832795

			struct EpicMaterial {
				float metallic;
				float roughness;
				float specular_coefficient;
				vec3 cbase;
			};

			struct LightData {
			  vec3 direction;
			  vec3 color;
			  float attenuation;
			};

			smooth in vec4 vertexNormal;
			smooth in vec3 cameraDirection;
			smooth in vec2 textureCoord;
			smooth in LightData light;

			// DEBUG VARIABLE
			smooth in vec3 otangent;
			smooth in vec3 obitangent;

			uniform EpicMaterial material;
			uniform sampler2D cbaseTex;
			uniform sampler2D metallicTex;
			uniform sampler2D specularTex;
			uniform sampler2D roughnessTex;	
			uniform sampler2D normalTex;

			out vec4 fragColor;

			float G1(vec3 v) {
				float k = pow(material.roughness + 1, 2) / 8.0;
				float divisor = (dot(vertexNormal.xyz, v)*(1-k)) + k;
				return dot(vertexNormal.xyz, v) / divisor;
			}

			EpicMaterial getMaterialSetup()
			{
				EpicMaterial mymaterial;
				vec3 _cbaseTex = texture2D(cbaseTex, textureCoord.st).rgb;

				/*
					The metallic, specular and roughness texture are considered 
					to be rgb image such that all channels have the same value
					and this function uses the green channel to take these values.
				*/

				float _metallicTex = texture2D(metallicTex, textureCoord.st).g;
				float _specularTex = texture2D(specularTex, textureCoord.st).g;
				float _roughnessTex = texture2D(roughnessTex, textureCoord.st).g;

				mymaterial.cbase = material.cbase + _cbaseTex; 
				mymaterial.metallic = material.metallic + _metallicTex; 
				mymaterial.specular_coefficient = material.specular_coefficient + _specularTex;
				mymaterial.roughness = material.roughness + _roughnessTex;

				return mymaterial;
			}

			void main() {
				vec3 N = normalize(texture2D(normalTex, textureCoord.st).rgb*2.0 - 1.0);
				vec3 V = cameraDirection;
				vec3 L = light.direction;
				vec3 H = normalize(L + V).xyz;
				
				/* get material data considering texture. */
				EpicMaterial mymaterial = getMaterialSetup();				

				// diffuse term
				vec3 cdiff = (1 - mymaterial.metallic) * mymaterial.cbase.rgb;
				vec3 d = cdiff / M_PI;

				//specular term
				vec3 cspec = mix(0.08 * mymaterial.specular_coefficient * vec3(1,1,1), mymaterial.cbase.rgb, mymaterial.metallic);
				float alpha = mymaterial.roughness * mymaterial.roughness;

				float alpha2 = alpha*alpha;

				float D = alpha2 / (M_PI * pow((pow(dot(N, H), 2.0)) * (alpha2 - 1.0) + 1.0, 2.0));
				vec3 F = cspec + (1 - cspec)*pow(2.0, -5.55473*(dot(V,H)) - 6.98316*(dot(V,H)));
				float G = G1(L)*G1(V);

				vec3 s = (D*F*G) / (4.0 * (dot(N,L))*(dot(N, V)));

				vec3 fcolor = light.color * dot(N, L) * (d + s); 
				fcolor = fcolor * light.attenuation + vec3(0.3, 0.3, 0.3);
				
				fragColor = vec4(fcolor, 1.0);				
			}
		"""	