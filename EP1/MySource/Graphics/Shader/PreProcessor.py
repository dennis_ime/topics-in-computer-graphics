from PyQt5.QtCore import QObject
import re

# =============== Pre Processor Extension ================================================= #
class PreProcessorExtension(QObject):
	def __init__(self):
		super(PreProcessorExtension, self).__init__()
		
	def process(self, src):
		raise NotImplementedError("PreProcessorExtension::process() must be implemented in child class")


# ================ Pre Processor Extension Include File ============================================ #
class PreProcessorExtensionIncludeSourceFile(PreProcessorExtension):	
	def __init__(self):
		super(PreProcessorExtensionIncludeSourceFile, self).__init__()
		
	def _extractTags(self, src):
		reInclude = re.compile(r'#include\s+\"(?P<filename>\S+)"')
		return reInclude.search(src)

	def _subIncludeTags(self, src):
		match = self._extractTags(src)

		if match:
			content = self._includeContent(match.group("filename"))
			content = self.process(content) # include inside include 
			src = src[:match.start()] + "\n" + content.strip() + "\n" + src[match.end():]
			return self.process(src)

		return src

	def _includeContent(self, filename):
		with open(filename) as f:
			return f.read()

	def process(self, src):
		return self._subIncludeTags(src)

# ===================== Pre Processor Extension Type Modifier ==================================== #
class TypeModifier:
	FLAT = 0
	SMOOTH = 1
	Types = [FLAT, SMOOTH]

class PreProcessorExtensionTypeModifier(PreProcessorExtension):
	def __init__(self, modifierType=TypeModifier.FLAT):
		super(PreProcessorExtensionTypeModifier).__init__()
		self._modifierType = modifierType

	@property
	def modifierType(self):
		return self._modifierType

	@modifierType.setter
	def modifierType(self, value):
		self._modifierType = value		

	def process(self, src):
		reModifier = re.compile(r'(?P<tag>#modifier)\s+(?P<kind>in\s+|out\s+|)(?P<type>\S+)\s+(?P<var_name>\S+);')
		match = reModifier.search(src)

		if match:
			src = src[:match.start()] + self.typeToModifier() + src[match.start()+len("#modifier"):]
			return self.process(src)

		return src

	def typeToModifier(self):
		 if self._modifierType == TypeModifier.FLAT: 
		 	return "flat" 
		 else: 
		 	return "smooth"


# ======================== Pre Processor ======================================================== #
class PreProcessor(QObject):
	def __init__(self, extensions):
		super(PreProcessor, self).__init__()
		self._extensions = extensions

	def process(self, src):
		for ext in self._extensions:
			src = ext.process(src)

		return src