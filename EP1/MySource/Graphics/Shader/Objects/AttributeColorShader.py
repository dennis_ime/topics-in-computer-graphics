from MySource.Graphics.Shader.MyShader import MyShader

from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect

class AttributeColorShader(MyShader):
	"""docstring for WireframeMaterialShader"""
	def __init__(self, geometryShader=GeometryShaderEffect.NONE):
		super(AttributeColorShader, self).__init__(geometryShader)

	def setUpMaterial(self, textures, material):
		pass

	def setUpSceneIllumination(self, scene):
		pass

	def setUpActorTransform(self, transform):
		self.setUniformValue("modelMatrix", transform)
	
	def setUpSceneCamera(self, scene):
		self.setUniformValue("viewMatrix", scene.camera.viewMatrix)
		self.setUniformValue("projectionMatrix", scene.camera.projectionMatrix)

	def createGeometryShader(self, geometryShader):
		return None

	def vertexSource(self):
		return """
			#version 330
			layout(location = 0) in vec3 position;
			layout(location = 2) in vec3 color;
			uniform mat4 modelMatrix;
			uniform mat4 viewMatrix;
			uniform mat4 projectionMatrix;
			smooth out vec4 vertexColor;

			void main()
			{
			    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0);
			    vertexColor = vec4(color, 1.0);
			}
		"""

	def fragmentSource(self):
		return """
			#version 330
			smooth in vec4 vertexColor;
			out vec4 fragColor;

			void main()
			{
				fragColor = vertexColor;
			}
		"""