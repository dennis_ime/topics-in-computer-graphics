from MySource.Graphics.Shader.MyShader import MyShader

from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect

class BackgroundShader(MyShader):
	"""docstring for BackgroundShader"""
	def __init__(self, geometryShader=GeometryShaderEffect.NONE):
		super(BackgroundShader, self).__init__(geometryShader)
		
	def setUpMaterial(self, textures, material):
		pass	

	def setUpSceneIllumination(self, scene):
		pass

	def setUpActorTransform(self, transform):
		pass

	def setUpSceneCamera(self, scene):
		pass

	def createGeometryShader(self, geometryShader):
		return None

	def vertexSource(self):
		return """
			#version 330
			layout(location = 0) in vec3 position;
			layout(location = 2) in vec3 color;
			smooth out vec4 vertexColor;

			void main()
			{
				gl_Position = vec4(position, 1.0);
				vertexColor = vec4(color, 1.0);
			}
		"""

	def fragmentSource(self):
		return """
			#version 330
			smooth in vec4 vertexColor;
			out vec4 fragColor;

			void main()
			{
				fragColor = vertexColor;
			}
		"""