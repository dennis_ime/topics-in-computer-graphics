from MySource.Graphics.Shader.MyShader import MyShader

from MySource.Graphics.Shader.GeometryShaderEffect import GeometryShaderEffect

class UniformMaterialShader(MyShader):
	"""docstring for WireframeMaterialShader"""
	def __init__(self, geometryShader=GeometryShaderEffect.NONE):
		super(UniformMaterialShader, self).__init__(geometryShader)

	def setUpMaterial(self, textures, material):
		self.setUniformValue("material.emission", material.emissionColor)
		self.setUniformValue("material.ambient", material.ambientColor)
		self.setUniformValue("material.diffuse", material.diffuseColor)
		self.setUniformValue("material.specular", material.specularColor)
		self.setUniformValue("material.shininess", material.shininess)

	def setUpSceneIllumination(self, scene):
		pass

	def setUpActorTransform(self, transform):
		self.setUniformValue("modelMatrix", transform)
	
	def setUpSceneCamera(self, scene):
		self.setUniformValue("viewMatrix", scene.camera.viewMatrix)
		self.setUniformValue("projectionMatrix", scene.camera.projectionMatrix)

	def createGeometryShader(self, geometryShader):
		return None

	def vertexSource(self):
		return """
			#version 330
		
			struct Material {
				vec3 emission;
				vec3 ambient;
				vec3 diffuse;
				vec3 specular;    
				float shininess;
			}; 

			layout(location = 0) in vec3 position;
			
			uniform mat4 modelMatrix;
			uniform mat4 viewMatrix;
			uniform mat4 projectionMatrix;
			uniform Material material;

			smooth out vec4 vertexColor;

			void main()
			{
			    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0);
			    vertexColor = vec4(material.diffuse, 1.0);
			}
		"""

	def fragmentSource(self):
		return """
			#version 330
			smooth in vec4 vertexColor;
			out vec4 fragColor;

			void main()
			{
				fragColor = vertexColor;
			}
		"""