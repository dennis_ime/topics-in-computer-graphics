from OpenGL import GL
from PyQt5.QtGui import QVector3D
import numpy as np
import scipy.misc as mi


## ============================ Texture 2D =================================================================== ##
class MyTexture2D(object):
	# NOTE: This class is meant to be used after the OpenGL context be initilized
	# This class and the comments are based on the webpage: https://learnopengl.com/Getting-started/Textures

	TextureActiveId = [GL.GL_TEXTURE0, GL.GL_TEXTURE1, GL.GL_TEXTURE2, GL.GL_TEXTURE3, GL.GL_TEXTURE4, GL.GL_TEXTURE5]

	class WrappingOption:
		Repeat = GL.GL_REPEAT                   ## Repeats the texture image.
		MirroredRepeat = GL.GL_MIRRORED_REPEAT  ## Repeats the mirrored texture image.
		ClampToEdge = GL.GL_CLAMP_TO_EDGE       ## Clamps the coordinates between 0 and 1.
		ClampToBorder = GL.GL_CLAMP_TO_BORDER   ## Coordinates outside range are given a user-specified border color
		Options = [Repeat, MirroredRepeat, ClampToEdge, ClampToBorder]

	class FilteringMethod:
		Nearest = GL.GL_NEAREST                             ## Nearest neighbors filtering.
		Linear = GL.GL_LINEAR                               ## Takes an interpolated value from the texture's neighboring texels.
		NearestMipmapNearest = GL.GL_NEAREST_MIPMAP_NEAREST ## Nearest mipmap - nearest neighbor interpolation for texture sampling
		LinearMipmapNearest = GL.GL_LINEAR_MIPMAP_NEAREST   ## Nearest mipmap - linear interpolation for texture sampling
		NearestMipmapLinear = GL.GL_NEAREST_MIPMAP_LINEAR   ## Linear mipmap - nearest neighbor interpolation for texture sampling
		LinearMipmapLinear = GL.GL_LINEAR_MIPMAP_LINEAR     ## Linear mipmap - linear interpolation for texture sampling
		Methods = [Nearest, Linear, NearestMipmapNearest, LinearMipmapLinear, NearestMipmapLinear, LinearMipmapLinear]

	def __init__(self, data, **kwargs):
		super(MyTexture2D, self).__init__()
		
		self._data = data
		self._id = kwargs.get("id", None)
		self._texUnit = kwargs.get("texUnit", None)
		self._wrappingSOption = kwargs.get("wrappingSOption", MyTexture2D.WrappingOption.MirroredRepeat)
		self._wrappingTOption = kwargs.get("wrappingTOption", MyTexture2D.WrappingOption.MirroredRepeat)
		self._borderColor = kwargs.get("borderColor", QVector3D(0.0, 0.0, 0.0))
		self._magFiltering = kwargs.get("magFiltering", MyTexture2D.FilteringMethod.Linear)
		self._minFiltering = kwargs.get("minFiltering", MyTexture2D.FilteringMethod.Linear)
		self._shouldGenerateMipmap = kwargs.get("shouldGenerateMipmap", True)

		self.updateGraphicDeviceState()
		
	def bind(self):
		GL.glBindTexture(GL.GL_TEXTURE_2D, self._id)

	def destroy(self):
		GL.glDeleteTextures(self._id)
		self._id = None

	def updateGraphicDeviceState(self):
		if self._id == None: self._id = GL.glGenTextures(1)
		self.bind()
		data = self._data
		GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, self._wrappingSOption)
		GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, self._wrappingTOption)
		GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, self._minFiltering)
		GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, self._magFiltering)
		GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB, data.shape[1], data.shape[0], 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, data)
		if self._shouldGenerateMipmap: GL.glGenerateMipmap(GL.GL_TEXTURE_2D)		
		if self.wrappingSOption == MyTexture2D.WrappingOption.ClampToBorder or self.wrappingTOption == MyTexture2D.WrappingOption.ClampToBorder:
			GL.glParameterfv(GL.GL_TEXTURE_2D. GL.GL_TEXTURE_BORDER_COLOR, self._borderColor)

	@property
	def wrappingSOption(self):
		return self._wrappingSOption

	@wrappingSOption.setter
	def wrappingSOption(self, value):
		self._wrappingTOption = value
	
	def setDeviceWrappingSOption(self, value):
		self.wrappingSOption = value
		self.bind()
		GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, self.wrappingSOption)

	@property
	def wrappingTOption(self):
		return self._wrappingTOption

	@wrappingTOption.setter
	def wrappingTOptiong(self, value):
		self._wrappingTOption = value

	def setDeviceWrappingTOption(self, value):
		self.wrappingTOption = value
		self.bind()
		self.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, self.wrappingTOption)

	@property
	def borderColor(self):
		return self._borderColor

	@borderColor.setter
	def borderColor(self, value):
		self._borderColor = value

	def setDeviceBorderColor(self, value):
		self.borderColor = value
		self.bind()
		GL.glParameterfv(GL.GL_TEXTURE_2D. GL.GL_TEXTURE_BORDER_COLOR, self._borderColor)

	@property
	def magFiltering(self):
		return self._magFiltering

	@magFiltering.setter
	def magFiltering(self, value):
		self._magFiltering = value

	def setDeviceMagFiltering(self, value):
		self.magFiltering = value
		self.bind()
		GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, self._magFiltering)
	
	@property
	def minFiltering(self):
		return self._minFiltering

	@minFiltering.setter
	def minFiltering(self, value):
		self._minFiltering = value

	def setMinFiltering(self, value):
		self.minFiltering = value
		self.bind()
		self.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, self._minFiltering)

	def generateMipmapByDevice(self):
		self._shouldGenerateMipmap = True
		GL.glGenerateMipmap(GL_TEXTURE_2D)

	@property
	def shouldGenerateMipmap(self):
		return self._shouldGenerateMipmap

	@property
	def texUnit(self):
		return self._texUnit

	def setUpTextureAndActive(self):
		self.updateGraphicDeviceState()
		self.bind()
		self.active()

	def active(self):
		GL.glActiveTexture(self.TextureActiveId[self._texUnit])
