*------------------------------------------------------------------ *
Nome: Dênnis José da Silva
NUSP: 9176517

MAC6913 - Tópicos em Computação Gráfica
EP #1 - Geometry Shader
* ----------------------------------------------------------------- *

EXECUCAÇÃO:
* ------------------------------------------------------------------------ *
rodar o arquivo main.py com python:

    > python3 main.py

COMENTARIOS
* ---------------------------------------------------------------------------- *

O código desenvolvido nesse exercício foi realizado em cima do último
de MAC5744, o programa executável desse exercício assim como a 
devidas modificações para manter a consistência entre as novas interfaces das
classes foram mantidas para o fim de comparação.


Todos os arquivos desenvolvidos ou modificados para o 
Exercício-programa estão na pasta "MySource", além do 
arquivo "main.py"

A pasta 3rd-party, possui um código desenvolvido por mim, para
transformar o arquivo obj do teapot em um formato mais "amigável",
para implementação do Actor Teapot (no EP #3 de MAC5744).

OBS.: Embora eu tenha trocado quase todas as classes originais do
código fornecido pelo professor, eu mantive as classes não alteradas em Source,
e as classes que não foram modificadas são utilizadas no código do
meu exercício, por isso a mesma não pode ser removida.

O programa desenvolvido nesse exercício deve cumprir todas os itens solicitados na 
descrição do exercício. Porém, nota-se que conforme o professor informou na sala
que o BRDF da Epíc desenvolvido na disciplina anterior não era necessário, o mesmo,
não foi adaptado para esse exercício (isso demandaria tempo além do prazo final, embora
as classes desenvolvidas no exercício possuam algo suporte ao mesmo). Portanto,
a aplicação desenvolvida possui as seguintes características:

- Implementação do Toro, com informações de normais e coordenadas de textura. Parametrizável
	por raio interior, raio exterior e resoluções em latitude e longitude. 
	OBS.: os seguintes materiais foram utilizadas como apoio:
	http://web.cs.ucdavis.edu/~amenta/s12/findnorm.pdf
	http://www.it.hiof.no/~borres/j3d/JOGL/torus/p-torus.html

- "Spike" implementado como um geometry shader, incluindo calculo das normais e coordenadas de 
	textura para as novas faces geradas no shader.

- Interface para trocas os objetos geometricos: Cubo, Esfera, Cilindro, Cone, "Teapot" e Toro.
	assim como uma interface para trocar os parâmetros do Toro.

- Interface para desligar e ligar o "spike mapping".

- Interface para alterar os parâmetros dos "spike mapping".



OBS Interface.:
* -------------------------------------------------------------------------------------------- *
A interface oferece uma barra de status padrão fornecida no código base. A interface 
principal possui uma barra de menu lateral com os seguintes itens:

- BRDF:      Menu onde eu pretendia incluir a troca do BRDF Epic ou Blinn-Phong, mas 
	           por causa do prazo o mesmo não foi implementado.

- 3D Model:  Menu feito para trocar o modelo visualizado, se o Toro for selecionado
             o menu abre opções para trocar seus parâmetros.

- Lighting:  Menu feito para ajuste nos parâmetros de iluminação, nesse menu pode ser
             alterado as cores das fontes de luz assim com o tipo.

- Material:  Menu feito para ajuste dos parâmetros de material, os parâmetros são 
			       os mesmo que descrevem um material no modelo Blinn-Phong

- Effect:    Menu de efeitos, a ideia original do menu era ativar e desativar spike mapping
             e normal mapping, mas não tive tempo de adaptar o normal mapping, portanto, nesse
						 menu é possível ativar e alterar os parâmetros do "spike mapping" apenas.

- About:     Um pequeno texto sobre o programa assim com os créditos das imagens utilizadas
             no menu assim como solicitado pelo website que as disponibilizou. 



PRINCIPAIS ITENS DE DESENVOLVIMOTO PARA ESSE EXERCÍCIO
* ------------------------------------------------------------------------------------------------ *
- Refactoring de toda a interface que a partir do último EP da disciplina anterior não
  suportava a inclusão de novos elementos com facilidade. A maioria das classes desenvolvidas
  nos arquivos da pasta "MyGUI" foram rescritas da pasta GUI feitas no exercício anterior
  e diversas funcionalidade foram dividas em painéis depois do refactoring.

- Desenvolvimento de um pequeno preprocessador para GLSL. Nesse modulo incluímos a
  funcionalidade de #include (semelhante a linguagem C) de arquivos GLSL em outros
  arquivos GLSL e de #modifier (para indicar flat ou smooth) de variáveis. Essa 
  funcionalidade está no arquivo PreProcessor.py na pasta shader.

 - Programas "GLSL" (com as extensões de pre-processamento desenvolvidas) contendo os shaders 
   e o shader de geometria estão na pasta "MySource/ShaderProgram"

 - Refactoring das classes de shader para aceitar o preprocessamento e a leitura dos programas
	 GLSL via arquivo. A inclusão do shader de geometria - Padrão (Default) que só passa os
	 parâmetros com a nova interface e "spiky" que implementa o "spike mapping".

 - Torus.py implementa o Toro.


Créditos para os icones utilizados
* --------------------------------------------------------------------------------------------------- *

Geometry (model): Designed by Smashicons from www.flaticon.com
Cube (shading): Designed by Smashicons from www.flaticon.com
light (light): Designed by Freepik from www.flaticon.com
big-data (material): Designed by Dinosoft from www.flaticon.com
enhance-effect (effect): Designed by Freepik from www.flaticon.com
information (about): Designed by Good Ware from www.flaticon.com


CONTATO
* --------------------------------------------------------------------------------------------------- *
Qualquer dúvida ou problema, por favor, entrar em contato por um dos seguintes e-mails:

dennis@ime.usp.br
dennisjosesilva@gmail.com