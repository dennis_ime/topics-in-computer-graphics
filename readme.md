-------------------------------------------------------------------------------------
Author: Dennis Jóse da Silva (http://www.vision.ime.usp.br/~dennis/_site/)

Description: Repository to manage code from the assignments of the course MAC6913 - 
topics in computer graphics from the Institute of mathematics and statistics of 
the University of São Paulo (IME-USP) 
--------------------------------------------------------------------------------------

The assignments use PyOpenGL and PyQt5. The starting point are given by the instructor
(professor Marcel P. Jackowski), it can be downloaded at: https://github.com/mjck/mac420

Currently, this repository contains the code for the first assignment of the course MAC6913.
In this assignment, I coded:

- "Spiky effect": It generates spikes in all vertices of a 3D model by computing a triangle
									(face) for each vertex. Given a vertex v which forms a face with the 
									vertices vl and vr, we compute this new triangle  (face of a spike) by: 
									(i) create a new vertex at the same position of v translated by
									"height" in the direction of the normal vector of v; 
									(ii) create other two new vertices in the same position of v but 
									translated  by "width" in the direction of the vectors vl - v and
									vr - v. The normal vector for this new vertices are computed with
									"cross product" and the texture coordinates are interpolated.
									This effect is activated by including a geometry shader 
									coded for the assignment. 

- "Torus":       I computed a new actor (Actor is a structure in the framework which 
								 represents a geometry entity that can be rendered, it contains geometry, 
								 normals, texcoord and material information) with the geometry of a Torus. 
								 It is parameterized by latitude resolution, longitude resolution, inner
								 radius and outer radius.

- Interface:     I coded the necessary UI elements to allow users to handle the parameters 
								 of the Torus and "spiky effect". The UI also provides controls of the 
								 material and lighting parameters.

In order to run the application, one may run the file "main.py" (inside EP1 directory) using 
a python3 interpreter. It is important to note that the application depends on:

- Python 3
- Numpy
- PyQt5
- PyOpenGL
---------------------------------------------------------------------------------------------- 

This repository also contains an application developed as an assignment of the course:
MAC5744 - Introduction to computer graphics. The main feature of this application is 
the implementation of the BRDF defined by the instructor. This BRDF is called "Epic" 
in the application because the instructor said he based it on a BSDF used by the 
"Unreal engine" developed by "Epic games" 
(https://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf).
The application also provides UI to control parameter of this BRDF including the use of texture to
set up material parameters.

This application can be run by using a python3 interpreter on the file "ep3-MAC5744.py".


---------------------------------------------------------------------------------------------------
Credits:

The images used on the UI are downloaded at www.flaticon.com as described below:

- Geometry (model): Designed by Smashicons from www.flaticon.com

- Cube (shading): Designed by Smashicons from www.flaticon.com

- light (light): Designed by Freepik from www.flaticon.com

- big-data (material): Designed by Dinosoft from www.flaticon.com

- enhance-effect (effect): Designed by Freepik from www.flaticon.com

- information (about): Designed by Good Ware from www.flaticon.com

------------------------------------------------------------------------------------------------------
Contact

- dennisjosesilva@gmail.com.br
- dennis@ime.usp.br


